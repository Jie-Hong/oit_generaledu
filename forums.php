<?php
/*
討論區更新需要更新這些檔案 2017/11/07
/php/functions.php
/topic.php   300~800
/js/topic-class.js
/js/topic-responses.js
資料庫 資料表 web_class
*/
  require_once "./php/functions.php";

  $semester = $_GET['semester'];

  if (isset($semester)) {
    //學期區別
    if ($semester === "1062") {
      $sDate1 = date('Y-m-d H:i:s', strtotime("03/01/2018"));
      $sDate2 = date('Y-m-d H:i:s', strtotime("07/01/2018"));
    } else if ($semester === "1052") {
      $sDate1 = date('Y-m-d H:i:s', strtotime("03/01/2017"));
      $sDate2 = date('Y-m-d H:i:s', strtotime("07/01/2017"));

    } else if ($semester === "1051") {
      $sDate1 = date('Y-m-d H:i:s', strtotime("09/01/2016"));
      $sDate2 = date('Y-m-d H:i:s', strtotime("02/01/2017"));

    } else if ($semester === "1042") {
      $sDate1 = date('Y-m-d H:i:s', strtotime("02/01/2016"));
      $sDate2 = date('Y-m-d H:i:s', strtotime("07/01/2016"));
    } else if ($semester === "1041") {
      $sDate1 = date('Y-m-d H:i:s', strtotime("09/01/2015"));
      $sDate2 = date('Y-m-d H:i:s', strtotime("02/01/2016"));
    } else if ($semester === "1061") {
      $sDate1 = date('Y-m-d H:i:s', strtotime("09/01/2017"));
      $sDate2 = date('Y-m-d H:i:s', strtotime("02/01/2018"));
    }else if($semester === "1071"){
      $sDate1 = date('Y-m-d H:i:s', strtotime("09/01/2018"));
      $sDate2 = date('Y-m-d H:i:s', strtotime("02/01/2019"));
    } 
    else {
      header('Location: 404.php');
    }

    $sql = "SELECT * FROM `forum_article` LEFT JOIN `web_member` ON `forum_article`.`articlePublisher` = `web_member`.`memberAccount`
            WHERE `articleDate` >= '". $sDate1 . "' AND `articleDate` <= '". $sDate2 ."' ORDER BY `articleID` DESC";
    $result = $db->query($sql);

  } else {
    header('Location: forums.php?semester=1071');
    /* 學長給的原本有error 使用學校原版之後ok 待研究 2017/8/22
    $semester = "1052";
    $sDate1 = date('Y-m-d H:i:s', strtotime("03/01/2017"));
    $sDate2 = date('Y-m-d H:i:s', strtotime("07/01/2017"));

    $sql = "SELECT * FROM `forum_article` LEFT JOIN `web_member` ON `forum_article`.`articlePublisher` = `web_member`.`memberAccount`
            WHERE `articleDate` >= '". $sDate1 . "' AND `articleDate` <= '". $sDate2 ."' ORDER BY `articleID` DESC";
    $result = $db->query($sql);
    */
  }
  // $db = null;

/*
  $query  = "SELECT * FROM forum_article ORDER BY articleID DESC";
  $result = $conn->query($query);
  if (!$result) die($conn->error);
  $rows = $result->num_rows;
*/

 /* for ($j = 0 ; $j < $rows ; ++$j)
  {
    $result->data_seek($j);
    echo 'articleID: '   . $result->fetch_assoc()['articleID']   . '<br>';
    $result->data_seek($j);
    echo 'articleType: '    . $result->fetch_assoc()['articleType']    . '<br>';
    $result->data_seek($j);
    echo 'articleViewer: ' . $result->fetch_assoc()['articleViewer'] . '<br>';
    $result->data_seek($j);
    echo 'articleSubject: '     . $result->fetch_assoc()['articleSubject']     . '<br>';
    $result->data_seek($j);
    echo 'articlePublisher: '     . $result->fetch_assoc()['articlePublisher']     . '<br>';
    $result->data_seek($j);
    echo 'articleDate: '     . $result->fetch_assoc()['articleDate']     . '<br><br>';
  }
*/
?>
<!DOCTYPE html>
<html lang="zh-Hant-TW">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, , maximum-scale=1">
  <title><?php echo $semester ?> 討論區 - 文韻亞東</title>
  <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css"> <!-- Font-Awesome -->
  <link rel="stylesheet" href="outdatedbrowser/outdatedbrowser.css"> <!-- outdatedbrowser 檢查瀏覽器 -->
  <script src="outdatedbrowser/outdatedbrowser.js"></script> <!-- outdatedbrowser 檢查瀏覽器 -->
  <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<header id="header">
  <section class="header-site-channel">
    <div class="container">
      <div class="header-channel-content">
        <div class="header-logo">
          <a href="index.php"><img src="images/logo.png" /></a>
          <span>本課程由教育部資訊及科技教育司支持</span>
          <div class="header-video">
            <a href="./news-detail.php?id=24">
              <!-- <img src="./video/1042.jpg"> -->
              <!-- <i class="fa fa-play-circle-o"></i> -->
            </a>
          </div>
        </div>
        <?php
          if ($loggedin)
          {
            echo "<div class=\"header-login\">
                    <ul>
                      <li><a>您好，$user</a></li>
                      <li><a href=\"user-edito.php\">會員編輯</a></li>
                      <li><a href=\"admin/index.php\">後台管理</a></li>
                      <li><a href=\"logout.php\">登出</a></li>
                    </ul>
                  </div>";
          }
          else
          {
            echo "<div class=\"header-login\">
                    <ul>
                      <li><a href=\"login.php\">登入</a></li>
                      <li><a href=\"register.php\">註冊</a></li>
                    </ul>
                  </div>";
          }
        ?>
      </div>
    </div>
  </section>
  <nav class="navbar" role="navigation">
    <div class="container">
      <div class="navbar-collapse">
        <ul class="nav navbar-nav">
          <li><a href="news.php">最新消息</a></li>
          <li><a href="intro.php">課程計畫</a></li>
          <li class="active"><a href="forums.php">討論區</a></li>
          <li><a href="vote.php">投票活動</a></li>
          <li><a href="memory.php">亞東印記</a></li>
          <li><a href="works.php">優秀作品</a></li>
          <li><a href="picture.php">影像紀錄</a></li>
          <li><a href="activity.php">活動集錦</a></li>
          <li><a href="videosharing.php">影片分享</a></li>
          <li><a href="feedback.php">TA回饋分享</a></li>
          <li><a href="team.php">核心團隊</a></li>
          <li><a href="links.php">相關資源</a></li>
        </ul>
      </div>
    </div>
  </nav>
</header>
<section class="forums-title">
  <div class="container">
    <h1><span>Forums</span>討論區</h1>
  </div>
</section>
<section class="forums-main">
  <div class="container">
    <div class="breadcrumb">
      <ul>
        <li><a href="index.php">首頁</a></li>
        <li><a href="forums.php">討論區</a></li>
      </ul>
    </div>
    <div class="semester">
      <ul>
        <?php //87寫法 懶  ?>

        <?php if ($semester==="1071") { ?>
        <li><a href="forums.php?semester=1071" class="active">107-1</a></li>
        <?php } else {?>
        <li><a href="forums.php?semester=1071">107-1</a></li>
        <?php } ?>

        <?php if ($semester==="1062") { ?>
        <li><a href="forums.php?semester=1062" class="active">106-2</a></li>
        <?php } else {?>
        <li><a href="forums.php?semester=1062">106-2</a></li>
        <?php } ?>

        <?php if ($semester==="1061") { ?>
        <li><a href="forums.php?semester=1061" class="active">106-1</a></li>
        <?php } else {?>
        <li><a href="forums.php?semester=1061">106-1</a></li>
        <?php } ?>

        <?php if ($semester==="1052") { ?>
        <li><a href="forums.php?semester=1052" class="active">105-2</a></li>
        <?php } else {?>
        <li><a href="forums.php?semester=1052">105-2</a></li>
        <?php } ?>

        <?php if ($semester==="1051") { ?>
        <li><a href="forums.php?semester=1051" class="active">105-1</a></li>
        <?php } else {?>
        <li><a href="forums.php?semester=1051">105-1</a></li>
        <?php } ?>

        <?php if ($semester==="1042") { ?>
        <li><a href="forums.php?semester=1042" class="active">104-2</a></li>
        <?php } else {?>
        <li><a href="forums.php?semester=1042">104-2</a></li>
        <?php } ?>

        <?php if ($semester==="1041") { ?>
        <li><a href="forums.php?semester=1041" class="active">104-1</a></li>
        <?php } else {?>
        <li><a href="forums.php?semester=1041">104-1</a></li>
        <?php } ?>
      </ul>
    </div>
    <div class="forums-inner">
      <ul class="forumslist">
      <?php
        while ($row = $result->fetch()){
          $title=htmlspecialchars($row['articleSubject'], ENT_QUOTES);
          $date=substr($row['articleDate'],0,10);
          $articleType=$row['articleType'];
          $articleHot=$row['articleViewer'];
          //顯示文章類別
          switch($articleType){
            case '1':
              $articleTypeName = '延伸閱讀';
              break;
            case '2':
              $articleTypeName = '一般討論';
            break;
            case '3':
              $articleTypeName = '比賽活動';
              break;
          }
          //顯示熱度
          if ($articleHot >= 0 && $articleHot < 100)
              $articleHotColor=0;
              elseif ($articleHot >= 100 && $articleHot < 500)
              $articleHotColor=1;
              else
              $articleHotColor=2;
          //統計留言次數
          $CommentCountArticleID = $row['articleID'];
          $sqlCommentCount = "SELECT COUNT(`commentArticleID`) FROM `forum_comment` WHERE `commentArticleID` = '$CommentCountArticleID'";
          $countResult = $db->query($sqlCommentCount);
          $countrow = $countResult->fetch();
          $commentCount=$countrow['COUNT(`commentArticleID`)'];

          echo
            "<li data-views=\"$articleHotColor\">
              <p class=\"info\">
                <span class=\"type\" data-type=\"$articleType\">$articleTypeName</span>
                <span class=\"responses\">$commentCount</span>
              </p>
              <!-- title -->
              <p class=\"title\">
                <a href=\"topic.php?id={$row['articleID']}\">$title</a>
                <span class=\"author\">{$row['memberNickname']}</span>
                <span class=\"post-date\">$date</span>
              </p>
              <span class=\"views\">{$row['articleViewer']}</span>
            </li>";
        }
      ?>
        <!--<a href="javascript:void(0)" id="news-more" class="news-more">載入更多</a>-->
      </ul>
    </div>
  </div>
</section>
<footer id="footer">
  <div class="subfooter">
    <div class="container">
      <div class="row">
        <div class="col-xxs-12 col-md-7">
          <div class="footer-nav">
            <ul>
              <li><a href="index.php">首頁</a></li>
              <li><a href="news.php">最新消息</a></li>
              <li><a href="intro.php">課程計畫</a></li>
              <li><a href="forums.php">討論區</a></li>
              <li><a href="works.php">優秀作品</a></li>
              <li><a href="picture.php">影像紀錄</a></li>
              <li><a href="team.php">核心團隊</a></li>
            </ul>
          </div>
        </div>
        <div class="col-xxs-12 col-md-5">
          <div class="school">
            <p>亞東技術學院 通識教育中心 OIT Center for General Education</p>
          </div>
          <div class="plan">
            <p>文韻亞東 ─ 中文閱讀書寫課程革新計畫</p>
          </div>
          <div class="hss">
            <p>本課程由教育部資訊及科技教育司支持</p>
          </div>
        </div>
      </div>
    </div> <!-- container -->
  </div> <!-- subfooter -->
  <div class="copyright">
    <div class="container">
      <div class="text">
        <small>
          <address class="author">
            Copyright  2015-2016.
          </address>
          <a href="http://gecw.oit.edu.tw/bin/home.php" title="亞東技術學院通識教育中心" target="_blank">亞東技術學院通識教育中心</a> All Rights Reserved.
        </small>
      </div>
      <div class="total">
        <?php require_once "./statistics.php"; ?>
      </div>
    </div>
  </div>
</footer>
<div id="outdated"></div> <!-- outdatedbrowser 檢查瀏覽器 -->
<script src="js/style.js"></script>
<script src="outdatedbrowser/dom-outdatedbrowser.js"></script> <!-- outdatedbrowser 檢查瀏覽器 -->
</body>
</html>
