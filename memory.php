<?php
  require_once "./php/functions.php";
?>
<!DOCTYPE html>
<html lang="zh-Hant-TW">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, , maximum-scale=1">
  <title>亞東印記 - 文韻亞東</title>
  <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css"> <!-- Font-Awesome -->
  <link rel="stylesheet" href="outdatedbrowser/outdatedbrowser.css"> <!-- outdatedbrowser 檢查瀏覽器 -->
  <script src="outdatedbrowser/outdatedbrowser.js"></script> <!-- outdatedbrowser 檢查瀏覽器 -->
  <link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>
  <header id="header">
    <section class="header-site-channel">
      <div class="container">
        <div class="header-channel-content">
          <div class="header-logo">
            <a href="index.php"><img src="images/logo.png" /></a>
            <span>本課程由教育部資訊及科技教育司支持</span>
            <div class="header-video">
              <a href="./news-detail.php?id=24">
                <!-- <img src="./video/1042.jpg"> -->
                <!-- <i class="fa fa-play-circle-o"></i> -->
              </a>
            </div>
          </div>
          <?php
        if($loggedin)
        {
        echo
          "<div class=\"header-login\">
             <ul>
               <li><a>您好，$user</a></li>
               <li><a href=\"user-edito.php\">會員編輯</a></li>
               <li><a href=\"admin/index.php\">後台管理</a></li>
               <li><a href=\"logout.php\">登出</a></li>
             </ul>
           </div>";
        }
        else
        {
          echo
            "<div class=\"header-login\">
               <ul>
                 <li><a href=\"login.php\">登入</a></li>
                 <li><a href=\"register.php\">註冊</a></li>
               </ul>
             </div>";
        }
        ?>
        </div>
      </div>
    </section>
    <nav class="navbar" role="navigation">
      <div class="container">
        <div class="navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="news.php">最新消息</a></li>
            <li><a href="intro.php">課程計畫</a></li>
            <li><a href="forums.php">討論區</a></li>
            <li><a href="vote.php">投票活動</a></li>
            <li class="active"><a href="memory.php">亞東印記</a></li>
            <li><a href="works.php">優秀作品</a></li>
            <li><a href="picture.php">影像紀錄</a></li>
            <li><a href="activity.php">活動集錦</a></li>
            <li><a href="videosharing.php">影片分享</a></li>
            <li><a href="feedback.php">TA回饋分享</a></li>
            <li><a href="team.php">核心團隊</a></li>
            <li><a href="links.php">相關資源</a></li>
          </ul>
        </div>
      </div>
    </nav>
  </header>
  <section class="works">
    <div class="container">
      <h1><span>OIT MEMORY</span>亞東印記</h1>
    </div>
  </section>
  <section class="ta-main">
    <div class="container">
      <div class="breadcrumb">
        <ul>
          <li><a href="index.php">首頁</a></li>
          <li><a href="memory.php">亞東印記</a></li>
        </ul>
      </div>
      <div class="memory-content" id="memory-content">
        <div class="row">
          <div class="col-xs-6">
            <div class="box" data-id="2018-9">
              <img src="./images/memory/cover-2018-9.jpg" />
              <h4>第五期 2018-9</h4>
              <div class="read">開始閱讀</div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-6">
            <div class="box" data-id="2017-6">
              <img src="./images/memory/cover-2017-6.jpg" />
              <h4>第三期 2017-6</h4>
              <div class="read">開始閱讀</div>
            </div>
          </div>
          <div class="col-xs-6">
            <div class="box" data-id="2017-12">
              <img src="./images/memory/cover-2017-12.jpg" />
              <h4>第四期 2017-12</h4>
              <div class="read">開始閱讀</div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-6">
            <div class="box" data-id="2016-7">
              <img src="./images/memory/cover-2016-7.jpg" />
              <h4>創刊號 2016-7</h4>
              <div class="read">開始閱讀</div>
            </div>
          </div>
          <div class="col-xs-6">
            <div class="box" data-id="2016-12">
              <img src="./images/memory/cover-2016-11.jpg" />
              <h4>第二期 2016-11</h4>
              <div class="read">開始閱讀</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
  </section>
  <footer id="footer">
    <div class="subfooter">
      <div class="container">
        <div class="row">
          <div class="col-xxs-12 col-md-7">
            <div class="footer-nav">
              <ul>
                <li><a href="index.php">首頁</a></li>
                <li><a href="news.php">最新消息</a></li>
                <li><a href="intro.php">課程計畫</a></li>
                <li><a href="forums.php">討論區</a></li>
                <li><a href="works.php">優秀作品</a></li>
                <li><a href="picture.php">影像紀錄</a></li>
                <li><a href="team.php">核心團隊</a></li>
              </ul>
            </div>
          </div>
          <div class="col-xxs-12 col-md-5">
            <div class="school">
              <p>亞東技術學院 通識教育中心 OIT Center for General Education</p>
            </div>
            <div class="plan">
              <p>文韻亞東 ─ 中文閱讀書寫課程革新計畫</p>
            </div>
            <div class="hss">
              <p>本課程由教育部資訊及科技教育司支持</p>
            </div>
          </div>
        </div>
      </div> <!-- container -->
    </div> <!-- subfooter -->
    <div class="copyright">
      <div class="container">
        <div class="text">
          <small>
            <address class="author">
              Copyright 2015-2016.
            </address>
            <a href="http://gecw.oit.edu.tw/bin/home.php" title="亞東技術學院通識教育中心" target="_blank">亞東技術學院通識教育中心</a> All
            Rights Reserved.
          </small>
        </div>
        <div class="total">
          <?php require_once "./statistics.php"; ?>
        </div>
      </div>
    </div>
  </footer>
  <div id="memory-lightbox" class="memory-lightobx">
    <div id="memory-close"><i class="fa fa-arrow-left"></i></div>
    <h2>創刊號 2016-7</h2>
    <h4>電子版</h4>
    <a href="#" target="_blank"><img src=""></a>
    <br>
    <br>
    <h4>實體版</h4>
    <a href="#" target="_blank"><img src=""></a>
    <a href="#" target="_blank"><img src=""></a>
  </div>
  <div id="outdated"></div> <!-- outdatedbrowser 檢查瀏覽器 -->
  <script src="js/style.js"></script>
  <script src="outdatedbrowser/dom-outdatedbrowser.js"></script> <!-- outdatedbrowser 檢查瀏覽器 -->
  <script src="js/memory.js"></script>
</body>

</html>