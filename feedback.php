<?php
  require_once "./php/functions.php";

  $semester = $_GET['semester'];

  if (isset($semester)) {
    if ($semester === "1041" || $semester === "1042" || $semester === "1051" || $semester === "1052" || $semester === "1061") {
    } else {
      header('Location: 404.php');
    }
  } else {
    header('Location: feedback.php?semester=1061');
  }
?>
<!DOCTYPE html>
<html lang="zh-Hant-TW">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, , maximum-scale=1">
  <title><?php echo $semester ?> TA回饋分享 - 文韻亞東</title>
  <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css"> <!-- Font-Awesome -->
  <link rel="stylesheet" href="outdatedbrowser/outdatedbrowser.css"> <!-- outdatedbrowser 檢查瀏覽器 -->
  <script src="outdatedbrowser/outdatedbrowser.js"></script> <!-- outdatedbrowser 檢查瀏覽器 -->
  <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<header id="header">
  <section class="header-site-channel">
    <div class="container">
      <div class="header-channel-content">
        <div class="header-logo">
          <a href="index.php"><img src="images/logo.png" /></a>
          <span>本課程由教育部資訊及科技教育司支持</span>
          <div class="header-video">
            <a href="./news-detail.php?id=24">
              <!-- <img src="./video/1042.jpg"> -->
              <!-- <i class="fa fa-play-circle-o"></i> -->
            </a>
          </div>
        </div>
        <?php
        if($loggedin)
        {
        echo
          "<div class=\"header-login\">
             <ul>
               <li><a>您好，$user</a></li>
               <li><a href=\"user-edito.php\">會員編輯</a></li>
               <li><a href=\"admin/index.php\">後台管理</a></li>
               <li><a href=\"logout.php\">登出</a></li>
             </ul>
           </div>";
        }
        else
        {
          echo
            "<div class=\"header-login\">
               <ul>
                 <li><a href=\"login.php\">登入</a></li>
                 <li><a href=\"register.php\">註冊</a></li>
               </ul>
             </div>";
        }
        ?>
      </div>
    </div>
  </section>
  <nav class="navbar" role="navigation">
    <div class="container">
      <div class="navbar-collapse">
        <ul class="nav navbar-nav">
          <li><a href="news.php">最新消息</a></li>
          <li><a href="intro.php">課程計畫</a></li>
          <li><a href="forums.php">討論區</a></li>
          <li><a href="vote.php">投票活動</a></li>
          <li><a href="memory.php">亞東印記</a></li>
          <li><a href="works.php">優秀作品</a></li>
          <li><a href="picture.php">影像紀錄</a></li>
          <li><a href="activity.php">活動集錦</a></li>
          <li><a href="videosharing.php">影片分享</a></li>
          <li class="active"><a href="feedback.php">TA回饋分享</a></li>
          <li><a href="team.php">核心團隊</a></li>
          <li><a href="links.php">相關資源</a></li>
        </ul>
      </div>
    </div>
  </nav>
</header>
<section class="ta-share">
  <div class="container">
    <h1><span>TA Feedback Share</span>TA回饋分享</h1>
  </div>
</section>
<section class="ta-main">
  <div class="container">
    <div class="breadcrumb">
      <ul>
        <li><a href="index.php">首頁</a></li>
        <li><a href="feedback.php">TA回饋分享</a></li>
        <li><a href="feedback.php?semester=<?php echo $semester ?>">第<?php echo $semester ?>學期</a></li>
      </ul>
    </div>
    <div class="team-select">
      <select id="team-select" class="team-dropdown">
        <option value="1061" <?php if ($semester==="1061"){ echo "selected"; }?>>1061 學期</option>
        <option value="1052" <?php if ($semester==="1052"){ echo "selected"; }?>>1052 學期</option>
        <option value="1051" <?php if ($semester==="1051"){ echo "selected"; }?>>1051 學期</option>
        <option value="1042" <?php if ($semester==="1042"){ echo "selected"; }?>>1042 學期</option>
        <option value="1041" <?php if ($semester==="1041"){ echo "selected"; }?>>1041 學期</option>
      </select>
    </div>
    <div class="ta-share-content" id="ta-share-content">

      <?php if ( $semester === "1041") { ?>
      <div class="ta-share-semester">
        <h2>1041學期</h2>
      </div>
      <ul>
        <li>
          <div class="info">
            <img src="images/team/ta-01.jpg">
            <h3>黃國華</h3>
          </div>
          <div class="reviews">
            <div class="review-box">
              <img src="images/ta-share/1041/8/small-1.jpg" data-img="images/ta-share/1041/8/1.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1041/9/small-1.jpg" data-img="images/ta-share/1041/9/1.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1041/10/small-1.jpg" data-img="images/ta-share/1041/10/1.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1041/11/small-1.jpg" data-img="images/ta-share/1041/11/1.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1041/12/small-1.jpg" data-img="images/ta-share/1041/12/1.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1041/1/6.jpg" data-img="images/ta-share/1041/1/6.jpg" />
            </div>
          </div>
        </li>
        <li>
          <div class="info">
            <img src="images/team/ta-02.jpg">
            <h3>楊平軒</h3>
          </div>
          <div class="reviews">
            <div class="review-box">
              <img src="images/ta-share/1041/8/small-2.jpg" data-img="images/ta-share/1041/8/2.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1041/9/small-2.jpg" data-img="images/ta-share/1041/9/2.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1041/10/small-2.jpg" data-img="images/ta-share/1041/10/2.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1041/11/small-2.jpg" data-img="images/ta-share/1041/11/2.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1041/12/small-2.jpg" data-img="images/ta-share/1041/12/2.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1041/1/5.jpg" data-img="images/ta-share/1041/1/5.jpg" />
            </div>
          </div>
        </li>
        <li>
          <div class="info">
            <img src="images/team/ta-03.jpg">
            <h3>林昱瑄</h3>
          </div>
          <div class="reviews">
            <div class="review-box">
              <img src="images/ta-share/1041/8/small-3.jpg" data-img="images/ta-share/1041/8/3.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1041/9/small-3.jpg" data-img="images/ta-share/1041/9/3.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1041/10/small-3.jpg" data-img="images/ta-share/1041/10/3.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1041/11/small-3.jpg" data-img="images/ta-share/1041/11/3.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1041/12/small-3.jpg" data-img="images/ta-share/1041/12/3.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1041/1/3.jpg" data-img="images/ta-share/1041/1/3.jpg" />
            </div>
          </div>
        </li>
        <li>
          <div class="info">
            <img src="images/team/ta-04.jpg">
            <h3>黃佳雯</h3>
          </div>
          <div class="reviews">
            <div class="review-box">
              <img src="images/ta-share/1041/8/small-4.jpg" data-img="images/ta-share/1041/8/4.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1041/9/small-4.jpg" data-img="images/ta-share/1041/9/4.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1041/10/small-4.jpg" data-img="images/ta-share/1041/10/4.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1041/11/small-4.jpg" data-img="images/ta-share/1041/11/4.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1041/10/small-4.jpg" data-img="images/ta-share/1041/12/4.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1041/1/4.jpg" data-img="images/ta-share/1041/1/4.jpg" />
            </div>
          </div>
        </li>
        <li>
          <div class="info">
            <img src="images/team/ta-05.jpg">
            <h3>柯奕吉</h3>
          </div>
          <div class="reviews">
            <div class="review-box">
              <img src="images/ta-share/1041/8/small-5.jpg" data-img="images/ta-share/1041/8/5.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1041/9/small-5.jpg" data-img="images/ta-share/1041/9/5.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1041/10/small-5.jpg" data-img="images/ta-share/1041/10/5.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1041/11/small-5.jpg" data-img="images/ta-share/1041/11/5.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1041/12/small-5.jpg" data-img="images/ta-share/1041/12/5.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1041/1/1.jpg" data-img="images/ta-share/1041/1/1.jpg" />
            </div>
          </div>
        </li>
        <li>
          <div class="info">
            <img src="images/team/ta-06.jpg">
            <h3>陳忠平</h3>
          </div>
          <div class="reviews">
            <div class="review-box">
              <img src="images/ta-share/1041/8/small-6.jpg" data-img="images/ta-share/1041/8/6.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1041/9/small-6.jpg" data-img="images/ta-share/1041/9/6.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1041/10/small-6.jpg" data-img="images/ta-share/1041/10/6.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1041/11/small-6.jpg" data-img="images/ta-share/1041/11/6.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1041/12/small-6.jpg" data-img="images/ta-share/1041/12/6.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1041/1/2.jpg" data-img="images/ta-share/1041/1/2.jpg" />
            </div>
          </div>
        </li>
      </ul>
      <!-- ***************************************************** 分隔* **************************************************** -->
      <?php } else if ( $semester === "1042") { ?>
      <div class="ta-share-semester">
        <h2>1042學期</h2>
      </div>
      <ul>
        <li>
          <div class="info">
            <img src="images/team/ta-08.jpg">
            <h3>曹育愷</h3>
          </div>
          <div class="reviews">
            <div class="review-box">
              <img src="images/ta-share/1042/08/small-1.jpg" data-img="images/ta-share/1042/08/1.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1042/08/small-2.jpg" data-img="images/ta-share/1042/08/2.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1042/08/3.jpg" data-img="images/ta-share/1042/08/3.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1042/08/4.jpg" data-img="images/ta-share/1042/08/4.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1042/08/5.jpg" data-img="images/ta-share/1042/08/5.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1042/08/6.jpg" data-img="images/ta-share/1042/08/6.jpg" />
            </div>
          </div>
        </li>
        <li>
          <div class="info">
            <img src="images/team/ta-09.jpg">
            <h3>黃榆諭</h3>
          </div>
          <div class="reviews">
            <div class="review-box">
              <img src="images/ta-share/1042/09/1.jpg" data-img="images/ta-share/1042/09/1.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1042/09/2.jpg" data-img="images/ta-share/1042/09/2.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1042/09/3.jpg" data-img="images/ta-share/1042/09/3.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1042/09/4.jpg" data-img="images/ta-share/1042/09/4.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1042/09/5.jpg" data-img="images/ta-share/1042/09/5.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1042/09/6.jpg" data-img="images/ta-share/1042/09/6.jpg" />
            </div>
          </div>
        </li>
        <li>
          <div class="info">
            <img src="images/team/ta-10.jpg">
            <h3>趙正媛</h3>
          </div>
          <div class="reviews">
            <div class="review-box">
              <img src="images/ta-share/1042/10/1.jpg" data-img="images/ta-share/1042/10/1.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1042/10/2.jpg" data-img="images/ta-share/1042/10/2.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1042/10/3.jpg" data-img="images/ta-share/1042/10/3.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1042/10/4.jpg" data-img="images/ta-share/1042/10/4.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1042/10/5.jpg" data-img="images/ta-share/1042/10/5.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1042/10/6.jpg" data-img="images/ta-share/1042/10/6.jpg" />
            </div>
          </div>
        </li>
        <li>
          <div class="info">
            <img src="images/team/ta-11.jpg">
            <h3>洪梅馨</h3>
          </div>
          <div class="reviews">
            <div class="review-box">
              <img src="images/ta-share/1042/11/1.jpg" data-img="images/ta-share/1042/11/1.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1042/11/2.jpg" data-img="images/ta-share/1042/11/2.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1042/11/3.jpg" data-img="images/ta-share/1042/11/3.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1042/11/4.jpg" data-img="images/ta-share/1042/11/4.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1042/11/5.jpg" data-img="images/ta-share/1042/11/5.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1042/11/6.jpg" data-img="images/ta-share/1042/11/6.jpg" />
            </div>
          </div>
        </li>
        <li>
          <div class="info">
            <img src="images/team/ta-05.jpg">
            <h3>柯奕吉</h3>
          </div>
          <div class="reviews">
            <div class="review-box">
              <img src="images/ta-share/1042/05/1.jpg" data-img="images/ta-share/1042/05/1.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1042/05/2.jpg" data-img="images/ta-share/1042/05/2.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1042/05/3.jpg" data-img="images/ta-share/1042/05/3.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1042/05/4.jpg" data-img="images/ta-share/1042/05/4.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1042/05/5.jpg" data-img="images/ta-share/1042/05/5.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1042/05/6.jpg" data-img="images/ta-share/1042/05/6.jpg" />
            </div>
          </div>
        </li>
        <li>
          <div class="info">
            <img src="images/team/ta-12.jpg">
            <h3>陳柏安</h3>
          </div>
          <div class="reviews">
            <div class="review-box">
              <img src="images/ta-share/1042/12/1.jpg" data-img="images/ta-share/1042/12/1.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1042/12/2.jpg" data-img="images/ta-share/1042/12/2.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1042/12/3.jpg" data-img="images/ta-share/1042/12/3.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1042/12/4.jpg" data-img="images/ta-share/1042/12/4.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1042/12/5.jpg" data-img="images/ta-share/1042/12/5.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1042/12/6.jpg" data-img="images/ta-share/1042/12/6.jpg" />
            </div>
          </div>
        </li>
      </ul>

      <!-- ***************************************************** 分隔* **************************************************** -->

      <?php } else if ( $semester === "1051") { ?>
      <div class="ta-share-semester">
        <h2>1051學期</h2>
      </div>
      <ul>
        <li>
          <div class="info">
            <img src="images/team/ta-18.jpg">
            <h3>丁涵茹</h3>
          </div>
          <div class="reviews">
            <div class="review-box">
              <img src="images/ta-share/1051/18/1.jpg" data-img="images/ta-share/1051/18/1.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1051/18/2.jpg" data-img="images/ta-share/1051/18/2.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1051/18/3.jpg" data-img="images/ta-share/1051/18/3.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1051/18/4.jpg" data-img="images/ta-share/1051/18/4.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1051/18/5.jpg" data-img="images/ta-share/1051/18/5.jpg" />
            </div>

          </div>
        </li>
        <li>
          <div class="info">
            <img src="images/team/ta-17.jpg">
            <h3>卜政嘉</h3>
          </div>
          <div class="reviews">
            <div class="review-box">
              <img src="images/ta-share/1051/17/1.jpg" data-img="images/ta-share/1051/17/1.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1051/17/2.jpg" data-img="images/ta-share/1051/17/2.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1051/17/3.jpg" data-img="images/ta-share/1051/17/3.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1051/17/4.jpg" data-img="images/ta-share/1051/17/4.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1051/17/5.jpg" data-img="images/ta-share/1051/17/5.jpg" />
            </div>

          </div>
        </li>
        <li>
          <div class="info">
            <img src="images/team/ta-14.jpg">
            <h3>吳佩璇</h3>
          </div>
          <div class="reviews">
            <div class="review-box">
              <img src="images/ta-share/1051/14/1.jpg" data-img="images/ta-share/1051/14/1.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1051/14/2.jpg" data-img="images/ta-share/1051/14/2.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1051/14/3.jpg" data-img="images/ta-share/1051/14/3.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1051/14/4.jpg" data-img="images/ta-share/1051/14/4.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1051/14/5.jpg" data-img="images/ta-share/1051/14/5.jpg" />
            </div>

          </div>
        </li>

        <li>
          <div class="info">
            <img src="images/team/ta-16.jpg">
            <h3>林建宇</h3>
          </div>
          <div class="reviews">
            <div class="review-box">
              <img src="images/ta-share/1051/16/1.jpg" data-img="images/ta-share/1051/16/1.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1051/16/2.jpg" data-img="images/ta-share/1051/16/2.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1051/16/3.jpg" data-img="images/ta-share/1051/16/3.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1051/16/4.jpg" data-img="images/ta-share/1051/16/4.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1051/16/5.jpg" data-img="images/ta-share/1051/16/5.jpg" />
            </div>

          </div>
        </li>
        <li>
          <div class="info">
            <img src="images/team/ta-11.jpg">
            <h3>洪梅馨</h3>
          </div>
          <div class="reviews">
            <div class="review-box">
              <img src="images/ta-share/1051/11/1.jpg" data-img="images/ta-share/1051/11/1.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1051/11/2.jpg" data-img="images/ta-share/1051/11/2.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1051/11/3.jpg" data-img="images/ta-share/1051/11/3.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1051/11/4.jpg" data-img="images/ta-share/1051/11/4.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1051/11/5.jpg" data-img="images/ta-share/1051/11/5.jpg" />
            </div>

          </div>
        </li>
        <li>
          <div class="info">
            <img src="images/team/ta-15.jpg">
            <h3>游巧歆</h3>
          </div>
          <div class="reviews">
            <div class="review-box">
              <img src="images/ta-share/1051/15/1.jpg" data-img="images/ta-share/1051/15/1.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1051/15/2.jpg" data-img="images/ta-share/1051/15/2.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1051/15/3.jpg" data-img="images/ta-share/1051/15/3.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1051/15/4.jpg" data-img="images/ta-share/1051/15/4.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1051/15/5.jpg" data-img="images/ta-share/1051/15/5.jpg" />
            </div>

          </div>
        </li>
        <li>
          <div class="info">
            <img src="images/team/ta-05.jpg">
            <h3>柯奕吉</h3>
          </div>
          <div class="reviews">
            <div class="review-box">
              <img src="images/ta-share/1051/05/1.jpg" data-img="images/ta-share/1051/05/1.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1051/05/2.jpg" data-img="images/ta-share/1051/05/2.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1051/05/3.jpg" data-img="images/ta-share/1051/05/3.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1051/05/4.jpg" data-img="images/ta-share/1051/05/4.jpg" />
            </div>


          </div>
        </li>
        <li>
          <div class="info">
            <img src="images/team/ta-13.jpg">
            <h3>呂亞庭</h3>
          </div>
          <div class="reviews">
            <div class="review-box">
              <img src="images/ta-share/1051/13/1.jpg" data-img="images/ta-share/1051/13/1.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1051/13/2.jpg" data-img="images/ta-share/1051/13/2.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1051/13/3.jpg" data-img="images/ta-share/1051/13/3.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1051/13/4.jpg" data-img="images/ta-share/1051/13/4.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1051/13/5.jpg" data-img="images/ta-share/1051/13/5.jpg" />
            </div>

          </div>
        </li>


      </ul>

      <!-- ***************************************************** 分隔* **************************************************** -->

      <?php } else if ( $semester === "1052") { ?>
      <div class="ta-share-semester">
        <h2>1052學期</h2>
      </div>
      <ul>
        <li>
          <div class="info">
            <img src="images/team/ta-14.jpg">
            <h3>吳佩璇</h3>
          </div>
          <div class="reviews">
            <div class="review-box">
              <img src="images/ta-share/1052/14/1.jpg" data-img="images/ta-share/1052/14/1.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/14/2.jpg" data-img="images/ta-share/1052/14/2.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/14/3.jpg" data-img="images/ta-share/1052/14/3.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/14/4.jpg" data-img="images/ta-share/1052/14/4.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/14/5.jpg" data-img="images/ta-share/1052/14/5.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/14/6.jpg" data-img="images/ta-share/1052/14/6.jpg" />
            </div>
          </div>
        </li>
        <li>
          <div class="info">
            <img src="images/team/ta-21.jpg">
            <h3>凃諭伶</h3>
          </div>
          <div class="reviews">
            <div class="review-box">
              <img src="images/ta-share/1052/21/1.jpg" data-img="images/ta-share/1052/21/1.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/21/2.jpg" data-img="images/ta-share/1052/21/2.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/21/3.jpg" data-img="images/ta-share/1052/21/3.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/21/4.jpg" data-img="images/ta-share/1052/21/4.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/21/5.jpg" data-img="images/ta-share/1052/21/5.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/21/6.jpg" data-img="images/ta-share/1052/21/6.jpg" />
            </div>
          </div>
        </li>
        <li>
          <div class="info">
            <img src="images/team/ta-11.jpg">
            <h3>洪梅馨</h3>
          </div>
          <div class="reviews">
            <div class="review-box">
              <img src="images/ta-share/1052/11/1.jpg" data-img="images/ta-share/1052/11/1.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/11/2.jpg" data-img="images/ta-share/1052/11/2.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/11/3.jpg" data-img="images/ta-share/1052/11/3.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/11/4.jpg" data-img="images/ta-share/1052/11/4.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/11/5.jpg" data-img="images/ta-share/1052/11/5.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/11/6.jpg" data-img="images/ta-share/1052/11/6.jpg" />
            </div>
          </div>
        </li>
        <li>
          <div class="info">
            <img src="images/team/ta-19.jpg">
            <h3>陳雅伶</h3>
          </div>
          <div class="reviews">
            <div class="review-box">
              <img src="images/ta-share/1052/19/1.jpg" data-img="images/ta-share/1052/19/1.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/19/2.jpg" data-img="images/ta-share/1052/19/2.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/19/3.jpg" data-img="images/ta-share/1052/19/3.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/19/4.jpg" data-img="images/ta-share/1052/19/4.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/19/5.jpg" data-img="images/ta-share/1052/19/5.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/19/6.jpg" data-img="images/ta-share/1052/19/6.jpg" />
            </div>
          </div>
        </li>
        <li>
          <div class="info">
            <img src="images/team/ta-15.jpg">
            <h3>游巧歆</h3>
          </div>
          <div class="reviews">
            <div class="review-box">
              <img src="images/ta-share/1052/15/1.jpg" data-img="images/ta-share/1052/15/1.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/15/2.jpg" data-img="images/ta-share/1052/15/2.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/15/3.jpg" data-img="images/ta-share/1052/15/3.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/15/4.jpg" data-img="images/ta-share/1052/15/4.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/15/5.jpg" data-img="images/ta-share/1052/15/5.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/15/6.jpg" data-img="images/ta-share/1052/15/6.jpg" />
            </div>
          </div>
        </li>
        <li>
          <div class="info">
            <img src="images/team/ta-20.jpg">
            <h3>羅亦</h3>
          </div>
          <div class="reviews">
            <div class="review-box">
              <img src="images/ta-share/1052/20/1.jpg" data-img="images/ta-share/1052/20/1.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/20/2.jpg" data-img="images/ta-share/1052/20/2.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/20/3.jpg" data-img="images/ta-share/1052/20/3.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/20/4.jpg" data-img="images/ta-share/1052/20/4.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/20/5.jpg" data-img="images/ta-share/1052/20/5.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/20/6.jpg" data-img="images/ta-share/1052/20/6.jpg" />
            </div>
          </div>
        </li>
        <li>
          <div class="info">
            <img src="images/team/ta-13.jpg">
            <h3>呂亞庭</h3>
          </div>
          <div class="reviews">
            <div class="review-box">
              <img src="images/ta-share/1052/13/1.jpg" data-img="images/ta-share/1052/13/1.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/13/2.jpg" data-img="images/ta-share/1052/13/2.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/13/3.jpg" data-img="images/ta-share/1052/13/3.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/13/4.jpg" data-img="images/ta-share/1052/13/4.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/13/5.jpg" data-img="images/ta-share/1052/13/5.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/13/6.jpg" data-img="images/ta-share/1052/13/6.jpg" />
            </div>
          </div>
        </li>
        <li>
          <div class="info">
            <img src="images/team/ta-05.jpg">
            <h3>柯奕吉</h3>
          </div>
          <div class="reviews">
            <div class="review-box">
              <img src="images/ta-share/1052/05/1.jpg" data-img="images/ta-share/1052/05/1.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/05/2.jpg" data-img="images/ta-share/1052/05/2.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/05/3.jpg" data-img="images/ta-share/1052/05/3.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/05/4.jpg" data-img="images/ta-share/1052/05/4.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/05/5.jpg" data-img="images/ta-share/1052/05/5.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1052/05/6.jpg" data-img="images/ta-share/1052/05/6.jpg" />
            </div>
          </div>
        </li>
      </ul>
      <!-- ***************************************************** 分隔* **************************************************** -->

      <?php } else if ( $semester === "1061") { ?>
      <div class="ta-share-semester">
        <h2>1061學期</h2>
      </div>
      <ul>
        <li>
          <div class="info">
            <img src="images/team/ta-35.jpg">
            <h3>江蒂容</h3>
          </div>
          <div class="reviews">
            <div class="review-box">
              <img src="images/ta-share/1061/35/1.jpg" data-img="images/ta-share/1061/35/1.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1061/35/2.jpg" data-img="images/ta-share/1061/35/2.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1061/35/3.jpg" data-img="images/ta-share/1061/35/3.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1061/35/4.jpg" data-img="images/ta-share/1061/35/4.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1061/35/5.jpg" data-img="images/ta-share/1061/35/5.jpg" />
            </div>

          </div>
        </li>
        <li>
          <div class="info">
            <img src="images/team/ta-31.jpg">
            <h3>吳佳樺</h3>
          </div>
          <div class="reviews">
            <div class="review-box">
              <img src="images/ta-share/1061/31/1.jpg" data-img="images/ta-share/1061/31/1.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1061/31/2.jpg" data-img="images/ta-share/1061/31/2.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1061/31/3.jpg" data-img="images/ta-share/1061/31/3.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1061/31/4.jpg" data-img="images/ta-share/1061/31/4.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1061/31/5.jpg" data-img="images/ta-share/1061/31/5.jpg" />
            </div>
          </div>
        </li>
        <li>
          <div class="info">
            <img src="images/team/ta-32.jpg">
            <h3>洪鈞頎</h3>
          </div>
          <div class="reviews">
            <div class="review-box">
              <img src="images/ta-share/1061/32/1.jpg" data-img="images/ta-share/1061/32/1.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1061/32/2.jpg" data-img="images/ta-share/1061/32/2.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1061/32/3.jpg" data-img="images/ta-share/1061/32/3.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1061/32/4.jpg" data-img="images/ta-share/1061/32/4.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1061/32/5.jpg" data-img="images/ta-share/1061/32/5.jpg" />
            </div>
          </div>
        </li>
        <li>
          <div class="info">
            <img src="images/team/ta-33.jpg">
            <h3>張宸禎</h3>
          </div>
          <div class="reviews">
            <div class="review-box">
              <img src="images/ta-share/1061/33/1.jpg" data-img="images/ta-share/1061/33/1.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1061/33/2.jpg" data-img="images/ta-share/1061/33/2.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1061/33/3.jpg" data-img="images/ta-share/1061/33/3.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1061/33/4.jpg" data-img="images/ta-share/1061/33/4.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1061/33/5.jpg" data-img="images/ta-share/1061/33/5.jpg" />
            </div>
          </div>
        </li>
        <li>
          <div class="info">
            <img src="images/team/ta-44.jpg">
            <h3>游巧歆</h3>
          </div>
          <div class="reviews">
            <div class="review-box">
              <img src="images/ta-share/1061/44/1.jpg" data-img="images/ta-share/1061/44/1.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1061/44/2.jpg" data-img="images/ta-share/1061/44/2.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1061/44/3.jpg" data-img="images/ta-share/1061/44/3.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1061/44/4.jpg" data-img="images/ta-share/1061/44/4.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1061/44/5.jpg" data-img="images/ta-share/1061/44/5.jpg" />
            </div>
          </div>
        </li>
        <li>
          <div class="info">
            <img src="images/team/ta-36.jpg">
            <h3>董奕岑</h3>
          </div>
          <div class="reviews">
            <div class="review-box">
              <img src="images/ta-share/1061/36/1.jpg" data-img="images/ta-share/1061/36/1.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1061/36/2.jpg" data-img="images/ta-share/1061/36/2.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1061/36/3.jpg" data-img="images/ta-share/1061/36/3.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1061/36/4.jpg" data-img="images/ta-share/1061/36/4.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1061/36/5.jpg" data-img="images/ta-share/1061/36/5.jpg" />
            </div>
          </div>
        </li>
        <li>
          <div class="info">
            <img src="images/team/ta-34.jpg">
            <h3>劉姿敏</h3>
          </div>
          <div class="reviews">
            <div class="review-box">
              <img src="images/ta-share/1061/34/1.jpg" data-img="images/ta-share/1061/34/1.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1061/34/2.jpg" data-img="images/ta-share/1061/34/2.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1061/34/3.jpg" data-img="images/ta-share/1061/34/3.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1061/34/4.jpg" data-img="images/ta-share/1061/34/4.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1061/34/5.jpg" data-img="images/ta-share/1061/34/5.jpg" />
            </div>
          </div>
        </li>
        <li>
          <div class="info">
            <img src="images/team/ta-20.jpg">
            <h3>羅亦</h3>
          </div>
          <div class="reviews">
            <div class="review-box">
              <img src="images/ta-share/1061/20/1.jpg" data-img="images/ta-share/1061/20/1.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1061/20/2.jpg" data-img="images/ta-share/1061/20/2.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1061/20/3.jpg" data-img="images/ta-share/1061/20/3.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1061/20/4.jpg" data-img="images/ta-share/1061/20/4.jpg" />
            </div>
            <div class="review-box">
              <img src="images/ta-share/1061/20/5.jpg" data-img="images/ta-share/1061/20/5.jpg" />
            </div>
          </div>
        </li>
      </ul>

      <!--年度資料往上插!-->
      <?php } ?>

    </div>
  </div>
</section>
<footer id="footer">
  <div class="subfooter">
    <div class="container">
      <div class="row">
        <div class="col-xxs-12 col-md-7">
          <div class="footer-nav">
            <ul>
              <li><a href="index.php">首頁</a></li>
              <li><a href="news.php">最新消息</a></li>
              <li><a href="intro.php">課程計畫</a></li>
              <li><a href="forums.php">討論區</a></li>
              <li><a href="works.php">優秀作品</a></li>
              <li><a href="picture.php">影像紀錄</a></li>
              <li><a href="team.php">核心團隊</a></li>
            </ul>
          </div>
        </div>
        <div class="col-xxs-12 col-md-5">
          <div class="school">
            <p>亞東技術學院 通識教育中心 OIT Center for General Education</p>
          </div>
          <div class="plan">
            <p>文韻亞東 ─ 中文閱讀書寫課程革新計畫</p>
          </div>
          <div class="hss">
            <p>本課程由教育部資訊及科技教育司支持</p>
          </div>
        </div>
      </div>
    </div> <!-- container -->
  </div> <!-- subfooter -->
  <div class="copyright">
    <div class="container">
      <div class="text">
        <small>
          <address class="author">
            Copyright  2015-2016.
          </address>
          <a href="http://gecw.oit.edu.tw/bin/home.php" title="亞東技術學院通識教育中心" target="_blank">亞東技術學院通識教育中心</a> All Rights Reserved.
        </small>
      </div>
      <div class="total">
        <?php require_once "./statistics.php"; ?>
      </div>
    </div>
  </div>
</footer>
<div id="ta-lightbox" class="ta-lightobx">
  <div class="ta-lightobx-center">
    <img src="">
  </div>
</div>
<div id="outdated"></div> <!-- outdatedbrowser 檢查瀏覽器 -->
<script src="js/style.js"></script>
<script type="text/javascript">
  document.getElementById("team-select").addEventListener("change", function() {
    document.location.href= "feedback.php?semester=" + this.value;
  });
</script>
<script src="outdatedbrowser/dom-outdatedbrowser.js"></script> <!-- outdatedbrowser 檢查瀏覽器 -->
<script src="js/tashare.js"></script>
</body>
</html>
