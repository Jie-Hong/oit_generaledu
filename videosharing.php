<?php
  require_once "./php/functions.php";

  $semester = $_GET['semester'];

  if (isset($semester)) {
    if ( $semester === "104" || $semester === "105" ||$semester === "106") {
    } else {
      header('Location: 404.php');
    }
  } else {
    header('Location: videosharing.php?semester=106');
  }
?>
<!DOCTYPE html>
<html lang="zh-Hant-TW">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, , maximum-scale=1">
  <title>
    <?php echo $semester ?> 活動集錦 - 文韻亞東</title>
  <link rel="stylesheet" type="text/css" href="css/videopopup.css" media="screen" />
  <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css"> <!-- Font-Awesome -->
  <link rel="stylesheet" href="outdatedbrowser/outdatedbrowser.css"> <!-- outdatedbrowser 檢查瀏覽器 -->
  <script src="outdatedbrowser/outdatedbrowser.js"></script> <!-- outdatedbrowser 檢查瀏覽器 -->
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <!-- Video.js  -->
  <link href="https://vjs.zencdn.net/7.1.0/video-js.css" rel="stylesheet">
</head>

<body>
  <header id="header">
    <section class="header-site-channel">
      <div class="container">
        <div class="header-channel-content">
          <div class="header-logo">
            <a href="index.php"><img src="images/logo.png" /></a>
            <span>本課程由教育部資訊及科技教育司支持</span>
            <div class="header-video">
              <a href="./news-detail.php?id=24">
                <!-- <img src="./video/1042.jpg"> -->
                <!-- <i class="fa fa-play-circle-o"></i> -->
              </a>
            </div>
          </div>
          <?php
        if($loggedin)
        {
        echo
          "<div class=\"header-login\">
             <ul>
               <li><a>您好，$user</a></li>
               <li><a href=\"user-edito.php\">會員編輯</a></li>
               <li><a href=\"admin/index.php\">後台管理</a></li>
               <li><a href=\"logout.php\">登出</a></li>
             </ul>
           </div>";
        }
        else
        {
          echo
            "<div class=\"header-login\">
               <ul>
                 <li><a href=\"login.php\">登入</a></li>
                 <li><a href=\"register.php\">註冊</a></li>
               </ul>
             </div>";
        }
        ?>
        </div>
      </div>
    </section>
    <nav class="navbar" role="navigation">
      <div class="container">
        <div class="navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="news.php">最新消息</a></li>
            <li><a href="intro.php">課程計畫</a></li>
            <li><a href="forums.php">討論區</a></li>
            <li><a href="vote.php">投票活動</a></li>
            <li><a href="memory.php">亞東印記</a></li>
            <li><a href="works.php">優秀作品</a></li>
            <li><a href="picture.php">影像紀錄</a></li>
            <li><a href="activity.php">活動集錦</a></li>
            <li class="active"><a href="videosharing.php">影片分享</a></li>
            <li><a href="feedback.php">TA回饋分享</a></li>
            <li><a href="team.php">核心團隊</a></li>
            <li><a href="links.php">相關資源</a></li>
          </ul>
        </div>
      </div>
    </nav>
  </header>

  <section class="activity">
    <div class="container">
      <h1><span>Video</span>影片分享</h1>
    </div>
  </section>
  <section class="ta-main">
    <div class="container">
      <div class="breadcrumb">
        <ul>
          <li><a href="index.php">首頁</a></li>
          <li><a href="videosharing.php">影片分享</a></li>
          <!-- <li><a href="videosharing.php?semester=<?php //echo $semester ?>">第
              <?php //echo $semester ?>學期</a></li> -->
        </ul>
      </div>

      <div class="team-select">
        <select id="team-select" class="team-dropdown" onchange="window.location.href=this.value">
          <option value="#106">106</option>
          <option value="#105">105</option>
          <option value="#104">104</option>
          <!-- <option value="106" <?php //if ($semester==="106" ){ echo "selected" ; }?>>106 學期</option> -->
          <!-- <option value="105" <?php //if ($semester==="105" ){ echo "selected" ; }?>>105 學期</option> -->
          <!-- <option value="104" <?php //if ($semester==="104" ){ echo "selected" ; }?>>104 學期</option> -->
        </select>
      </div>
      <div class="activity-content">
        <div class="row">
          <div class="col-xs-12">
            <h2 id="106">106</h2>
            <div class="row">
              <div class=" col-xs-6 ">
                <div id="instructions" style="height:450px">
                  <video id="my_video_1" class="video-js vjs-default-skin" controls preload="none" poster='./images/oit.jpeg'
                    data-setup='{ "aspectRatio":"640:450", "playbackRates": [1, 1.5, 2] }'>
                    <source src="./video/106-1.mp4" type='video/mp4' />
                  </video>
                </div>
              </div>

              <div class=" col-xs-6 ">
                <div id="instructions" style="height:450px">
                  <video id="my_video_1" class="video-js vjs-default-skin" controls preload="none" poster='./images/oit.jpeg'
                    data-setup='{ "aspectRatio":"640:450", "playbackRates": [1, 1.5, 2] }'>
                    <source src="./video/106-2.mp4" type='video/mp4' />
                  </video>
                </div>
              </div>
            </div>

          </div>
        </div>

        <div class="row">
          <div class="col-xs-12">
            <h2 id="105">105</h2>
            <div class="row">
              <div class=" col-xs-6 ">
                <div id="instructions" style="height:450px">
                  <video id="my_video_1" class="video-js vjs-default-skin" controls preload="none" poster='./images/oit.jpeg'
                    data-setup='{ "aspectRatio":"640:450", "playbackRates": [1, 1.5, 2] }'>
                    <source src="./video/105-1.mp4" type='video/mp4' />
                  </video>
                </div>
              </div>
            

            <div class=" col-xs-6 ">
              <div id="instructions" style="height:450px">
                <video id="my_video_1" class="video-js vjs-default-skin" controls preload="none" poster='./images/oit.jpeg'
                  data-setup='{ "aspectRatio":"640:450", "playbackRates": [1, 1.5, 2] }'>
                  <source src="./video/105-2.mp4" type='video/mp4' />
                </video>
              </div>
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12">
            <h2 id="104">104</h2>
            <div class="row">
              <div class=" col-xs-6 ">
                <div id="instructions" style="height:450px">
                  <video id="my_video_1" class="video-js vjs-default-skin" controls preload="none" poster='./images/oit.jpeg'
                    data-setup='{ "aspectRatio":"640:450", "playbackRates": [1, 1.5, 2] }'>
                    <source src="./video/104-1.mp4" type='video/mp4' />
                  </video>
                </div>
              </div>
            </div>
          </div>
        </div>


      </div>
    </div>
  </section>


  <footer id="footer">
    <div class="subfooter">
      <div class="container">
        <div class="row">
          <div class="col-xxs-12 col-md-7">
            <div class="footer-nav">
              <ul>
                <li><a href="index.php">首頁</a></li>
                <li><a href="news.php">最新消息</a></li>
                <li><a href="intro.php">課程計畫</a></li>
                <li><a href="forums.php">討論區</a></li>
                <li><a href="works.php">優秀作品</a></li>
                <li><a href="picture.php">影像紀錄</a></li>
                <li><a href="team.php">核心團隊</a></li>
              </ul>
            </div>
          </div>
          <div class="col-xxs-12 col-md-5">
            <div class="school">
              <p>亞東技術學院 通識教育中心 OIT Center for General Education</p>
            </div>
            <div class="plan">
              <p>文韻亞東 ─ 中文閱讀書寫課程革新計畫</p>
            </div>
            <div class="hss">
              <p>本課程由教育部資訊及科技教育司支持</p>
            </div>
          </div>
        </div>
      </div> <!-- container -->
    </div> <!-- subfooter -->
    <div class="copyright">
      <div class="container">
        <div class="text">
          <small>
            <address class="author">
              Copyright 2015-2016.
            </address>
            <a href="http://gecw.oit.edu.tw/bin/home.php" title="亞東技術學院通識教育中心" target="_blank">亞東技術學院通識教育中心</a> All
            Rights Reserved.
          </small>
        </div>
        <div class="total">
          <?php require_once "./statistics.php"; ?>
        </div>
      </div>
    </div>
  </footer>
  <div id="outdated"></div> <!-- outdatedbrowser 檢查瀏覽器 -->
  <script src="https://vjs.zencdn.net/7.1.0/video.js"></script>
  <script src="js/style.js"></script>

  <script type="text/javascript">
    //document.getElementById("team-select").addEventListener("change", function () {
    //document.location.href = "videosharing.php?semester=" + this.value;
    //});
  </script>

  <script src="outdatedbrowser/dom-outdatedbrowser.js"></script> <!-- outdatedbrowser 檢查瀏覽器 -->
  <script type="text/javascript">
    <!-- 學期切換的事件聆聽javascript-->
    var abox = document.getElementsByClassName('activity-box');
    for (var i = 0; i < abox.length; i++) {
      abox[i].addEventListener("click", function () {
        aheightbox(this);
      }, false);
    }

    function aheightbox(e) {
      console.log(e)
      e.getElementsByClassName('info')[0].classList.toggle('open');
    }
  </script>
</body>

</html>