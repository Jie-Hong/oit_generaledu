<?php
  require_once "./php/functions.php";
  //確認getID為整數
  urlCheckID();

  $nID = $_GET['id'];
  if(isset($nID))
  {
    $sql = "SELECT * FROM `web_news` LEFT JOIN `web_member` ON `web_news`.`newsPublisher` = `web_member`.`memberAccount` WHERE `newsID` =" . $nID;
    $result = $db->query($sql);
    //db = null;
    //無查詢結果轉跳news.php
    if($result->rowCount() == 0)
    { 
      header('Location: news.php');
    }
  } else {
    header('Location: 404.php');
  }
  $row = $result->fetch();
  $title=htmlspecialchars($row['newsTitle'], ENT_QUOTES);
  $content=htmlspecialchars($row['newsContent'], ENT_QUOTES);
  $date=substr($row['newsDate'],0,10);
  $updateViewer=$row['newsViewer']+1;
  $updateID=$row['newsID'];
  $sqlUpdateViewer = "UPDATE `web_news` SET `newsViewer` ='$updateViewer' WHERE `newsID` = '$updateID'";
  $updateResult = $db->query($sqlUpdateViewer);
  
/*
  $nID = $_GET['id'];
  $query  = "SELECT * FROM web_news WHERE newsID =" . $nID;
  $result = $conn->query($query);
  if (!$result) die($conn->error);
  $rows = $result->num_rows;
  */
/*
 for ($j = 0 ; $j < $rows ; ++$j)
  {
    $result->data_seek($j);
    echo 'newsID: '   . $result->fetch_assoc()['newsID']   . '<br>';
    $result->data_seek($j);
    echo 'newsType: '    . $result->fetch_assoc()['newsType']    . '<br>';
    $result->data_seek($j);
    echo 'newsViewer: ' . $result->fetch_assoc()['newsViewer'] . '<br>';
    $result->data_seek($j);
    echo 'newsTitle: '     . $result->fetch_assoc()['newsTitle']     . '<br>';
    $result->data_seek($j);
    echo 'newsPublisher: '     . $result->fetch_assoc()['newsPublisher']     . '<br>';
    $result->data_seek($j);
    echo 'newsDate: '     . $result->fetch_assoc()['newsDate']     . '<br><br>';  
  }
*/
?>
<!DOCTYPE html>
<html lang="zh-Hant-TW">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, , maximum-scale=1">
  <title><?php echo $title;?> - 最新消息 - 文韻亞東</title>
  <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css"> <!-- Font-Awesome -->
  <link rel="stylesheet" href="outdatedbrowser/outdatedbrowser.css"> <!-- outdatedbrowser 檢查瀏覽器 -->
  <script src="outdatedbrowser/outdatedbrowser.js"></script> <!-- outdatedbrowser 檢查瀏覽器 -->
  <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<header id="header">
  <section class="header-site-channel">
    <div class="container">
      <div class="header-channel-content">
        <div class="header-logo">
          <a href="index.php"><img src="images/logo.png" /></a>
          <span>本課程由教育部資訊及科技教育司支持</span>
          <div class="header-video">
            <a href="./news-detail.php?id=24">
              <!-- <img src="./video/1042.jpg"> -->
              <!-- <i class="fa fa-play-circle-o"></i> -->
            </a>
          </div>
        </div>
        <?php
        if($loggedin)
        {
        echo
          "<div class=\"header-login\">
             <ul>
               <li><a>您好，$user</a></li>
               <li><a href=\"user-edito.php\">會員編輯</a></li>
               <li><a href=\"admin/index.php\">後台管理</a></li>
               <li><a href=\"logout.php\">登出</a></li>
             </ul>
           </div>";
        }
        else
        {
          echo
            "<div class=\"header-login\">
               <ul>
                 <li><a href=\"login.php\">登入</a></li>
                 <li><a href=\"register.php\">註冊</a></li>
               </ul>
             </div>";    
        }
        ?>
      </div>
    </div>
  </section>
  <nav class="navbar" role="navigation">
    <div class="container">
      <div class="navbar-collapse">
        <ul class="nav navbar-nav">
          <li class="active"><a href="news.php">最新消息</a></li>          
          <li><a href="intro.php">課程計畫</a></li>
          <li><a href="forums.php">討論區</a></li>
          <li><a href="vote.php">投票活動</a></li>
          <li><a href="memory.php">亞東印記</a></li>
          <li><a href="works.php">優秀作品</a></li>          
          <li><a href="picture.php">影像紀錄</a></li>
          <li><a href="activity.php">活動集錦</a></li>
          <li><a href="feedback.php">TA回饋分享</a></li>
          <li><a href="team.php">核心團隊</a></li>
          <li><a href="links.php">相關資源</a></li>
        </ul>
      </div>
    </div>
  </nav>
</header>
<section class="news-title">
  <div class="container">
    <h1><span>What's News</span>最新消息</h1>
  </div>
</section>
<article class="news-main">
<?php
  echo
  "<div class=\"container\">
    <div class=\"breadcrumb\">
      <ul>
        <li><a href=\"index.php\">首頁</a></li>
        <li><a href=\"news.php\">最新消息</a></li>
        <li><a href=\"news-detail.php?id=$nID\">$title</a></li>
      </ul>
    </div>
    <div class=\"news-detail\">
      <div class=\"news-detail-date\">$date</div>
      <h1>$title</h1>
      <div class=\"news-detail-info\">
        <span class=\"author\"><i class=\"fa fa-user\"></i>${row['memberNickname']}</span>
        <span class=\"view\"><i class=\"fa fa-eye\"></i> {$row['newsViewer']}</span>
      </div>
      <div class=\"news-detail-content\">
       {$row['newsContent']}
      </div>
      <div class=\"news-detail-back\">
        <a href=\"news.php\"><i class=\"fa fa-arrow-circle-o-left\"></i></a>
      </div>
    </div>
  </div>";
?>
</article>
<footer id="footer">
  <div class="subfooter">
    <div class="container">
      <div class="row">
        <div class="col-xxs-12 col-md-7">
          <div class="footer-nav">
            <ul>
              <li><a href="index.php">首頁</a></li>
              <li><a href="news.php">最新消息</a></li>
              <li><a href="intro.php">課程計畫</a></li>
              <li><a href="forums.php">討論區</a></li>
              <li><a href="works.php">優秀作品</a></li>
              <li><a href="picture.php">影像紀錄</a></li>
              <li><a href="team.php">核心團隊</a></li>
            </ul>
          </div>
        </div>
        <div class="col-xxs-12 col-md-5">
          <div class="school">
            <p>亞東技術學院 通識教育中心 OIT Center for General Education</p>
          </div>
          <div class="plan">
            <p>文韻亞東 ─ 中文閱讀書寫課程革新計畫</p>
          </div>          
          <div class="hss">
            <p>本課程由教育部資訊及科技教育司支持</p>
          </div>
        </div>
      </div>
    </div> <!-- container -->
  </div> <!-- subfooter -->
  <div class="copyright">
    <div class="container">
      <div class="text">
        <small>
          <address class="author">
            Copyright  2015-2016. 
          </address>
          <a href="http://gecw.oit.edu.tw/bin/home.php" title="亞東技術學院通識教育中心" target="_blank">亞東技術學院通識教育中心</a> All Rights Reserved.
        </small> 
      </div>
      <div class="total">
        <?php require_once "./statistics.php"; ?>
      </div>
    </div>     
  </div>
</footer>
<div id="outdated"></div> <!-- outdatedbrowser 檢查瀏覽器 -->
<script src="js/style.js"></script>
<script src="outdatedbrowser/dom-outdatedbrowser.js"></script> <!-- outdatedbrowser 檢查瀏覽器 -->
</body>
</html>