<?php
  require_once "./php/functions.php";
  date_default_timezone_set('Asia/Taipei');
  urlCheckID();
  $eID = $_GET['id'];

  function creationVoteCount($numberRecord,$eID,$db)
  {

    $sqlSelectCount = "SELECT count(recordNumber)as creationVote FROM vote_record WHERE recordEventID = '$eID' AND recordNumber = '$numberRecord' AND recordVisible = 1";
    $resultSelectCount = $db->query($sqlSelectCount);
    $rowSelectCount = $resultSelectCount->fetch();
    $creationVote = $rowSelectCount['creationVote'];

    if ($creationVote != 0)
    {
      $sqlUpdataCount = "UPDATE vote_creation SET creationVote = '$creationVote' WHERE creationEventID = '$eID' AND creationNumber = '$numberRecord'";
      $updateCountResult = $db->query($sqlUpdataCount);
    }

  }

function studentidChecker($str,$eID,$db)
{
  $sql = "SELECT count(recordStudentID) AS sidcount FROM vote_record WHERE recordEventID = '$eID' AND recordStudentID= '$str' AND recordVisible = 1";
  $result = $db->query($sql);
  $row = $result->fetch();
  $count = $row['sidcount'];
  return $count;
}
function dateChecker($dateNow,$date)
{
  if(strtotime($dateNow)>strtotime($date))
  {
    $dateCheck = 1;
  }
  else
  {
    $dateCheck = 0;
  }
  return $dateCheck;
}
function ClassChecker($classRecord) {
  /* 1042 */
  switch($classRecord){
    case "1062-ha-a":
    case '1062-ha-b':
    case '1062-ce-a':
    case '1062-ce-b':
    case '1062-mi-a':
    case '1062-mi-b':
    case '1062-mt-a':
    case '1062-mt-b':
    case '1062-md-a':
    case '1062-md-b':
      $CheckClassYN = 0;
      break;
    default:
      $CheckClassYN = 1;
  }
  return $CheckClassYN;
}


  if(isset($eID))
  {
    $sqlEventGet = "SELECT * FROM vote_event LEFT JOIN web_member ON vote_event.eventPublisher = web_member.memberAccount where eventID = '$eID'";
    $result = $db->query($sqlEventGet);
    $row = $result->fetch();
    if($result->rowCount() == 0)
    {
      header('Location: vote.php');
    }
    if($result->rowCount() !== false)
    {
      $eventID        = $row['eventID'];
      $eventTitle     = $row['eventSubject'];
      $eventDate      = $row['eventDate'];
      $eventInfo      = $row['eventInfo'];
      $eventHot       = $row['eventViewer'];
      $eventPublisher = $row['memberNickname'];
      $eventIamge     = $row['eventImagePath'];
      //updata viewer
      $updateEventViewer=$row['eventViewer']+1;
      $sqlUpdateViewer = "UPDATE vote_event SET eventViewer ='$updateEventViewer' WHERE eventID = '$eventID'";
      $updateResult = $db->query($sqlUpdateViewer);

          if(isset($_POST['voteid']) && isset($eID))
          {
            $myip = get_client_ip();
            $sqlInsertRecord = "INSERT INTO vote_record(recordID, recordEventID, recordClass, recordStudentID, recordPublisher, recordContent, recordDate, recordIP, recordVisible, recordNumber) VALUES (null,?,?,?,?,?,?,?,?,?)";
            $statement = $db->prepare($sqlInsertRecord);
            $classRecord = filter_input(INPUT_POST,'class-select');
            $studentidRecord = filter_input(INPUT_POST,'vote-studentid', FILTER_VALIDATE_INT);
            $publisherRecord = filter_input(INPUT_POST,'vote-name');
            $publisherRecord = htmlentities($publisherRecord,ENT_QUOTES,'UTF-8');
            $contentRecord = filter_input(INPUT_POST,'text');
            $contentRecord = htmlentities($contentRecord,ENT_QUOTES,'UTF-8');
            $contentRecord = str_replace("\n","<br/>",$contentRecord);
            $dateRecord = date('Y-m-d H:i:s');
            $ipRecord = $myip;
            $visibleRecord = '1';
            $numberRecord = filter_input(INPUT_POST,'voteid', FILTER_VALIDATE_INT);
            $studentCheck=studentidChecker($studentidRecord,$eID,$db);
            $dateCheck=dateChecker($dateRecord,$eventDate);
            $classRecordCheck=ClassChecker($classRecord);

            if(!$publisherRecord == 0 &&  !$contentRecord == 0 && !$dateRecord == 0 && !$ipRecord == 0 && !$numberRecord == 0 && !$visibleRecord == 0 && $studentCheck == 0 && $dateCheck == 0 && $classRecordCheck == 0 )
            {
              $statement->bindValue('1', $eID, PDO::PARAM_INT);
              $statement->bindValue('2', $classRecord);
              $statement->bindValue('3', $studentidRecord);
              $statement->bindValue('4', $publisherRecord);
              $statement->bindValue('5', $contentRecord);
              $statement->bindValue('6', $dateRecord);
              $statement->bindValue('7', $ipRecord);
              $statement->bindValue('8', $visibleRecord);
              $statement->bindValue('9', $numberRecord);
              $recordInsertResult = $statement->execute();
              creationVoteCount($numberRecord,$eID,$db);
              echo '<script type="text/javascript">
                alert("投票成功");
                </script>';
            } else if ($dateCheck != 0) {
              echo '<script type="text/javascript">
                alert("超過投票時間");
                </script>';
            } else if ($studentCheck != 0) {
              echo '<script type="text/javascript">
                alert("你已經投過囉!");
                </script>';
            } else if ( $classRecordCheck != 0 ) {
              echo '<script type="text/javascript">
                alert("班級錯誤，請檢查是否選擇班級!");
                </script>';
            } else {
              echo '<script type="text/javascript">
                alert("失敗，錯誤代碼: A-0002");
                </script>';
            }
          }

      if($result->rowCount() !== false && isset($eventID))
      {
        $sqlCreationGet = "SELECT * FROM vote_creation where creationEventID = '$eventID' ORDER BY creationNumber ASC";
        $resultCreation = $db->query($sqlCreationGet);
        //$rowCreation = $resultCreation->fetch();
        $creationRowCount = $resultCreation->rowCount();

        $sqlRecordGet = "SELECT * FROM vote_record where recordEventID = '$eventID' ORDER BY recordID ASC";
        $resultRecord = $db->query($sqlRecordGet);
        //$rowRecord = $resultRecord->fetch();
        $recordRowCount = $resultRecord->rowCount();

        //rank
        $sqlCreationRank = "SELECT * FROM vote_creation WHERE creationEventID = '$eID' ORDER BY creationVote DESC";
        $resultCreationRank = $db->query($sqlCreationRank);
        $CreationRankRowCount = $resultCreationRank->rowCount();

        $sqlCreationRankSum = "SELECT sum(creationVote) AS sumRank FROM vote_creation WHERE creationEventID = '$eID'";
        $resultCreationRankSum = $db->query($sqlCreationRankSum);
        $rowCreationRankSum = $resultCreationRankSum->fetch();
        $rankTotal = $rowCreationRankSum['sumRank'];

        }
    }
  }
  else
  {
    header('Location: 404.php');
  }
?>
<!DOCTYPE html>
<html lang="zh-Hant-TW">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, , maximum-scale=1">
  <title><?php echo $eventTitle;?> - 投票活動 - 文韻亞東</title>
  <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css"> <!-- Font-Awesome -->
  <link rel="stylesheet" href="outdatedbrowser/outdatedbrowser.css"> <!-- outdatedbrowser 檢查瀏覽器 -->
  <script src="outdatedbrowser/outdatedbrowser.js"></script> <!-- outdatedbrowser 檢查瀏覽器 -->
  <script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <?php
  if ( $level < 5) {
    echo "<script src=\"./sweetalert-master/dist/sweetalert.min.js\"></script>";
    echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"./sweetalert-master/dist/sweetalert.css\">";
  }
  ?>
</head>
<body>
<header id="header">
  <section class="header-site-channel">
    <div class="container">
      <div class="header-channel-content">
        <div class="header-logo">
          <a href="index.php"><img src="images/logo.png" /></a>
          <span>本課程由教育部資訊及科技教育司支持</span>
          <div class="header-video">
            <a href="./news-detail.php?id=24">
              <!-- <img src="./video/1042.jpg"> -->
              <!-- <i class="fa fa-play-circle-o"></i> -->
            </a>
          </div>
        </div>
        <?php
        if($loggedin)
        {
        echo
          "<div class=\"header-login\">
             <ul>
               <li><a>您好，$user</a></li>
               <li><a href=\"user-edito.php\">會員編輯</a></li>
               <li><a href=\"admin/index.php\">後台管理</a></li>
               <li><a href=\"logout.php\">登出</a></li>
             </ul>
           </div>";
        }
        else
        {
          echo
            "<div class=\"header-login\">
               <ul>
                 <li><a href=\"login.php\">登入</a></li>
                 <li><a href=\"register.php\">註冊</a></li>
               </ul>
             </div>";
        }
        ?>
      </div>
    </div>
  </section>
  <nav class="navbar" role="navigation">
    <div class="container">
      <div class="navbar-collapse">
        <ul class="nav navbar-nav">
          <li><a href="news.php">最新消息</a></li>
          <li><a href="intro.php">課程計畫</a></li>
          <li><a href="forums.php">討論區</a></li>
          <li class="active"><a href="vote.php">投票活動</a></li>
          <li><a href="memory.php">亞東印記</a></li>
          <li><a href="works.php">優秀作品</a></li>
          <li><a href="picture.php">影像紀錄</a></li>
          <li><a href="activity.php">活動集錦</a></li>
          <li><a href="videosharing.php">影片分享</a></li>
          <li><a href="feedback.php">TA回饋分享</a></li>
          <li><a href="team.php">核心團隊</a></li>
          <li><a href="links.php">相關資源</a></li>
        </ul>
      </div>
    </div>
  </nav>
</header>
<section class="vote">
  <div class="container">
    <h1><span>Vote</span>投票活動</h1>
  </div>
</section>
<article class="ta-main">
  <div class="container">
    <div class="breadcrumb">
      <ul>
        <li><a href="index.php">首頁</a></li>
        <li><a href="vote.php">投票活動</a></li>
        <?php
        echo "<li><a href=\"vote-detail.php?id=$eventID\">$eventTitle</a></li>"
        ?>
      </ul>
    </div>
    <?php
    $eventDetail=<<<EODH
    <div class="vote-detail-content">
      <div class="row">
        <div class="col-lg-9 col-md-8">
          <div class="vote-container">
            <h1>$eventTitle</h1>
            <div class="vote-c-info">
              $eventInfo
            </div>

EODH;
      echo $eventDetail;
    ?>
          <div class="vote-gogo" id="vote-gogo">
            <h3>作品一覽：</h3>
            <ul>
           <?php
              while($rowCreation = $resultCreation->fetch()){
                $VoteBox=<<<EOBH
                <li data-id="{$rowCreation['creationNumber']}" data-burl="{$rowCreation['creationImagePathB']}">
                  <div class="vote-gogo-box">
                    <div class="image">
                      <img src="{$rowCreation['creationImagePath']}"/>
                    </div>
                    <span class="number">編號：{$rowCreation['creationNumber']}</span>
                    <span class="votes">{$rowCreation['creationVote']}票</span>
                    <a href="javascript: void(0)" class="vote-gogo-box-btn">投此作品</a>
                  </div>
                </li>
EOBH;
              echo $VoteBox;
              }
            ?>
              <!-- Vote box show example
              <li data-id="1">
                <div class="vote-gogo-box">
                  <div class="image">
                    <img src="./images/vote/2015-11-17/1.jpg"/>
                    <span class="number">編號：1</span>
                    <span class="votes">187票</span>
                  </div>
                  <a href="javascript: void(0)" class="vote-gogo-box-btn">投此作品</a>
                </div>
              </li>
              <li data-id="2">
                <div class="vote-gogo-box">
                  <div class="image">
                    <img src="./images/vote/2015-11-17/2.jpg"/>
                    <span class="number">編號：2</span>
                    <span class="votes">128票</span>
                  </div>
                  <a href="javascript: void(0)" class="vote-gogo-box-btn">投此作品</a>
                </div>
              </li>
              <li data-id="3">
                <div class="vote-gogo-box">
                  <div class="image">
                    <img src="./images/vote/2015-11-17/3.jpg"/>
                    <span class="number">編號：3</span>
                    <span class="votes">87票</span>
                  </div>
                  <a href="javascript: void(0)" class="vote-gogo-box-btn">投此作品</a>
                </div>
              </li>
              <li data-id="4">
                <div class="vote-gogo-box">
                  <div class="image">
                    <img src="./images/vote/2015-11-17/4.jpg"/>
                    <span class="number">編號：4</span>
                    <span class="votes">7票</span>
                  </div>
                  <a href="javascript: void(0)" class="vote-gogo-box-btn">投此作品</a>
                </div>
              </li>
              -->
            </ul>
 </div>
          </div>
          <div class="vote-comments" id="vote-comments">
            <div class="vote-all">總共有 <?php echo $rankTotal; ?> 人投票</div>
            <ul>
            <?php
            while($rowRecord=$resultRecord->fetch()){
              $mark = mb_substr($rowRecord['recordPublisher'],0,1,"UTF-8");
              $date=substr($rowRecord['recordDate'],0,16);
              $recordClass = classShow($rowRecord['recordClass']);
              $recordVisible = $rowRecord['recordVisible'];


              if ( $recordVisible == 1 && $level == 5) {
              //懶人寫法
              //遊客、正常

              $voteComment=<<<EOCH

            <li data-reclass="{$rowRecord['recordClass']}">
                <div class="comment-avatar">
                  <span>$mark</span>
                </div>
                <div class="comment-right">
                  <div class="comment-r-info">
                    <span class="comment-reply-class">$recordClass</span>
                    <span class="comment-reply-id">{$rowRecord['recordStudentID']}</span>
                    <span class="comment-reply-author">{$rowRecord['recordPublisher']}</span>
                    <span class="comment-reply-time">$date</span>
                  </div>
                  <div class="comment-r-content">
                    <span>投給編號：{$rowRecord['recordNumber']}</span>
                    <p>{$rowRecord['recordContent']}</p>
                  </div>
                </div>
              </li>
EOCH;
} else if ( $recordVisible == 2 && $level == 5 ) {
              //遊客、被屏蔽
              $voteComment=<<<EOCH
              <li data-reclass="{$rowRecord['recordClass']}">
                <div class="comment-avatar">
                  <span>$mark</span>
                </div>
                <div class="comment-right">
                  <div class="comment-r-info">
                    <span class="comment-reply-class">$recordClass</span>
                    <span class="comment-reply-id">{$rowRecord['recordStudentID']}</span>
                    <span class="comment-reply-author">{$rowRecord['recordPublisher']}</span>
                    <span class="comment-reply-time">$date</span>
                  </div>
                  <div class="comment-r-content adminDelete">
                    <div class="invalid">無效票</div>
                    <span>投給編號：{$rowRecord['recordNumber']}</span>
                    <p>{$rowRecord['recordContent']}</p>
                  </div>
                </div>
              </li>
EOCH;
} else if (  $recordVisible == 1 && $level < 5 ) {
              //管理員、正常
              $voteComment=<<<EOCH
              <li data-reclass="{$rowRecord['recordClass']}" data-recordnumber="{$rowRecord['recordNumber']}">
                <div class="comment-avatar">
                  <span>$mark</span>
                </div>
                <div class="comment-right">
                  <div class="comment-r-info">
                    <span class="comment-reply-class">$recordClass</span>
                    <span class="comment-reply-id">{$rowRecord['recordStudentID']}</span>
                    <span class="comment-reply-author">{$rowRecord['recordPublisher']}</span>
                    <span class="comment-reply-time">$date</span>
                  </div>
                  <div class="comment-r-content">
                    <span>投給編號：{$rowRecord['recordNumber']}</span>
                    <p>{$rowRecord['recordContent']}</p>
                  </div>
                  <div class="comment-r-admin">
                    <a class="setInvalid" href="javascript: void(0);" data-eventid="{$eID}" data-eventnumber="{$rowRecord['recordNumber']}" data-recordid="{$rowRecord['recordID']}" data-studentid="{$rowRecord['recordStudentID']}" data-studentname="{$rowRecord['recordPublisher']}">將 {$rowRecord['recordStudentID']} {$rowRecord['recordPublisher']} >> 設定為無效票</a>
                  </div>
                </div>
              </li>
EOCH;
} else if ( $recordVisible == 2 && $level < 5 ) {
              //管理員、被屏蔽
              $voteComment=<<<EOCH
              <li data-reclass="{$rowRecord['recordClass']}" data-recordnumber="{$rowRecord['recordNumber']}">
                <div class="comment-avatar">
                  <span>$mark</span>
                </div>
                <div class="comment-right">
                  <div class="comment-r-info">
                    <span class="comment-reply-class">$recordClass</span>
                    <span class="comment-reply-id">{$rowRecord['recordStudentID']}</span>
                    <span class="comment-reply-author">{$rowRecord['recordPublisher']}</span>
                    <span class="comment-reply-time">$date</span>
                  </div>
                  <div class="comment-r-content adminDelete">
                    <div class="invalid">無效票</div>
                    <span>投給編號：{$rowRecord['recordNumber']}</span>
                    <p>{$rowRecord['recordContent']}</p>
                  </div>
                </div>
              </li>
EOCH;

}
                echo $voteComment;
            }
              ?>

              <!--vote comment example
              <li data-reclass="1041-ce-a">
                <div class="comment-avatar">
                  <span>甘</span>
                </div>
                <div class="comment-right">
                  <div class="comment-r-info">
                    <span class="comment-reply-class">1041 通訊1A</span>
                    <span class="comment-reply-id">104109139</span>
                    <span class="comment-reply-author">甘謦榮</span>
                    <span class="comment-reply-time">2015-11-26 13:39</span>
                  </div>
                  <div class="comment-r-content">
                    <span>投給編號：7</span>
                    <p>每個人的「內在價值」和「外在價值」都有所不同</p>
                  </div>
                </div>
              </li>
              <li data-reclass="1041-ce-a">
                <div class="comment-avatar">
                  <span>甘</span>
                </div>
                <div class="comment-right">
                  <div class="comment-r-info">
                    <span class="comment-reply-class">1041 通訊1A</span>
                    <span class="comment-reply-id">104109139</span>
                    <span class="comment-reply-author">甘謦榮</span>
                    <span class="comment-reply-time">2015-11-26 13:39</span>
                  </div>
                  <div class="comment-r-content">
                    <span>投給編號：17</span>
                    <p>每個人的「內在價值」和「外在價值」都有所不同都有所不同都有所不同都有所不同都有所不同都有所不同都有所不同都有所不同</p>
                  </div>
                </div>
              </li>
              <li data-reclass="1041-ce-a">
                <div class="comment-avatar">
                  <span>甘</span>
                </div>
                <div class="comment-right">
                  <div class="comment-r-info">
                    <span class="comment-reply-class">1041 通訊1A</span>
                    <span class="comment-reply-id">104109139</span>
                    <span class="comment-reply-author">甘謦榮</span>
                    <span class="comment-reply-time">2015-11-26 13:39</span>
                  </div>
                  <div class="comment-r-content">
                    <span>投給編號：7</span>
                    <p>每個人的「內在價值」和「外在價值」都有所不同</p>
                  </div>
                </div>
              </li>
                -->
            </ul>
          </div>
        </div>
        <div class="col-lg-3 col-md-4">
        <?php
          $rightBanner=<<<EORH
          <div class="vote-banner">
            <img src="$eventIamge"/>
          </div>
          <div class="vote-right-info">
            <span class="user">發布人：$eventPublisher</span>
            <span class="date">截止時間：$eventDate</span>
            <span class="views">人氣：$eventHot</span>
          </div>
EORH;
          echo $rightBanner;
        ?>
          <!-- Event banner and right info example
          <div class="vote-banner">
            <img src="./images/vote/01.png"/>
          </div>
          <div class="vote-right-info">
            <span class="user">發布人：通識教育中心</span>
            <span class="date">時間：2016-01-31 23:59:00</span>
            <span class="views">人氣：152</span>
          </div>
          -->
          <div class="vote-rank">
            <ul>
            <?php
            $number=1;
            while($rowCreationRank = $resultCreationRank->fetch()){
              if($number<6)
              {
                $rankBg=$number;
              }
              else
              {
                $rankBg=6;
              }
              //Percent
              if($rankTotal != 0)
              {
                $votePercent = $rowCreationRank['creationVote']/$rankTotal*100;
              }
              else
              {
                $votePercent = 0;
              }

              $voteRank=<<<EORAH
              <li>
                <div class="bg" data-rank="vote-rank-bg-$rankBg" style=" width: $votePercent%;"></div>
                <div class="level"><span>$number</span></div>
                <div class="number">編號：{$rowCreationRank['creationNumber']}</div>
                <div class="percentage">{$rowCreationRank['creationVote']}票</div>
              </li>
EORAH;
              echo $voteRank;
              $number=$number+1;
            }
            ?>
              <!--Vote rank example
              <li>
                <div class="bg" data-rank="vote-rank-bg-1" style=" width: 60%;"></div>
                <div class="level"><span>1</span></div>
                <div class="number">編號：9</div>
                <div class="percentage">104票</div>
              </li>
              <li>
                <div class="bg" data-rank="vote-rank-bg-2" style=" width: 19%;"></div>
                <div class="level"><span>2</span></div>
                <div class="number">編號：10</div>
                <div class="percentage">98票</div>
              </li>
              <li>
                <div class="bg" data-rank="vote-rank-bg-3" style=" width: 16%;"></div>
                <div class="level"><span>3</span></div>
                <div class="number">編號：8</div>
                <div class="percentage">19票</div>
              </li>
              <li>
                <div class="bg" data-rank="vote-rank-bg-4" style=" width: 13%;"></div>
                <div class="level"><span>4</span></div>
                <div class="number">編號：19</div>
                <div class="percentage">4票</div>
              </li>
              <li>
                <div class="bg" data-rank="vote-rank-bg-5" style=" width: 11%;"></div>
                <div class="level"><span>5</span></div>
                <div class="number">編號：5</div>
                <div class="percentage">2票</div>
              </li>
              <li>
                <div class="bg" data-rank="vote-rank-bg-6" style=" width: 9%;"></div>
                <div class="level"><span>6</span></div>
                <div class="number">編號：7</div>
                <div class="percentage">1票</div>
              </li>
              <li>
               <div class="bg" data-rank="vote-rank-bg-6" style=" width: 7%;"></div>
                <div class="level"><span>7</span></div>
                <div class="number">編號：2</div>
                <div class="percentage">0票</div>
              </li>
              <li>
               <div class="bg" data-rank="vote-rank-bg-6" style=" width: 4%;"></div>
                <div class="level"><span>8</span></div>
                <div class="number">編號：6</div>
                <div class="percentage">0票</div>
              </li>
              -->
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</article>
<footer id="footer">
  <div class="subfooter">
    <div class="container">
      <div class="row">
        <div class="col-xxs-12 col-md-7">
          <div class="footer-nav">
            <ul>
              <li><a href="index.php">首頁</a></li>
              <li><a href="news.php">最新消息</a></li>
              <li><a href="intro.php">課程計畫</a></li>
              <li><a href="forums.php">討論區</a></li>
              <li><a href="works.php">優秀作品</a></li>
              <li><a href="picture.php">影像紀錄</a></li>
              <li><a href="team.php">核心團隊</a></li>
            </ul>
          </div>
        </div>
        <div class="col-xxs-12 col-md-5">
          <div class="school">
            <p>亞東技術學院 通識教育中心 OIT Center for General Education</p>
          </div>
          <div class="plan">
            <p>文韻亞東 ─ 中文閱讀書寫課程革新計畫</p>
          </div>
          <div class="hss">
            <p>本課程由教育部資訊及科技教育司支持</p>
          </div>
        </div>
      </div>
    </div> <!-- container -->
  </div> <!-- subfooter -->
  <div class="copyright">
    <div class="container">
      <div class="text">
        <small>
          <address class="author">
            Copyright  2015-2016.
          </address>
          <a href="http://gecw.oit.edu.tw/bin/home.php" title="亞東技術學院通識教育中心" target="_blank">亞東技術學院通識教育中心</a> All Rights Reserved.
        </small>
      </div>
      <div class="total">
        <?php require_once "./statistics.php"; ?>
      </div>
    </div>
  </div>
</footer>
<div id="vote-lightbox" class="vote-lightobx">
  <div class="vote-lightobx-left">
    <a href="" target="_blank"><img src=""></a>
  </div>
  <div class="vote-lightobx-right">
    <div class="vote-lightobx-right-form">
      <div class="vote-ppp">
        投給編號：<span></span>
      </div>
      <form id="vote" method="post" action="">
        <span>
          <select class="vote-dropdown" name="class-select" id="class-select" required>
            <option value="">------請選擇班級------</option>
                          <option value="1062-ha-a">1062 醫管1A</option>
                          <option value="1062-ha-b">1062 醫管1B</option>

                          <option value="1062-ce-a">1062 通訊1A</option>
                          <option value="1062-ce-b">1062 通訊1B</option>

                          <option value="1062-mi-a">1062 資管1A</option>
                          <option value="1062-mi-b">1062 資管1B</option>

                          <option value="1062-mt-a">1062 材纖1A</option>
                          <option value="1062-mt-b">1062 材纖1B</option>

                          <option value="1062-md-a">1062 行銷1A</option>
                          <option value="1062-md-b">1062 行銷1B</option>
          </select>
        </span>
        <span><input type="text" name="vote-studentid" class="vote-input" id="vote-studentid" placeholder="學號" maxlength="9" required/></span>
        <span><input type="text" name="vote-name" class="vote-input" id="vote-name" placeholder="姓名" required/></span>
        <textarea class="vote-text" name="text" id="vote-text" rows="4" placeholder="請輸入投票感想...20字以上" required></textarea>
        <input type="submit" id="vote-submit" hidden>
        <input class="vote-btn" id="vote-post" type="submit" value="投票">
        <input type="text" hidden name="voteid" id="voteid">
      </form>
    </div>
    <a href="javascript: void(0)" id="vote-lightobx-close">關閉</a>
  </div>
</div>
<div id="outdated"></div> <!-- outdatedbrowser 檢查瀏覽器 -->
<script src="js/style.js"></script>
<script src="outdatedbrowser/dom-outdatedbrowser.js"></script> <!-- outdatedbrowser 檢查瀏覽器 -->
<script src="js/vote.js"></script></body>
<?php
  if ( $level < 5){
?>
  <script type="text/javascript">
    window.addEventListener('load', function() {
      var voteComments = document.getElementById("vote-comments");
      var setInvalid = voteComments.getElementsByClassName("setInvalid");




      for ( var i = 0 ; i < setInvalid.length; i++ ) {
        setInvalid[i].addEventListener("click", function(e) {
          var studentID = e.target.dataset.studentid,
              studentName = e.target.dataset.studentname,
              recordID = e.target.dataset.recordid,
              eventid = e.target.dataset.eventid,
              eventnumber = e.target.dataset.eventnumber;
          swal({
            title: "你確定嗎？",
            text: "確定將 " + studentID + "/" + studentName + " 設定為無效票嗎?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, 設定為無效票!",
            closeOnConfirm: false
          },
          function(){
            $.ajax({
              type: "POST",
              async: true,
              url: "./php/AjaxVoteAdmin.php",
              data: {
                "recordid": recordID,
                "eventid":eventid,
                "eventnumber": eventnumber
              },
              success:function(data){
                if ( data == "ok") {
                  swal({
                    title: "修改成功！",
                    type: "success",
                    text: "將於3秒內進行轉址"
                  },
                  function(){
                    window.location.reload();
                  });
                  setTimeout( function(){ window.location.reload(); }, 3500);

                } else if ( data == "false" ) {
                  swal({
                    title: "錯誤！",
                    type: "warning",
                    text: "錯誤",
                  });
                }
                console.log(data);
              },
              error:function(){
                swal({
                    title: "錯誤2！",
                    type: "warning",
                    text: "錯誤",
                  });

              }
            });



          });

        },false);
      }
    });
  </script>
<?php
  }
?>
</html>
