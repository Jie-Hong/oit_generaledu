<?php
  require_once "./php/functions.php";
  date_default_timezone_set('Asia/Taipei');
  $invitation = $username = $password = $password2 = $name = $nickname = $email = $level_id = "";

  $invitation = filter_input(INPUT_POST,'invitation');
  $username = filter_input(INPUT_POST,'username');
  $username = htmlentities($username,ENT_QUOTES,'UTF-8');
  $password = filter_input(INPUT_POST,'password');
  $password2 = filter_input(INPUT_POST,'password2');
  $name = filter_input(INPUT_POST,'name');
  $name = htmlentities($name,ENT_QUOTES,'UTF-8');
  $nickname = filter_input(INPUT_POST,'nickname');
  $nickname = htmlentities($nickname,ENT_QUOTES,'UTF-8');
  $email = filter_input(INPUT_POST,'email',FILTER_VALIDATE_EMAIL);
  $registerLevel = filter_input(INPUT_POST,'level_id');
  $date = date('Y-m-d H:i:s');
  $sqlInvitation = "SELECT * FROM `member_invitation` WHERE `invitationCode` = '$invitation' and `invitationUsed` = '0'";
  $sqlUsername = "SELECT `memberAccount` FROM `web_member` WHERE `memberAccount` = '$username'";
  $resultCheckInvitation = $db->query($sqlInvitation);
  $resultCheckUsername = $db->query($sqlUsername);
  $rowInvitation = $resultCheckInvitation->fetch();
  $invitationLevel = $rowInvitation['invitationLevel'];


  $usernameLength=strlen($username);
  $passwordLength=strlen($password);
  $password2Length=strlen($password2);
  $nameLength=strlen($name);
  $nicknameLength=strlen($nickname);
  $emailLength=strlen($email);
  

  function registerCheckShow($resultCheckInvitation,$resultCheckUsername,$username,$usernameLength,$password,$passwordLength,$password2,$password2Length,$email,$emailLength,$registerLevel,$invitationLevel)
  {
	 if($resultCheckInvitation->rowCount() == 0)
       {
         echo "<br/>邀請碼無效，請確認";
       }
	 if($registerLevel != $invitationLevel)
       {
         echo "<br/>身分選擇錯誤";
       }

     if(!$resultCheckUsername->rowCount() == 0)
       {
         echo "<br/>帳號已存在，請更換";
       }
     if($usernameLength > 20 || $usernameLength < 6)
       {
         echo "<br/>請輸入長度6至20的帳號";
       }
     if($password !== $password2)
       {
         echo "<br/>密碼確認失敗，請重新輸入";
       }
     if($passwordLength <8 || $password2Length < 8)
       {
         echo "<br/>請輸入長度至少8位的密碼";
       }
     if($registerLevel == false)
       {
         echo "<br/>請選擇身份";
       }
     if($email === false)
       {
         echo "<br/>Email格式錯誤";
       }
  }  

  if(isset($_POST['username']) && !$resultCheckInvitation->rowCount() == 0 && $resultCheckUsername->rowCount() == 0 && $password === $password2 && !$registerLevel == 0 && $usernameLength <= 20 && $usernameLength >= 6 && $passwordLength >=8 && $password2Length >= 8 && $invitationLevel == $registerLevel)
  {
    $sqlRegister = 'INSERT INTO `web_member`(`memberID`, `memberAccount`, `memberPassword`, `memberName`, `memberNickname`, `memberLevel`, `memberEmail`, `memeberDate`) VALUES (null,?,?,?,?,?,?,?)';
    $statement = $db->prepare($sqlRegister);
    if(!$username == 0 && !$password == 0 && !$name == 0 && !$nickname == 0 && !$registerLevel == 0 && !$email == 0 && !$date == 0)
    {
    $passwordHash = password_hash($password,PASSWORD_DEFAULT,['cost'=>15]);
    $statement->bindValue('1', $username);
    $statement->bindValue('2', $passwordHash);
    $statement->bindValue('3', $name);
    $statement->bindValue('4', $nickname);
    $statement->bindValue('5', $registerLevel);
    $statement->bindValue('6', $email);
    $statement->bindValue('7', $date);
    $resultRegister = $statement->execute();
    echo '<script type="text/javascript">
                alert("註冊成功，請重新登入");
                document.location.href="index.php";
                </script>';
    //header('Location: index.php');
    }
  }
?>
<!DOCTYPE html>
<html lang="zh-Hant-TW">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, , maximum-scale=1">
  <title>會員註冊 - 文韻亞東</title>
  <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body class="register-bg">
<section class="register-main">
  <div class="container">    
    <div class="register-choose">
      <h1>會員註冊 Register</h1>
      <ul>
        <li>
          <header>我是課程老師</header>
          <div class="register-introduction">
            <ul>
              <li>個人頁面</li>
              <li>後台管理權限【進階】</li>
              <li>最新公告管理權限</li>
              <li>討論區管理權限</li>
              <li>編輯留言</li>
              <li>刪除留言</li>
              <li>回覆留言</li>
            </ul>
          </div>
          <a href="javascript:void(0)" id="register-choose-teacher" class="register-choose-btn register-choose-teacher">選擇 Choose</a>
        </li>
        <li>
          <header>我是課程TA</header>
          <div class="register-introduction">
            <ul>
              <li>個人頁面</li>
              <li>後台管理權限【基本】</li>
              <li>討論區管理權限</li>
              <li>編輯留言</li>
              <li>刪除留言</li>
              <li>回覆留言</li>
            </ul>
          </div>
          <a href="javascript:void(0)" id="register-choose-ta" class="register-choose-btn register-choose-ta">選擇 Choose</a>
        </li>
        <li>
          <header>我是亞東學生</header>
          <div class="register-introduction">
            <ul>
              <li>暫無開放</li>
              <li>暫無開放</li>
              <li>暫無開放</li>
              <li>學生直接匿名留言即可</li>
              <li>請記得每次留言必須填寫基本資料</li>
              <li>留言發佈後就無法編輯、刪除</li>
              <li>請同學留言時小心謹慎</li>
            </ul>
          </div>
          <a href="javascript:void(0)" id="register-choose-student" class="register-choose-btn register-choose-student">選擇 Choose</a>
        </li>
      </ul>
    </div>
    <div class="register-from">
      <?php
      if(isset($_POST['username']))
      {
         echo '<div class="register-check-text">';
	       registerCheckShow($resultCheckInvitation,$resultCheckUsername,$username,$usernameLength,$password,$passwordLength,$password2,$password2Length,$email,$emailLength,$registerLevel,$invitationLevel);
         echo '</div>';
      }
      ?>
      <form action="register.php" method="post" class="form-register">
        <div class="form__field">
          <label class="fontawesome-user" for="register__username"><i class="fa fa-gift"></i></label>
          <input id="register__invitation" type="text" class="form__input" placeholder="請輸入「邀請碼」" name="invitation"  required>
        </div>
        <div class="register-divider"></div>
        <div class="form__field">
          <label class="fontawesome-user" for="register__username"><i class="fa fa-user"></i></label>
          <input id="register__username" type="text" class="form__input" placeholder="帳號 Username (6~20字元)" name="username"  required>
        </div>
        <div class="form__field">
          <label class="fontawesome-lock" for="login__password"><i class="fa fa-key"></i></label>
          <input id="register__password" type="password" class="form__input" placeholder="密碼 Password (8字元以上)" name="password" required>          
        </div>
        <div class="form__field">
          <label class="fontawesome-lock" for="login__password"><i class="fa fa-key"></i></label>
          <input id="register__password__2" type="password" class="form__input" placeholder="再次輸入密碼" name="password2" required>
        </div>
        <div class="form__field">
          <label class="fontawesome-lock" for="login__password"><i class="fa fa-edit"></i></label>
          <input id="register__name" type="text" class="form__input" placeholder="姓名 Name (系統保留用)" name="name" required>
        </div>
        <div class="form__field">
          <label class="fontawesome-lock" for="login__password"><i class="fa fa-edit"></i></label>
          <input id="register__nickname" type="text" class="form__input" placeholder="暱稱 NickName" name="nickname" required>
        </div>
        <div class="form__field">
          <label class="fontawesome-lock" for="login__password"><i class="fa fa-envelope"></i></label>
          <input id="register__nickname" type="text" class="form__input" placeholder="信箱 Email" name="email" required>
        </div>
        <input id="register__level" type="text" hidden="" name="level_id">
        <div class="form__field">
          <input type="submit" value="註冊 Register">
        </div>
      </form>
  </div>
</section>
<script src="js/style.js"></script>
<script src="js/register.js"></script>
</body>
</html>