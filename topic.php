<?php
  require_once "./php/functions.php";
  //確認getID為整數
  date_default_timezone_set('Asia/Taipei');
  urlCheckID();
  $classComment = $studentidComment = $publisherComment = $contentComment = $dateComment = $cID = $classDiscussion = $studentidDiscussion = $publisherDiscussion = $contentDiscussion = $dateDiscussion = $myip = "";
  $aID = $_GET['id'];
  if(isset($aID))//文章ID抓取
  {
    $sql = "SELECT * FROM `forum_article` LEFT JOIN `web_member` ON `forum_article`.`articlePublisher` = `web_member`.`memberAccount` WHERE `articleID` =" .$aID;
    //insert comment
	if(isset($_POST['response']))
    {

      $myip = get_client_ip();

      if($loggedin)
      {
        $sqlReComment = 'INSERT INTO `forum_comment`(`commentID`, `commentArticleID`, `commentPublisher`, `commentContent`, `commentDate`, `commentIP`, `commentLevel`,commentVisible) VALUES (null,?,?,?,?,?,?,?)';
        $statement = $db->prepare($sqlReComment);
        $publisherComment = $user;
        $publisherComment = htmlentities($publisherComment,ENT_QUOTES,'UTF-8');
        $contentComment = filter_input(INPUT_POST,'text');
        $contentComment = htmlentities($contentComment,ENT_QUOTES,'UTF-8');
        $contentComment = str_replace("\n","<br/>",$contentComment);
        $dateComment = date('Y-m-d H:i:s');
        $ipComment = $myip;
        $levelComment = $level;
        $visibleComment = '1';
        if(!$publisherComment == 0 &&  !$contentComment == 0 && !$dateComment == 0 && !$ipComment == 0 && !$levelComment == 0 && !$visibleComment == 0)
        {
          $statement->bindValue('1', $aID, PDO::PARAM_INT);
          $statement->bindValue('2', $publisherComment);
          $statement->bindValue('3', $contentComment);
          $statement->bindValue('4', $dateComment);
          $statement->bindValue('5', $ipComment);
          $statement->bindValue('6', $levelComment);
          $statement->bindValue('7', $visibleComment);
          $resultReComment = $statement->execute();
          echo '<script type="text/javascript">
                alert("留言成功");
                </script>';
        }
      }
      else
      {
        if(!isset($_POST['click']))$click = 0;
        else $click = $_POST['click'];
        //如果使用者點了按鈕，就載進檢查驗證碼的檔案
        // if($click)include("./php/checkForm.php");
        $checkType=false;
        /*
        *  檢查表單送過來的東西    
        */
        if(!isset($_SESSION)) 
        { 
            session_start(); 
        } 
        $checkCode = $_POST['checkCode'];
        if(!isset($_SESSION))$SEC = "";
        else $SEC = $_SESSION['checkNum'];  
        //如果驗證碼為空
        if($checkCode == "")echo "<script type=\"text/javascript\">alert(\"驗證碼請勿空白\")</script>";
        //如果驗證碼不是空白但輸入錯誤
        else if($checkCode != $SEC && $checkCode !=""){
          echo "<script type=\"text/javascript\">alert(\"驗證碼請錯誤，請重新輸入\")</script>";
          $checkType=false;
        }
        else{//驗證碼輸入正確
            // echo "<script type=\"text/javascript\">alert(\"驗證碼正確！\")</script>";
            $checkType=true;
            //這邊可以做任何事情，像是寄信等等的東西
        } 
        if($checkType==true){     
          $sqlReComment = 'INSERT INTO `forum_comment`(`commentID`, `commentArticleID`, `commentClass`, `commentStudentID`, `commentPublisher`, `commentContent`, `commentDate`, `commentIP`, `commentLevel`,commentVisible) VALUES (null,?,?,?,?,?,?,?,?,?)';
          $statement = $db->prepare($sqlReComment);
          $classComment = filter_input(INPUT_POST,'class-select');
          $studentidComment = filter_input(INPUT_POST,'topic-re-studentid', FILTER_VALIDATE_INT);
          $publisherComment = filter_input(INPUT_POST,'topic-re-name');
          $publisherComment = htmlentities($publisherComment,ENT_QUOTES,'UTF-8');
          $contentComment = filter_input(INPUT_POST,'text');
          $contentComment = htmlentities($contentComment,ENT_QUOTES,'UTF-8');
          $contentComment = str_replace("\n","<br/>",$contentComment);
          $dateComment = date('Y-m-d H:i:s');
          $ipComment = $myip;
          $levelComment = $level;
          $visibleComment = '1';
          if(!$classComment == 0 && !$studentidComment == 0 && !$publisherComment == 0 &&  !$contentComment == 0 && !$dateComment == 0 && !$ipComment == 0 && !$levelComment == 0 && !$visibleComment == 0)
          {
            $statement->bindValue('1', $aID, PDO::PARAM_INT);
            $statement->bindValue('2', $classComment);
            $statement->bindValue('3', $studentidComment, PDO::PARAM_INT);
            $statement->bindValue('4', $publisherComment);
            $statement->bindValue('5', $contentComment);
            $statement->bindValue('6', $dateComment);
            $statement->bindValue('7', $ipComment);
            $statement->bindValue('8', $levelComment);
            $statement->bindValue('9', $visibleComment);
            $resultReComment = $statement->execute();
            echo '<script type="text/javascript">
                  alert("留言成功");
                  </script>';
          } else {
            echo '<script type="text/javascript">
                  alert("失敗，錯誤代碼: A-0001");
                  </script>';
          }
        }else{
          echo '<script type="text/javascript">
          alert("留言失敗");
          </script>';
          
        }
       
      }
    }

    //insert dissussion
    if(isset($_POST['comment_id']))
    {
      $myip = get_client_ip();

	  if($loggedin)
      {
      $sqlReDiscussion = 'INSERT INTO `forum_discussion`(`discussionID`, `discussionCommentID`, `discussionPublisher`, `discussionContent`, `discussionDate`, `discussionIP`, `discussionLevel`, `discussionVisible`) VALUES (null,?,?,?,?,?,?,?)';
      $statement = $db->prepare($sqlReDiscussion);

      $cID = filter_input(INPUT_POST,'comment_id');
      $publisherDiscussion = $user;
      $publisherDiscussion = htmlentities($publisherDiscussion,ENT_QUOTES,'UTF-8');
      $contentDiscussion = filter_input(INPUT_POST,'text');
      $contentDiscussion = htmlentities($contentDiscussion,ENT_QUOTES,'UTF-8');
      $contentDiscussion = str_replace("\n","<br/>",$contentDiscussion);
      $dateDiscussion = date('Y-m-d H:i:s');
      $ipDiscussion = $myip;
      $levelDiscussion = $level;
      $visibleDiscussion = '1';

      if(!$cID == 0 && !$publisherDiscussion == 0 &&  !$contentDiscussion == 0 && !$dateDiscussion == 0 && !$ipDiscussion == 0 && !$levelDiscussion == 0 && !$visibleDiscussion == 0)
      {
        $statement->bindValue('1', $cID, PDO::PARAM_INT);
        $statement->bindValue('2', $publisherDiscussion);
        $statement->bindValue('3', $contentDiscussion);
        $statement->bindValue('4', $dateDiscussion);
        $statement->bindValue('5', $ipDiscussion);
        $statement->bindValue('6', $levelDiscussion);
        $statement->bindValue('7', $visibleDiscussion);
        $resultReDiscussion = $statement->execute();
        echo '<script type="text/javascript">
                alert("留言成功");
                </script>';
        }
      }
      else
      {
        $sqlReDiscussion = 'INSERT INTO `forum_discussion`(`discussionID`, `discussionCommentID`, `discussionClass`, `discussionStudentID`, `discussionPublisher`, `discussionContent`, `discussionDate`, `discussionIP`, `discussionLevel`, `discussionVisible`) VALUES (null,?,?,?,?,?,?,?,?,?)';
        $statement = $db->prepare($sqlReDiscussion);

        $cID = filter_input(INPUT_POST,'comment_id');
        $classDiscussion = filter_input(INPUT_POST,'class-select');
        $studentidDiscussion = filter_input(INPUT_POST,'topic-re-studentid', FILTER_VALIDATE_INT);
        $publisherDiscussion = filter_input(INPUT_POST,'topic-re-name');
        $publisherDiscussion = htmlentities($publisherDiscussion,ENT_QUOTES,'UTF-8');
        $contentDiscussion = filter_input(INPUT_POST,'text');
        $contentDiscussion = htmlentities($contentDiscussion,ENT_QUOTES,'UTF-8');
        $contentDiscussion = str_replace("\n","<br/>",$contentDiscussion);
        $dateDiscussion = date('Y-m-d H:i:s');
        $ipDiscussion = $myip;
        $levelDiscussion = $level;
        $visibleDiscussion = '1';

        if(!$cID == 0 && !$classDiscussion == 0 && !$studentidDiscussion == 0 && !$publisherDiscussion == 0 &&  !$contentDiscussion == 0 && !$dateDiscussion == 0 && !$ipDiscussion == 0 && !$levelDiscussion == 0 && !$visibleDiscussion == 0)
        {
          $statement->bindValue('1', $cID, PDO::PARAM_INT);
          $statement->bindValue('2', $classDiscussion);
          $statement->bindValue('3', $studentidDiscussion, PDO::PARAM_INT);
          $statement->bindValue('4', $publisherDiscussion);
          $statement->bindValue('5', $contentDiscussion);
          $statement->bindValue('6', $dateDiscussion);
          $statement->bindValue('7', $ipDiscussion);
          $statement->bindValue('8', $levelDiscussion);
          $statement->bindValue('9', $visibleDiscussion);
          $resultReDiscussion = $statement->execute();
          echo '<script type="text/javascript">
                  alert("留言成功");
                  </script>';
        }
      }
    }

	$sql2 = "SELECT * FROM `forum_comment` LEFT JOIN `web_member` ON `forum_comment`.`commentPublisher` = `web_member`.`memberAccount` WHERE `commentArticleID` = '$aID' ORDER BY `commentDate` ASC";
    $result = $db->query($sql);
    $result2 = $db->query($sql2);
    $row = $result->fetch();
    $title=htmlspecialchars($row['articleSubject'], ENT_QUOTES);
    $content=htmlspecialchars($row['articleContent'], ENT_QUOTES);
    $date=substr($row['articleDate'],0,10);
    //回寫瀏覽次數
    $updateArticleViewer=$row['articleViewer']+1;
    $updateArticleID=$row['articleID'];
    $sqlUpdateViewer = "UPDATE `forum_article` SET `articleViewer` ='$updateArticleViewer' WHERE `articleID` = '$updateArticleID'";
    $updateResult = $db->query($sqlUpdateViewer);
    //統計留言次數
    $commentCountArticleID = $row['articleID'];
    $sqlCommentCount = "SELECT COUNT(`commentArticleID`) FROM `forum_comment` WHERE `commentArticleID` = '$commentCountArticleID'";
    $countResult = $db->query($sqlCommentCount);
    $countrow = $countResult->fetch();
    $commentCount=$countrow['COUNT(`commentArticleID`)'];
    //$db = null;
    //無查詢結果轉跳forums.php
    if($result->rowCount() == 0)
    {
      header('Location: forums.php');
    }
  } else {
    header('Location: 404.php');
  }

/*
  if ( $_COOKIE["pagepassword"] === "oit" ) {
    $pagecookie = "yes";
  } else {
    $pagecookie = "no";
  }
  if(isset($_POST['pagepw'])) {
    $pagepwid = filter_input(INPUT_POST,'pagepwid');
    if ( $pagepwid == "oit" ) {
      setcookie("pagepassword", "oit", time() + (86400 * 90) );
      Header('Location: '."topic.php?id=".$aID);
      Exit(); //optional
    } else {
      $pagecookie = "error";
    }
  }*/
?>
<!DOCTYPE html>
<html lang="zh-Hant-TW">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, , maximum-scale=1">
  <title>
    <?php echo $title;?>- 討論區 - 文韻亞東</title>
  <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css"> <!-- Font-Awesome -->
  <link rel="stylesheet" href="outdatedbrowser/outdatedbrowser.css"> <!-- outdatedbrowser 檢查瀏覽器 -->
  <script src="outdatedbrowser/outdatedbrowser.js"></script> <!-- outdatedbrowser 檢查瀏覽器 -->
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <script src="//code.jquery.com/jquery-2.1.3.min.js"></script>
</head>

<body>
  <header id="header">
    <section class="header-site-channel">
      <div class="container">
        <div class="header-channel-content">
          <div class="header-logo">
            <a href="index.php"><img src="images/logo.png" /></a>
            <span>本課程由教育部資訊及科技教育司支持</span>
            <div class="header-video">
              <a href="./news-detail.php?id=24">
                <!-- <img src="./video/1042.jpg"> -->
                <!-- <i class=""></i> -->
              </a>
            </div>
          </div>
          <?php
        if($loggedin)
        {
        echo
          "<div class=\"header-login\">
             <ul>
               <li><a>您好，$user</a></li>
               <li><a href=\"user-edito.php\">會員編輯</a></li>
               <li><a href=\"admin/index.php\">後台管理</a></li>
               <li><a href=\"logout.php\">登出</a></li>
             </ul>
           </div>";
        }
        else
        {
          echo
            "<div class=\"header-login\">
               <ul>
                 <li><a href=\"login.php\">登入</a></li>
                 <li><a href=\"register.php\">註冊</a></li>
               </ul>
             </div>";
        }
        ?>
        </div>
      </div>
    </section>
    <nav class="navbar" role="navigation">
      <div class="container">
        <div class="navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="news.php">最新消息</a></li>
            <li><a href="intro.php">課程計畫</a></li>
            <li class="active"><a href="forums.php">討論區</a></li>
            <li><a href="vote.php">投票活動</a></li>
            <li><a href="memory.php">亞東印記</a></li>
            <li><a href="works.php">優秀作品</a></li>
            <li><a href="picture.php">影像紀錄</a></li>
            <li><a href="activity.php">活動集錦</a></li>
            <li><a href="videosharing.php">影片分享</a></li>
            <li><a href="feedback.php">TA回饋分享</a></li>
            <li><a href="team.php">核心團隊</a></li>
            <li><a href="links.php">相關資源</a></li>
          </ul>
        </div>
      </div>
    </nav>
  </header>
  <section class="forums-title">
    <div class="container">
      <h1><span>Forums</span>討論區</h1>
    </div>
  </section>
  <article class="topic-main">
    <?php
  echo
  "<div class=\"container\">
    <div class=\"breadcrumb\">
      <ul>
        <li><a href=\"index.php\">首頁</a></li>
        <li><a href=\"forums.php\">討論區</a></li>
      </ul>
    </div>
    <div class=\"topic-inner\">
      <h1>$title</h1>
      <div class=\"topic-info\">
        <span class=\"author\"><i class=\"fa fa-user\"></i>{$row['memberNickname']}</span>
        <span class=\"date\"><i class=\"fa fa-calendar\"></i>$date</span>
        <span class=\"view\"><i class=\"fa fa-eye\"></i>{$row['articleViewer']}</span>
        <span class=\"comments\"><i class=\"fa fa-comment\"></i>$commentCount</span>
      </div>
      <div class=\"topic-content\">
        {$row['articleContent']}
      </div>
    </div>
  </div>";
?>
  </article>
  <section class="topic-comments">
    <div class="container">
      <div class="topic-comt-wrapper">
        <div class="row">
          <div class="col-md-9">
            <div class="comments">
              <h2>
                <?php echo $commentCount; ?> 則回應</h2>
              <div class="topic-respond">
                <?php
                if($loggedin) {
                  echo '
                  <form id="paper" method="post" action="">
                    <textarea id="topic-re-text" class="topic-re-text" name="text" rows="4" placeholder="請輸入回應文字，按Enter或Shift+Enter，可增加高度。" required></textarea>
                    <input class="topic-re-btn" type="submit" value="回應"  >
                    <input type="text" hidden name="response">
                  </form>';
                } else {
                  echo '
                  <form id="paper" method="post" action="">
                    <div class="topic-respond-info">
                      <span>
                        <select class="topic-dropdown" name="class-select" required>
                          <option value="">------請選擇班級------</option>
                          <option value="1071-ha-a">1071 醫管1A</option>
                          <option value="1071-ha-b">1071 醫管1B</option>
                          <option value="1071-ce-a">1071 通訊1A</option>
                          <option value="1071-ce-b">1071 通訊1B</option>

                          <option value="1071-mi-a">1071 資管1A</option>
                          <option value="1071-mi-b">1071 資管1B</option>

                          <option value="1071-mt-a">1071 材纖1A</option>
                          <option value="1071-mt-b">1071 材纖1B</option>

                          <option value="1071-md-a">1071 行銷1A</option>
                          <option value="1071-md-b">1071 行銷1B</option>
                        </select>
                      </span>
                      <span><input type="text" name="topic-re-studentid" class="topic-re-input" placeholder="學號" maxlength="9" required/></span>
                      <span><input type="text" name="topic-re-name" class="topic-re-input" placeholder="姓名" required/></span>
                      <br>
                      <img src="./php/checkCode.php" style="margin-right: 20px;">
                      <input class="topic-re-input" type="text" name="checkCode">

                    </div>
                    <textarea id="topic-re-text" class="topic-re-text" name="text" rows="4" placeholder="請輸入回應文字，按Enter或Shift+Enter，可增加高度。" required></textarea>
                    <input class="topic-re-btn" type="submit" value="回應" id="topic-post" name="click">
                    <input type="text" hidden name="response">
                  </form>';
                }
              ?>
              </div>
              <ul id="comments-list" class="comments-list">
                <?php
              while ($row2 = $result2->fetch()){
                $content = $row2['commentContent'];
                $date=substr($row2['commentDate'],0,16);
                $mark = mb_substr($row2['commentPublisher'],0,1,"UTF-8");
                $cID = $row2['commentID'];
                $classCode = $row2['commentClass'];
                $commentIP = IpHidden($row2['commentIP']);
                if($row2['memberNickname'] && $row2['memberLevel'] === $row2['commentLevel'])
                {
                  $name = $row2['memberNickname'];
                }
                else
                {
                  $name = $row2['commentPublisher'];
                }
                switch($row2['commentLevel']){
                  case '5':
                    $mark = mb_substr($row2['commentPublisher'],0,1,"UTF-8");
                    break;
                  case '4':
                    $mark = "TA";
                    break;
                  case '3':
                    $mark = "師";
                    break;
                  case '2':
                    $mark = "通";
                    break;
                  case '1':
                    $mark = "管";
                    break;
                }
                //轉換班別
                switch($classCode){
                  case "1041-ha-a":
                    $className = '1041 醫管1A';
                   break;
                  case '1041-ha-b':
                    $className = "1041 醫管1B";
                    break;
                  case '1041-ce-a':
                    $className = "1041 通訊1A";
                    break;
                  case '1041-ce-b':
                    $className = "1041 通訊1B";

                  case "1042-ha-a":
                    $className = '1042 醫管1A';
                   break;
                  case '1042-ha-b':
                    $className = "1042 醫管1B";
                    break;
                  case '1042-ce-a':
                    $className = "1042 通訊1A";
                    break;
                  case '1042-ce-b':
                    $className = "1042 通訊1B";
                    break;

                  //-------------------------------------
                  case "1051-ha-a":
                    $className = '1051 醫管1A';
                   break;
                  case '1051-ha-b':
                    $className = "1051 醫管1B";
                    break;
                  case '1051-ce-a':
                    $className = "1051 通訊1A";
                    break;
                  case '1051-ce-b':
                    $className = "1051 通訊1B";
                    break;
                  case '1051-mi-a':
                    $className = "1051 資管1A";
                    break;
                  case '1051-mi-b':
                    $className = "1051 資管1B";
                    break;
                  case '1051-mt-a':
                    $className = "1051 材纖1A";
                    break;
                  case '1051-mt-b':
                    $className = "1051 材纖1B";
                    break;
                  case '1051-md-a':
                    $className = "1051 行銷1A";
                    break;
                  case '1051-md-b':
                    $className = "1051 行銷1B";
                    break;

                  // ----------------------------------------------------------------
                    case "1052-ha-a":
                      $className = '1052 醫管1A';
                      break;
                    case '1052-ha-b':
                      $className = "1052 醫管1B";
                      break;
                    case '1052-ce-a':
                      $className = "1052 通訊1A";
                      break;
                    case '1052-ce-b':
                      $className = "1052 通訊1B";
                      break;
                    case '1052-mi-a':
                      $className = "1052 資管1A";
                      break;
                    case '1052-mi-b':
                      $className = "1052 資管1B";
                      break;
                    case '1052-mt-a':
                      $className = "1052 材纖1A";
                      break;
                    case '1052-mt-b':
                      $className = "1052 材纖1B";
                      break;
                    case '1052-md-a':
                      $className = "1052 行銷1A";
                      break;
                    case '1052-md-b':
                      $className = "1052 行銷1B";
                      break;
                  // ----------------------------------------------------------------
                    case "1061-ha-a":
                      $className = '1061 醫管1A';
                      break;
                    case '1061-ha-b':
                      $className = "1061 醫管1B";
                      break;
                    case '1061-ce-a':
                      $className = "1061 通訊1A";
                      break;
                    case '1061-ce-b':
                      $className = "1061 通訊1B";
                      break;
                    case '1061-mi-a':
                      $className = "1061 資管1A";
                      break;
                    case '1061-mi-b':
                      $className = "1061 資管1B";
                      break;
                    case '1061-mt-a':
                      $className = "1061 材纖1A";
                      break;
                    case '1061-mt-b':
                      $className = "1061 材纖1B";
                      break;
                    case '1061-md-a':
                      $className = "1061 行銷1A";
                      break;
                    case '1061-md-b':
                      $className = "1061 行銷1B";
                      break;
                      // ----------------------------------------------------------------
                        case "1062-ha-a":
                          $className = '1061 醫管1A';
                          break;
                        case '1062-ha-b':
                          $className = "1061 醫管1B";
                          break;
                        case '1062-ce-a':
                          $className = "1061 通訊1A";
                          break;
                        case '1062-ce-b':
                          $className = "1061 通訊1B";
                          break;
                        case '1062-mi-a':
                          $className = "1061 資管1A";
                          break;
                        case '1062-mi-b':
                          $className = "1061 資管1B";
                          break;
                        case '1062-mt-a':
                          $className = "1061 材纖1A";
                          break;
                        case '1062-mt-b':
                          $className = "1061 材纖1B";
                          break;
                        case '1062-md-a':
                          $className = "1061 行銷1A";
                          break;
                        case '1062-md-b':
                          $className = "1061 行銷1B";
                          break;
                      //-------------------------------------
                      case "1071-ha-a":
                      $className = '1071 醫管1A';
                      break;
                    case '1071-ha-b':
                      $className = "1071 醫管1B";
                      break;
                    case '1071-ce-a':
                      $className = "1071 通訊1A";
                      break;
                    case '1071-ce-b':
                      $className = "1071 通訊1B";
                      break;
                    case '1071-mi-a':
                      $className = "1071 資管1A";
                      break;
                    case '1071-mi-b':
                      $className = "1071 資管1B";
                      break;
                    case '1071-mt-a':
                      $className = "1071 材纖1A";
                      break;
                    case '1071-mt-b':
                      $className = "1071 材纖1B";
                      break;
                    case '1071-md-a':
                      $className = "1071 行銷1A";
                      break;
                    case '1071-md-b':
                      $className = "1071 行銷1B";
                      break;
                }
                $sql3 = "SELECT * FROM `forum_discussion` LEFT JOIN `web_member` ON `forum_discussion`.`discussionPublisher` = `web_member`.`memberAccount` WHERE `discussionCommentID` = '$cID' ORDER BY `discussionDate` ASC";
                $result3 = $db->query($sql3);
            /*  echo
                  "<li data-reid=\"{$row2['commentID']}\" data-reclass=\"$classCode\" data-level=\"{$row2['commentLevel']}\">
                    <div class=\"comment-avatar\"><span>$mark</span></div>
                    <div class=\"comment-right\">
                      <div class=\"comment-r-info\">
                        <span class=\"comment-reply-class\">$className</span>
                        <span class=\"comment-reply-id\">{$row2['commentStudentID']}</span>
                        <span class=\"comment-reply-author\">$name</span>
                        <span class=\"comment-reply-time\">$date</span>
                      </div>
                      <div class=\"comment-r-content\">
                        <p>$content</p>
                      </div>
                      <a href=\"javascript: void(0)\" class=\"comment-reply-btn\">回覆</a>
                      <span class=\"comment-reply-ip\">IP Address：$commentIP</span>
                      <div class=\"topic-comments-formbox\">
                      </div>";*/
                if($row2['commentLevel'] == 5)
                  {
                    echo
                    "<li data-reid=\"{$row2['commentID']}\" data-reclass=\"$classCode\" data-level=\"{$row2['commentLevel']}\">
                      <div class=\"comment-avatar\"><span>$mark</span></div>
                      <div class=\"comment-right\">
                      <div class=\"comment-r-info\">
                        <span class=\"comment-reply-class\">$className</span>
                        <span class=\"comment-reply-id\">{$row2['commentStudentID']}</span>
                        <span class=\"comment-reply-author\">$name</span>
                        <span class=\"comment-reply-time\">$date</span>
                      </div>
                      <div class=\"comment-r-content\">
                        <p>$content</p>
                      </div>
                      <a href=\"javascript: void(0)\" class=\"comment-reply-btn\">回覆</a>
                      <span class=\"comment-reply-ip\">IP Address：$commentIP</span>
                      <div class=\"topic-comments-formbox\">
                      </div>";
                    }
                    else
                    {
                    echo
                  "<li data-reid=\"{$row2['commentID']}\" data-reclass=\"$classCode\" data-level=\"{$row2['commentLevel']}\">
                    <div class=\"comment-avatar\"><span>$mark</span></div>
                    <div class=\"comment-right\">
                      <div class=\"comment-r-info\">
                        <span class=\"comment-reply-author\">$name</span>
                        <span class=\"comment-reply-time\">$date</span>
                      </div>
                      <div class=\"comment-r-content\">
                        <p>$content</p>
                      </div>
                      <a href=\"javascript: void(0)\" class=\"comment-reply-btn\">回覆</a>
                      <div class=\"topic-comments-formbox\">
                      </div>";
                      }

                while ($row3 = $result3->fetch()) {
                  //$content = htmlentities($row3['discussionContent'], ENT_QUOTES, 'UTF-8');
                  $content = $row3['discussionContent'];
                  $date=substr($row3['discussionDate'],0,16);
                  $discussionClassCode =  $row3['discussionClass'];
                  $discussionIP = IpHidden($row3['discussionIP']);
                  if($row3['memberNickname'] && $row3['memberLevel'] === $row3['discussionLevel'])
                  {
                    $name = $row3['memberNickname'];
                  }
                  else
                  {
                    $name = $row3['discussionPublisher'];
                  }
                  switch($row3['discussionLevel']) {
                    case '5':
                      $mark = mb_substr($row3['discussionPublisher'],0,1,"UTF-8");
                      break;
                    case '4':
                      $mark = "TA";
                      break;
                    case '3':
                      $mark = "師";
                      break;
                    case '2':
                      $mark = "通";
                      break;
                    case '1':
                      $mark = "管";
                      break;
                  }

                  switch($discussionClassCode){
                    case "1041-ha-a":
                      $discussionClassName = '1041 醫管1A';
                      break;
                    case '1041-ha-b':
                      $discussionClassName = "1041 醫管1B";
                      break;
                    case '1041-ce-a':
                      $discussionClassName = "1041 通訊1A";
                      break;
                    case '1041-ce-b':
                      $discussionClassName = "1041 通訊1B";
                      break;

                    case "1042-ha-a":
                      $discussionClassName = '1042 醫管1A';
                     break;
                    case '1042-ha-b':
                      $discussionClassName = "1042 醫管1B";
                      break;
                    case '1042-ce-a':
                      $discussionClassName = "1042 通訊1A";
                      break;
                    case '1042-ce-b':
                      $discussionClassName = "1042 通訊1B";
                      break;

                    // ----------------------------------------------------------------
                    case "1051-ha-a":
                      $discussionClassName = '1051 醫管1A';
                      break;
                    case '1051-ha-b':
                      $discussionClassName = "1051 醫管1B";
                      break;
                    case '1051-ce-a':
                      $discussionClassName = "1051 通訊1A";
                      break;
                    case '1051-ce-b':
                      $discussionClassName = "1051 通訊1B";
                      break;
                    case '1051-mi-a':
                      $discussionClassName = "1051 資管1A";
                      break;
                    case '1051-mi-b':
                      $discussionClassName = "1051 資管1B";
                      break;
                    case '1051-mt-a':
                      $discussionClassName = "1051 材纖1A";
                      break;
                    case '1051-mt-b':
                      $discussionClassName = "1051 材纖1B";
                      break;
                    case '1051-md-a':
                      $discussionClassName = "1051 行銷1A";
                      break;
                    case '1051-md-b':
                      $discussionClassName = "1051 行銷1B";
                      break;



                    // ----------------------------------------------------------------
                    case "1052-ha-a":
                      $discussionClassName = '1052 醫管1A';
                      break;
                    case '1052-ha-b':
                      $discussionClassName = "1052 醫管1B";
                      break;
                    case '1052-ce-a':
                      $discussionClassName = "1052 通訊1A";
                      break;
                    case '1052-ce-b':
                      $discussionClassName = "1052 通訊1B";
                      break;
                    case '1052-mi-a':
                      $discussionClassName = "1052 資管1A";
                      break;
                    case '1052-mi-b':
                      $discussionClassName = "1052 資管1B";
                      break;
                    case '1052-mt-a':
                      $discussionClassName = "1052 材纖1A";
                      break;
                    case '1052-mt-b':
                      $discussionClassName = "1052 材纖1B";
                      break;
                    case '1052-md-a':
                      $discussionClassName = "1052 行銷1A";
                      break;
                    case '1052-md-b':
                      $discussionClassName = "1052 行銷1B";
                      break;

                    // ----------------------------------------------------------------
                    case "1061-ha-a":
                      $discussionClassName = '1061 醫管1A';
                      break;
                    case '1061-ha-b':
                      $discussionClassName = "1061 醫管1B";
                      break;
                    case '1061-ce-a':
                      $discussionClassName = "1061 通訊1A";
                      break;
                    case '1061-ce-b':
                      $discussionClassName = "1061 通訊1B";
                      break;
                    case '1061-mi-a':
                      $discussionClassName = "1061 資管1A";
                      break;
                    case '1061-mi-b':
                      $discussionClassName = "1061 資管1B";
                      break;
                    case '1061-mt-a':
                      $discussionClassName = "1061 材纖1A";
                      break;
                    case '1061-mt-b':
                      $discussionClassName = "1061 材纖1B";
                      break;
                    case '1061-md-a':
                      $discussionClassName = "1061 行銷1A";
                      break;
                    case '1061-md-b':
                      $discussionClassName = "1061 行銷1B";
                      break;


                    // ----------------------------------------------------------------
                    case "1062-ha-a":
                      $discussionClassName = '1062 醫管1A';
                      break;
                    case '1062-ha-b':
                      $discussionClassName = "1062 醫管1B";
                      break;
                    case '1062-ce-a':
                      $discussionClassName = "1062 通訊1A";
                      break;
                    case '1062-ce-b':
                      $discussionClassName = "1062 通訊1B";
                      break;
                    case '1062-mi-a':
                      $discussionClassName = "1062 資管1A";
                      break;
                    case '1062-mi-b':
                      $discussionClassName = "1062 資管1B";
                      break;
                    case '1062-mt-a':
                      $discussionClassName = "1062 材纖1A";
                      break;
                    case '1062-mt-b':
                      $discussionClassName = "1062 材纖1B";
                      break;
                    case '1062-md-a':
                      $discussionClassName = "1062 行銷1A";
                      break;
                    case '1062-md-b':
                      $discussionClassName = "1062 行銷1B";
                      break;
                    // ------------------------------------------------
                    case "1071-ha-a":
                    $discussionClassName = '1071 醫管1A';
                    break;
                  case '1071-ha-b':
                    $discussionClassName = "1071 醫管1B";
                    break;
                  case '1071-ce-a':
                    $discussionClassName = "1071 通訊1A";
                    break;
                  case '1071-ce-b':
                    $discussionClassName = "1071 通訊1B";
                    break;
                  case '1071-mi-a':
                    $discussionClassName = "1071 資管1A";
                    break;
                  case '1071-mi-b':
                    $discussionClassName = "1071 資管1B";
                    break;
                  case '1071-mt-a':
                    $discussionClassName = "1071 材纖1A";
                    break;
                  case '1071-mt-b':
                    $discussionClassName = "1071 材纖1B";
                    break;
                  case '1071-md-a':
                    $discussionClassName = "1071 行銷1A";
                    break;
                  case '1071-md-b':
                    $discussionClassName = "1071 行銷1B";
                    break;

                    }

                  if($row3['discussionLevel'] == 5)
                  {
                    echo
                      "<div class=\"topic-comments-2\">
                        <ul>
                          <li data-reclass=\"$discussionClassCode\" data-level=\"{$row3['discussionLevel']}\">
                            <div class=\"comment-avatar\"><span>$mark</span></div>
                            <div class=\"comment-right\">
                              <div class=\"comment-r-info\">
                                <span class=\"comment-reply-class\">$discussionClassName</span>
                                <span class=\"comment-reply-id\">{$row3['discussionStudentID']}</span>
                                <span class=\"comment-reply-author\">$name</span>
                                <span class=\"comment-reply-time\">$date</span>
                              </div>
                              <div class=\"comment-r-content\">
                                <p>$content</p>
                              </div>
                              <span class=\"comment-reply-ip\">IP Address：$discussionIP</span>
                            </div>
                          </li>
                        </ul>
                      </div>";
                  } else {
                    echo
                      "<div class=\"topic-comments-2\">
                        <ul>
                          <li  data-level=\"{$row3['discussionLevel']}\">
                            <div class=\"comment-avatar\"><span>$mark</span></div>
                            <div class=\"comment-right\">
                              <div class=\"comment-r-info\">
                                <span class=\"comment-reply-author\">$name</span>
                                <span class=\"comment-reply-time\">$date</span>
                              </div>
                              <div class=\"comment-r-content\">
                                <p>$content</p>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </div>";
                  };
                }

                echo
                   "</div>
                  </li>";
              };
              ?>
              </ul>
            </div>
          </div>
          <div class="col-md-3">
            <div id="comments-filter" class="comments-filter">
              <h5>留言篩選</h5>
              <ul>
                <li><a href="javascript: void(0)" class="comments-select active" data-class="all">全部</a></li>

                <li class="department mt">材纖</li>
                <li><a href="javascript: void(0)" class="comments-select" data-class="mt-a">材纖1A</a></li>
                <li><a href="javascript: void(0)" class="comments-select" data-class="mt-b">材纖1B</a></li>

                <li class="department ce">通訊</li>
                <li><a href="javascript: void(0)" class="comments-select" data-class="ce-a">通訊1A</a></li>
                <li><a href="javascript: void(0)" class="comments-select" data-class="ce-b">通訊1B</a></li>

                <li class="department md">行銷</li>
                <li><a href="javascript: void(0)" class="comments-select" data-class="md-a">行銷1A</a></li>
                <li><a href="javascript: void(0)" class="comments-select" data-class="md-b">行銷1B</a></li>

                <li class="department mi">資管</li>
                <li><a href="javascript: void(0)" class="comments-select" data-class="mi-a">資管1A</a></li>
                <li><a href="javascript: void(0)" class="comments-select" data-class="mi-b">資管1B</a></li>

                <li class="department ha">醫管</li>
                <li><a href="javascript: void(0)" class="comments-select" data-class="ha-a">醫管1A</a></li>
                <li><a href="javascript: void(0)" class="comments-select" data-class="ha-b">醫管1B</a></li>


              </ul>
            </div>
            <div class="comments-specification">
              <h5>討論區規範</h5>
              <ul>
                <li>留言時，請務必確認個人基本資料是否正確，發佈後將無法刪除。</li>
                <li>不得假冒他人身分進行留言。</li>
                <li>嚴禁發表攻擊、侮辱、影射、謾罵、脅迫、挑釁、猥褻、不雅、有違社會善良風俗之文字。</li>
                <li>禁止廣告、商業性、內容不符合討論主題之留言。</li>
                <li>請勿使用 HTML 語法。</li>
                <li>若有未規定的部份，由管理者依主觀認定，視情況處理。</li>
                <li>如有操作上問題，請至「通識教育中心」諮詢，或者將問題發至<a href="mailto:qaawssedd456@gmail.com" class="email" target="_blank"
                    title="信箱">網頁助理信箱</a>。</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <footer id="footer">
    <div class="subfooter">
      <div class="container">
        <div class="row">
          <div class="col-xxs-12 col-md-7">
            <div class="footer-nav">
              <ul>
                <li><a href="index.php">首頁</a></li>
                <li><a href="news.php">最新消息</a></li>
                <li><a href="intro.php">課程計畫</a></li>
                <li><a href="forums.php">討論區</a></li>
                <li><a href="works.php">優秀作品</a></li>
                <li><a href="picture.php">影像紀錄</a></li>
                <li><a href="team.php">核心團隊</a></li>
              </ul>
            </div>
          </div>
          <div class="col-xxs-12 col-md-5">
            <div class="school">
              <p>亞東技術學院 通識教育中心 OIT Center for General Education</p>
            </div>
            <div class="plan">
              <p>文韻亞東 ─ 中文閱讀書寫課程革新計畫</p>
            </div>
            <div class="hss">
              <p>本課程由教育部資訊及科技教育司支持</p>
            </div>
          </div>
        </div>
      </div> <!-- container -->
    </div> <!-- subfooter -->
    <div class="copyright">
      <div class="container">
        <div class="text">
          <small>
            <address class="author">
              Copyright 2015-2016.
            </address>
            <a href="http://gecw.oit.edu.tw/bin/home.php" title="亞東技術學院通識教育中心" target="_blank">亞東技術學院通識教育中心</a> All
            Rights Reserved.
          </small>
        </div>
        <div class="total">
          <?php require_once "./statistics.php"; ?>
        </div>
      </div>
    </div>
  </footer>
  <div id="outdated"></div> <!-- outdatedbrowser 檢查瀏覽器 -->
  <script src="js/style.js"></script>
  <script src="outdatedbrowser/dom-outdatedbrowser.js"></script> <!-- outdatedbrowser 檢查瀏覽器 -->
  <?php
  if($loggedin) {
    echo '<script src="js/topic-responses-admin.js"></script>';
  } else {
    echo '<script src="js/topic-responses.js"></script>';
  }
?>
  <script src="js/topic-class.js"></script>
  <script src='js/autosize.js'></script>
  <script type="text/javascript">
    var topic_re_text = document.getElementById("topic-re-text");
    autosize(topic_re_text);
  </script>
</body>

</html>