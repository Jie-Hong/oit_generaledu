<!DOCTYPE html>
<html lang="zh-tw">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>錯誤訊息 404 - 文韻亞東</title>  
  <meta http-equiv="refresh" content="9;URL=http://122.116.166.79/chinese">
  <link rel="shortcut icon" href="images/icon.ico" />
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" type="text/css" href="css/404.css">
</head>
<body>
  <div class="error-404-container">
    <div class="error-404-box">
      <h1>4</h1>
      <h1>0</h1>
      <h1>4</h1>
    </div>
    <div class="error-404-explanation">
      <p>抱歉，您所指定的頁面不存在。</p>
      <a class="error-404-go-button" onclick="history.go(-1)" role="button">上一步</a>
    </div>
    <div class="error-404-reciprocal">
      <span>5</span>
      <span>4</span>
      <span>3</span>
      <span>2</span>
      <span>1</span>
      <p>秒跳轉到首頁。</p>
    </div>
  </div>
</body>
</html>