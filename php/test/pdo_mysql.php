<?php 

//資料庫連線
$servername = "localhost";
$dbname = "chinese-read-write";  //資料庫名稱
$username = "root";  //帳號名稱
$password = "oitgec000";  //密碼

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "<h1>" . "資料庫連線成功" . "</h1>"; 
}
catch(PDOException $e) {
    echo "資料庫連線失敗: " . $e->getMessage();
}

//關閉資料庫
$conn = null;

?>