<?php 

//取得IP
function get_client_ip() {
  foreach (array(
    'HTTP_CLIENT_IP',
    'HTTP_X_FORWARDED_FOR',
    'HTTP_X_FORWARDED',
    'HTTP_X_CLUSTER_CLIENT_IP',
    'HTTP_FORWARDED_FOR',
    'HTTP_FORWARDED',
    'REMOTE_ADDR') as $key) {
    if (array_key_exists($key, $_SERVER)) {
      foreach (explode(',', $_SERVER[$key]) as $ip) {
      $ip = trim($ip);
      if ((bool) filter_var($ip, FILTER_VALIDATE_IP,
        FILTER_FLAG_IPV4 |
        FILTER_FLAG_NO_PRIV_RANGE |
        FILTER_FLAG_NO_RES_RANGE)) {
        return $ip;
        }
      }
    }
  }
  return null;
}

$abcdfs = get_client_ip();

echo $abcdfs;


//IP輸出隱藏
function IpHidden($ip) {
  $ip_arr= explode('.', $ip);
  $ip_arr[3]='*';
  $ip= implode('.', $ip_arr);
  return $ip;
}

?>