<?php
  session_start();

  require_once 'db.inc.php';

  $userstr = ' (Guest)';

  if (isset($_SESSION['user']))
  {
    $user     = $_SESSION['user'];
    $level    = $_SESSION['level'];
    $token = $_SESSION['token'];
    $usernick = $_SESSION['nick'];
    $loggedin = TRUE;
    $userstr  = " ($user)";
  }
  else {
    $loggedin = FALSE;
    $level = 5;
  }

  function destroySession()
  {
    $_SESSION=array();
    if (session_id() != "")
      setcookie(session_name(), '', time()-259200, '/');
    session_destroy();
  }

  function urlCheckID()
  {
    if (!filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT))
      header('Location: 404.php');
  }

  function headerShow()
  {

  }

  function classShow($str)
  {
   switch($str){
   case "1041-ha-a":
    $className = "1041 醫管1A";
      break;
    case '1041-ha-b':
    $className = "1041 醫管1B";
    break;
    case '1041-ce-a':
    $className = "1041 通訊1A";
    break;
    case '1041-ce-b':
    $className = "1041 通訊1B";
    break;

    case "1042-ha-a":
    $className = "1042 醫管1A";
      break;
    case '1042-ha-b':
    $className = "1042 醫管1B";
    break;
    case '1042-ce-a':
    $className = "1042 通訊1A";
    break;
    case '1042-ce-b':
    $className = "1042 通訊1B";
    break;


    case "1051-ha-a":
    $className = "1051 醫管1A";
      break;
    case '1051-ha-b':
    $className = "1051 醫管1B";
    break;
    case '1051-ce-a':
    $className = "1051 通訊1A";
    break;
    case '1051-ce-b':
    $className = "1051 通訊1B";
    break;
    case '1051-mi-a':
    $className = "1051 資管1A";
    break;
    case '1051-mi-b':
    $className = "1051 資管1B";
    break;
    case '1051-mt-a':
    $className = "1051 材纖1A";
    break;
    case '1051-mt-b':
    $className = "1051 材纖1B";
    break;
    case '1051-md-a':
    $className = "1051 行銷1A";
    break;
    case '1051-md-b':
    $className = "1051 行銷1B";
    break;

    case "1052-ha-a":
    $className = "1052 醫管1A";
      break;
    case '1052-ha-b':
    $className = "1052 醫管1B";
    break;
    case '1052-ce-a':
    $className = "1052 通訊1A";
    break;
    case '1052-ce-b':
    $className = "1052 通訊1B";
    break;
    case '1052-mi-a':
    $className = "1052 資管1A";
    break;
    case '1052-mi-b':
    $className = "1052 資管1B";
    break;
    case '1052-mt-a':
    $className = "1052 材纖1A";
    break;
    case '1052-mt-b':
    $className = "1052 材纖1B";
    break;
    case '1052-md-a':
    $className = "1052 行銷1A";
    break;
    case '1052-md-b':
    $className = "1052 行銷1B";
    break;

    case "1061-ha-a":
    $className = "1061 醫管1A";
      break;
    case '1061-ha-b':
    $className = "1061 醫管1B";
    break;
    case '1061-ce-a':
    $className = "1061 通訊1A";
    break;
    case '1061-ce-b':
    $className = "1061 通訊1B";
    break;
    case '1061-mi-a':
    $className = "1061 資管1A";
    break;
    case '1061-mi-b':
    $className = "1061 資管1B";
    break;
    case '1061-mt-a':
    $className = "1061 材纖1A";
    break;
    case '1061-mt-b':
    $className = "1061 材纖1B";
    break;
    case '1061-md-a':
    $className = "1061 行銷1A";
    break;
    case '1061-md-b':
    $className = "1061 行銷1B";
    break;
///////////////////////////////////
    case "1062-ha-a":
    $className = "1062 醫管1A";
    break;
    case '1062-ha-b':
    $className = "1062 醫管1B";
    break;
    case '1062-ce-a':
    $className = "1062 通訊1A";
    break;
    case '1062-ce-b':
    $className = "1062 通訊1B";
    break;
    case '1062-mi-a':
    $className = "1062 資管1A";
    break;
    case '1062-mi-b':
    $className = "1062 資管1B";
    break;
    case '1062-mt-a':
    $className = "1062 材纖1A";
    break;
    case '1062-mt-b':
    $className = "1062 材纖1B";
    break;
    case '1062-md-a':
    $className = "1062 行銷1A";
    break;
    case '1062-md-b':
    $className = "1062 行銷1B";
    break;
////////////////////////////////////
    case "1071-ha-a":
    $className = "1062 醫管1A";
    break;
    case '1071-ha-b':
    $className = "1071 醫管1B";
    break;
    case '1071-ce-a':
    $className = "1071 通訊1A";
    break;
    case '1071-ce-b':
    $className = "1071 通訊1B";
    break;
    case '1071-mi-a':
    $className = "1071 資管1A";
    break;
    case '1071-mi-b':
    $className = "1071 資管1B";
    break;
    case '1071-mt-a':
    $className = "1071 材纖1A";
    break;
    case '1071-mt-b':
    $className = "1071 材纖1B";
    break;
    case '1071-md-a':
    $className = "1071 行銷1A";
    break;
    case '1071-md-b':
    $className = "1071 行銷1B";
    break;
    default:
    $className = "沒班級";
    }
    return $className;
  }
function get_client_ip() {
  foreach (array(
    'HTTP_CLIENT_IP',
    'HTTP_X_FORWARDED_FOR',
    'HTTP_X_FORWARDED',
    'HTTP_X_CLUSTER_CLIENT_IP',
    'HTTP_FORWARDED_FOR',
    'HTTP_FORWARDED',
    'REMOTE_ADDR') as $key) {
    if (array_key_exists($key, $_SERVER)) {
      foreach (explode(',', $_SERVER[$key]) as $ip) {
      $ip = trim($ip);
      if ((bool) filter_var($ip, FILTER_VALIDATE_IP,
        FILTER_FLAG_IPV4 |
        FILTER_FLAG_NO_PRIV_RANGE |
        FILTER_FLAG_NO_RES_RANGE)) {
        return $ip;
        }
      }
    }
  }
  return $ip;
}



//IP¿é¥XÁôÂÃ
function IpHidden($ip) {
  $ip_arr= explode('.', $ip);
  $ip_arr[3]='*';
  $ip= implode('.', $ip_arr);
  return $ip;
}

?>
