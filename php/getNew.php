<?php
    require_once "./functions.php";
    $sql = "SELECT * FROM web_news ORDER BY newsID DESC";
    $result = $db->query($sql);
    $show=array();
    while ($row = $result->fetch()){
        $title=htmlspecialchars($row['newsTitle'], ENT_QUOTES);
        $date=substr($row['newsDate'],0,10);
        $newsType=$row['newsType'];
            //顯示消息類別
            switch($newsType){
            case '1':
                $newsTypeName = '資訊';
                break;
            case '2':
                $newsTypeName = '活動';
                break;
            case '3':
                $newsTypeName = '其他';
                break;
            }
            $show[] = array('date'=>$date,'type'=>$newsType,'typeName'=>$newsTypeName,'title'=>$title,'viewer'=>$row['newsViewer']);	
            // echo $date;
            // echo $newsTypeName;
            // echo $title;
            // echo "{$row['newsViewer']}";              
    }     
    echo json_encode($show,JSON_UNESCAPED_UNICODE);

?>