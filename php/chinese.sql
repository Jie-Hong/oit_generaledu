-- phpMyAdmin SQL Dump
-- version 4.4.7
-- http://www.phpmyadmin.net
--
-- 主機: localhost
-- 產生時間： 2015 年 10 月 29 日 23:07
-- 伺服器版本: 5.5.43-MariaDB
-- PHP 版本： 5.5.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 資料庫： `chinese`
--

-- --------------------------------------------------------

--
-- 資料表結構 `forum_article`
--

CREATE TABLE IF NOT EXISTS `forum_article` (
  `articleID` int(10) unsigned NOT NULL,
  `articleContent` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `articleDate` datetime NOT NULL,
  `articleIP` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `articlePublisher` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `articleSubject` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `articleType` int(11) NOT NULL,
  `articleViewer` int(11) NOT NULL,
  `articleVisible` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 資料表的匯出資料 `forum_article`
--

INSERT INTO `forum_article` (`articleID`, `articleContent`, `articleDate`, `articleIP`, `articlePublisher`, `articleSubject`, `articleType`, `articleViewer`, `articleVisible`) VALUES
(1, '<p>請各位同學把問題討論的答案，留言在下面，謝謝</p>\r\n<h4>線上閱讀：</h4>\r\n<h5>你不是真的很愛錢吧</h5>\r\n<div class="pdf-reader">\r\n  <iframe id="player" frameborder="0" allowfullscreen="1" title="pdf reader" src="pdf.js-master/pdf.php?url=../pdf/1041-001.pdf"></iframe>\r\n</div>\r\n<h4>問題討論：</h4>\r\n<p>一、「內在價值」和「外在價值」要如何區別？</p>\r\n', '2015-10-05 01:00:00', '192.168.0.1', 'oitchinese', '104-1 第二週 侯文詠〈你不是真的很愛錢吧〉', 1, 5, 0),
(2, '<p>請各位同學把問題討論的答案，留言在下面，謝謝</p>\r\n<h4>線上閱讀：</h4>\r\n<h5>或許，最大的問題是和自己不熟吧</h5>\r\n<div class="pdf-reader">\r\n  <iframe id="player" frameborder="0" allowfullscreen="1" title="pdf reader" src="pdf.js-master/pdf.php?url=../pdf/1041-002.pdf"></iframe>\r\n</div>\r\n<h4>問題討論：</h4>\r\n<p>一、你有喜歡的人嗎？你喜歡她或他的三個初衷是甚麼？</p>\r\n<p>二、你心目中，「完美的戀情」有哪些要素？</p>\r\n<p>三、你和自己「熟」嗎？</p>', '2015-10-06 02:00:00', '192.168.0.1', 'oitchinese', '104-1 第二週 侯文詠〈或許，最大的問題是和自己不熟吧〉', 1, 5, 0),
(3, '<p>請各位同學把問題討論的答案，留言在下面，謝謝</p>\r\n<h4>線上閱讀：</h4>\r\n<h5>再見，模範生</h5>\r\n<div class="pdf-reader">\r\n  <iframe id="player" frameborder="0" allowfullscreen="1" title="pdf reader" src="pdf.js-master/pdf.php?url=../pdf/1041-003.pdf"></iframe>\r\n</div>\r\n<h4>問題討論：</h4>\r\n<p>一、離開學校後，你想「就業」還是「創業」呢？</p>\r\n<p>二、如果你是一家大公司的總經哩，甚麼情況下你會自己辭掉工作？</p>\r\n<p>三、你覺得「空檔年」對一個人的事業或人生有甚麼幫助？</p>\r\n', '2015-10-07 03:00:00', '192.168.0.1', 'oitchinese', '104-1 第四週 王文華〈再見，模範生〉', 1, 2, 0),
(4, '<p>請各位同學把問題討論的答案，留言在下面，謝謝</p>\r\n<h4>線上閱讀：</h4>\r\n<h5>鎂光燈滅了，才能看清自己</h5>\r\n<div class="pdf-reader">\r\n  <iframe id="player" frameborder="0" allowfullscreen="1" title="pdf reader" src="pdf.js-master/pdf.php?url=../pdf/1041-004.pdf"></iframe>\r\n</div>\r\n<h4>問題討論：</h4>\r\n<p>一、「領導」和「管理」有甚麼不一樣呢？</p>\r\n<p>二、如果你要參加若水公司的創業大賽，你會如何寫一則自己的推薦書與企劃案？</p>\r\n<p>三、社會企業跟一般的企業有甚麼不一樣？</p>', '2015-10-08 04:00:00', '192.168.0.1', 'oitchinese', '104-1 第四週 王文華〈鎂光燈滅了，才能看清自己〉', 1, 4, 0),
(5, '<p>請各位同學把問題討論的答案，留言在下面，謝謝</p>\r\n<h4>線上閱讀：</h4>\r\n<h5>自為墓誌銘</h5>\r\n<div class="pdf-reader">\r\n  <iframe id="player" frameborder="0" allowfullscreen="1" title="pdf reader" src="pdf.js-master/pdf.php?url=../pdf/1041-005.pdf"></iframe>\r\n</div>\r\n<h4>問題討論：</h4>\r\n<p>一、為什麼張岱要說自己是敗子、是廢物？</p>\r\n<p>二、你覺得張岱是甚麼星座？</p>\r\n<p>三、張岱〈自為墓誌銘〉和陶淵明〈自祭文〉都是為自己的人生作註解，你覺得他們的人生觀有甚麼不一樣？</p>', '2015-10-09 05:00:00', '192.168.0.1', 'oitchinese', '104-1 第六週 張岱〈自為墓誌銘〉', 1, 5, 0),
(6, '<p>請各位同學把問題討論的答案，留言在下面，謝謝</p>\r\n<h4>線上閱讀：</h4>\r\n<h5>阿爸的飯包</h5>\r\n<div class="pdf-reader">\r\n  <iframe id="player" frameborder="0" allowfullscreen="1" title="pdf reader" src="pdf.js-master/pdf.php?url=../pdf/1041-006.pdf"></iframe>\r\n</div>\r\n<h4>問題討論：</h4>\r\n<p>一、〈阿爸的飯包〉雖是一首短詩，卻極富意象與畫面，你覺得這首詩最感人之處在哪裏？</p>\r\n<p>二、〈阿爸的飯包〉以孩子的視角，藉著「飯包」描寫父親對孩子的愛。請你試著以你的視角分享至親對你愛的小故事？</p>\r\n<p>三、電影《即刻救援》中，父親奮不顧身用盡全力解救陷入危機的女兒，被稱為「地表最強老爸」。你有沒有一種經驗，讓你覺得你的父親是全天下最強的老爸？</p>', '2015-10-10 06:00:00', '192.168.0.1', 'oitchinese', '104-1 第九週 向陽〈阿爸的飯包〉', 1, 4, 0),
(7, '<p>請各位同學把問題討論的答案，留言在下面，謝謝</p>\r\n<h4>線上閱讀：</h4>\r\n<h5>國峻不回來吃飯</h5>\r\n<div class="pdf-reader">\r\n  <iframe id="player" frameborder="0" allowfullscreen="1" title="pdf reader" src="pdf.js-master/pdf.php?url=../pdf/1041-007.pdf"></iframe>\r\n</div>\r\n<h4>問題討論：</h4>\r\n一、〈國峻不回來吃飯〉用著最平凡的事情訴說最動人的情感，你覺得這首詩的感人之處在哪裏？ \r\n二、〈國峻不回來吃飯〉中看似簡單的「回家吃飯」卻是我們的歸屬感所在，家的吸引力超乎預料，試回想你是否曾經遇到什麼事情，令你急著想回家與家人分享？\r\n三、你曾經面對人生的低潮嗎？有沒有那麼一瞬間你想起了與至親相處的片刻，讓你重新出發？\r\n', '2015-10-11 07:00:00', '192.168.0.1', 'oitchinese', '104-1 第九週 黃春明〈國峻不回來吃飯〉', 1, 4, 0),
(8, '<p>請各位同學把問題討論的答案，留言在下面，謝謝</p>\r\n<h4>線上閱讀：</h4>\r\n<h5>茶的情詩</h5>\r\n<div class="pdf-reader">\r\n  <iframe id="player" frameborder="0" allowfullscreen="1" title="pdf reader" src="pdf.js-master/pdf.php?url=../pdf/1041-008.pdf"></iframe>\r\n</div>\r\n<h4>問題討論：</h4>\r\n<p>一、作者將一份愛情的美好經營，譬喻成泡一壺茶的過程，也在詩中看出作者對品茶的鍾愛與熟悉。而我們現在或許到咖啡廳、飲料店的經驗比較多，咖啡或是珍珠奶茶能否也讓我們聯想到愛情？或分享你擁有過怎樣特殊的味覺/嗅覺記憶（追憶起一段友情、愛情或是親情）？（補充資料：網路上有「茶與愛情」微電影大賽的官網，公佈來自世界各地參賽的微電影視頻，主題都是探索、表現茶與愛情的聯繫：http://esperanto.cri.cn/filmc/14tea3.htm）</p>\r\n<p>二、人類愛茶，甚至能為茶而戰爭，有否思考過茶為何有如此大的魅力？在世界各地裡，多擁有屬於自己的茶文化，如印度辣茶、英國下午茶（多喝伯爵紅茶）、日本抹茶、西藏酥油茶、馬來西亞拉茶等，每種代表茶散發不同精彩的氣質，也可從茶的味道透露出各民族的特性。台灣也自己發明了商機無限的手搖茶飲。你可否以自己喝茶的經驗，為這些不同國家的代表茶或者台灣各款式的手搖茶，賦予它們一套性格模式，類似一般看到的心理測驗，可從茶葉種類、顏色、味道、口感以及搭配茶的其他成分來進行聯想。</p>\r\n<p>三、針對張錯對愛情的看法「那麼你底香郁，必須倚賴我底無味」，或可詮釋成一種對愛人無私的襯托與成全。但也有人會說，愛情不能是一種無止境的討好。你對於愛情付出的分寸拿捏，有何觀點？</p>', '2015-10-12 08:00:00', '192.168.0.1', 'oitchinese', '104-1 第十三週 張錯〈茶的情詩〉', 1, 5, 0),
(9, '<p>請各位同學把問題討論的答案，留言在下面，謝謝</p>\r\n<h4>線上閱讀：</h4>\r\n<h5>席慕蓉詩選</h5>\r\n<div class="pdf-reader">\r\n  <iframe id="player" frameborder="0" allowfullscreen="1" title="pdf reader" src="pdf.js-master/pdf.php?url=../pdf/1041-009.pdf"></iframe>\r\n</div>\r\n<h4>問題討論：</h4>\r\n<p>一、	席慕蓉這三首詩皆隱含「相遇的缺憾」之命題，請試著分析其中架構編排與情感之差異。</p>\r\n<p>二、請試著以自身經驗出發，描述生命中一段交錯而過的相遇。</p>\r\n', '2015-10-13 09:00:00', '192.168.0.1', 'oitchinese', '104-1 第十三週 席慕蓉〈席慕蓉詩選〉', 1, 5, 0),
(10, '<p>請各位同學把問題討論的答案，留言在下面，謝謝</p>\r\n<h4>線上閱讀：</h4>\r\n<h5>寂寞的十七歲</h5>\r\n<div class="pdf-reader">\r\n  <iframe id="player" frameborder="0" allowfullscreen="1" title="pdf reader" src="pdf.js-master/pdf.php?url=../pdf/1041-010.pdf"></iframe>\r\n</div>\r\n<h4>問題討論：</h4>\r\n<p>一、本文藉著哪些情節的堆疊，形塑出主人翁的寂寞情懷？請舉例說明之。</p>\r\n<p>二、白先勇小說形容十七歲是「寂寞的」，李敖說是「虛擬的」，郭靜勇認為是「坎坷的」，西方亦有所謂「少年維特的煩惱」。請同學回想自己十七歲的時光，若以「□□的十七歲」為標題，你會如何描繪這個階段呢？又有哪些畫面浮現腦海？</p>\r\n<p>三、〈寂寞十七歲〉中主角楊雲峰代表的是一個社會邊緣人，他的邊緣現象在文中如何呈現？</p>\r\n<p>四、若面對社會的排擠，你可以如何自處？</p>', '2015-10-14 10:00:00', '192.168.0.1', 'oitchinese', '104-1 第十六週 席慕蓉〈寂寞的十七歲〉', 1, 3, 0);

-- --------------------------------------------------------

--
-- 資料表結構 `forum_comment`
--

CREATE TABLE IF NOT EXISTS `forum_comment` (
  `commentID` int(10) unsigned NOT NULL,
  `commentArticleID` int(10) unsigned NOT NULL,
  `commentClass` varchar(9) COLLATE utf8mb4_unicode_ci NOT NULL,
  `commentStudentID` varchar(9) COLLATE utf8mb4_unicode_ci NOT NULL,
  `commentPublisher` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `commentContent` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `commentDate` datetime NOT NULL,
  `commentIP` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `commentLevel` int(11) NOT NULL,
  `commentVisible` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 資料表結構 `forum_discussion`
--

CREATE TABLE IF NOT EXISTS `forum_discussion` (
  `discussionID` int(10) unsigned NOT NULL,
  `discussionCommentID` int(10) unsigned NOT NULL,
  `discussionClass` varchar(9) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discussionStudentID` varchar(9) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discussionPublisher` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discussionContent` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `discussionDate` datetime NOT NULL,
  `discussionIP` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discussionLevel` int(11) NOT NULL,
  `discussionVisible` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 資料表結構 `member_invitation`
--

CREATE TABLE IF NOT EXISTS `member_invitation` (
  `invitationCode` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `invitationMember` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `invitationUsed` int(2) NOT NULL,
  `invitationLevel` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `member_invitation`
--

INSERT INTO `member_invitation` (`invitationCode`, `invitationMember`, `invitationUsed`, `invitationLevel`) VALUES
('gec500', '', 0, 4),
('gec900', '', 0, 3);

-- --------------------------------------------------------

--
-- 資料表結構 `web_class`
--

CREATE TABLE IF NOT EXISTS `web_class` (
  `classCode` varchar(9) COLLATE utf8mb4_unicode_ci NOT NULL,
  `classYear` int(4) NOT NULL,
  `classDepartment` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `classGroup` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `className` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 資料表的匯出資料 `web_class`
--

INSERT INTO `web_class` (`classCode`, `classYear`, `classDepartment`, `classGroup`, `className`) VALUES
('1041-ce-a', 1041, 'ce', 'a', '通訊1A'),
('1041-ce-b', 1041, 'ce', 'b', '通訊1B'),
('1041-ha-a', 1041, 'ha', 'a', '醫管1A'),
('1041-ha-b', 1041, 'ha', 'b', '醫管1B');

-- --------------------------------------------------------

--
-- 資料表結構 `web_member`
--

CREATE TABLE IF NOT EXISTS `web_member` (
  `memberID` int(11) NOT NULL,
  `memberAccount` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `memberPassword` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `memberName` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `memberNickname` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `memberLevel` int(11) NOT NULL,
  `memberEmail` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `memeberDate` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 資料表的匯出資料 `web_member`
--

INSERT INTO `web_member` (`memberID`, `memberAccount`, `memberPassword`, `memberName`, `memberNickname`, `memberLevel`, `memberEmail`, `memeberDate`) VALUES
(1, 'admin', '$2y$15$M5iutvHvbPmqaLuyr2TKau4fDbCNqZZ4ZU9XeJtx2voxaDu6Iwlj.', '系統管理員', '系統管理員', 1, 'admin@oit.edu.tw', '2015-09-30 00:00:00'),
(2, 'oitchinese', '$2y$15$b.DOZWMyGLDjLXnnePofL.ofvwmBwPRb1yRRttDx0d8XFM.IPAkn2', '通識教育中心', '通識教育中心', 2, 'oitchinese@oit.edu.tw', '2015-10-10 00:00:00');

-- --------------------------------------------------------

--
-- 資料表結構 `web_news`
--

CREATE TABLE IF NOT EXISTS `web_news` (
  `newsID` int(10) unsigned NOT NULL,
  `newsTitle` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `newsContent` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `newsDate` datetime NOT NULL,
  `newsIP` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `newsPublisher` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `newsType` int(11) NOT NULL,
  `newsViewer` int(11) NOT NULL,
  `newsVisible` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 資料表的匯出資料 `web_news`
--

INSERT INTO `web_news` (`newsID`, `newsTitle`, `newsContent`, `newsDate`, `newsIP`, `newsPublisher`, `newsType`, `newsViewer`, `newsVisible`) VALUES
(1, '104/10/12(一) 第一次教學助理工作會報', '<p>第一次教學助理工作會報公告內容\r\n</p>', '2015-10-15 01:00:00', '192.168.0.1', 'oitchinese', 1, 8, 1),
(2, '104/10/19(一)前計畫繳交開課資料', '<p>104/10/19(一)前計畫繳交開課資料公告內容</p>', '2015-10-16 02:00:00', '192.168.0.1', 'oitchinese', 1, 7, 1),
(3, '104/10/21(三) 閱讀沙龍第一場', '<p>104/10/21(三) 閱讀沙龍第一場活動內容</p>\r\n<img src="images/news/1041/10-21.jpg" />\r\n', '2015-10-17 03:00:00', '192.168.0.1', 'oitchinese', 2, 9, 1),
(4, '104/11/04(三) 閱讀沙龍第二場', '<p>104/11/04(三) 閱讀沙龍第二場活動內容</p>\r\n<img src="images/news/1041/10-21.jpg" />\r\n', '2015-10-29 03:00:00', '192.168.0.1', 'oitchinese', 2, 1, 1),
(5, '104/11/05(四) 教師研習工作坊--閱讀與寫作研習活動', '<p>104/11/05(四) 教師研習工作坊--閱讀與寫作研習活動</p>\r\n', '2015-10-30 06:00:00', '192.168.0.1', 'oitchinese', 2, 1, 1),
(6, '104/11/11(三) 名家講座(一)：黃文成/生命書寫與心靈療癒', '<p>104/11/11(三) 名家講座(一)：黃文成/生命書寫與心靈療癒</p>\r\n<img src="images/news/1041/11-11.jpg" />', '2015-10-30 10:00:00', '192.168.0.1', 'oitchinese', 2, 2, 1),
(7, '104/11/16(一) 第二次教學助理工作會報', '<p>104/11/16(一) 第二次教學助理工作會報</p>\r\n', '2015-10-30 23:00:00', '192.168.0.1', 'oitchinese', 1, 2, 1),
(8, '104/12/02(三) 閱讀沙龍第三場', '<p>104/11/04(三) 閱讀沙龍第二場活動內容</p>\r\n<img src="images/news/1041/10-21.jpg" />\r\n', '2015-10-31 03:00:00', '192.168.0.1', 'oitchinese', 2, 1, 1),
(9, '104/12/02(三) 名家講座(二)：向陽/詩的情感', '<p>104/11/11(三) 名家講座(二)：向陽/詩的情感</p>\r\n<img src="images/news/1041/11-11.jpg" />', '2015-11-01 10:00:00', '192.168.0.1', 'oitchinese', 2, 1, 1);

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `forum_article`
--
ALTER TABLE `forum_article`
  ADD PRIMARY KEY (`articleID`),
  ADD KEY `fk_articlePublisher` (`articlePublisher`);

--
-- 資料表索引 `forum_comment`
--
ALTER TABLE `forum_comment`
  ADD PRIMARY KEY (`commentID`),
  ADD KEY `fk_commentID` (`commentArticleID`),
  ADD KEY `fk_commentClass` (`commentClass`);

--
-- 資料表索引 `forum_discussion`
--
ALTER TABLE `forum_discussion`
  ADD PRIMARY KEY (`discussionID`),
  ADD KEY `fk_discussionComment` (`discussionCommentID`),
  ADD KEY `fk_discussionClass` (`discussionClass`);

--
-- 資料表索引 `member_invitation`
--
ALTER TABLE `member_invitation`
  ADD PRIMARY KEY (`invitationCode`);

--
-- 資料表索引 `web_class`
--
ALTER TABLE `web_class`
  ADD PRIMARY KEY (`classCode`);

--
-- 資料表索引 `web_member`
--
ALTER TABLE `web_member`
  ADD PRIMARY KEY (`memberID`),
  ADD UNIQUE KEY `memberAccount` (`memberAccount`);

--
-- 資料表索引 `web_news`
--
ALTER TABLE `web_news`
  ADD PRIMARY KEY (`newsID`),
  ADD KEY `fk_newsPublisher` (`newsPublisher`);

--
-- 在匯出的資料表使用 AUTO_INCREMENT
--

--
-- 使用資料表 AUTO_INCREMENT `forum_article`
--
ALTER TABLE `forum_article`
  MODIFY `articleID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- 使用資料表 AUTO_INCREMENT `forum_comment`
--
ALTER TABLE `forum_comment`
  MODIFY `commentID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- 使用資料表 AUTO_INCREMENT `forum_discussion`
--
ALTER TABLE `forum_discussion`
  MODIFY `discussionID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- 使用資料表 AUTO_INCREMENT `web_member`
--
ALTER TABLE `web_member`
  MODIFY `memberID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- 使用資料表 AUTO_INCREMENT `web_news`
--
ALTER TABLE `web_news`
  MODIFY `newsID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- 已匯出資料表的限制(Constraint)
--

--
-- 資料表的 Constraints `forum_article`
--
ALTER TABLE `forum_article`
  ADD CONSTRAINT `fk_memberAccount` FOREIGN KEY (`articlePublisher`) REFERENCES `web_member` (`memberAccount`);

--
-- 資料表的 Constraints `forum_comment`
--
ALTER TABLE `forum_comment`
  ADD CONSTRAINT `fk_commentID` FOREIGN KEY (`commentArticleID`) REFERENCES `forum_article` (`articleID`);

--
-- 資料表的 Constraints `forum_discussion`
--
ALTER TABLE `forum_discussion`
  ADD CONSTRAINT `fk_discussion` FOREIGN KEY (`discussionCommentID`) REFERENCES `forum_comment` (`commentID`);

--
-- 資料表的 Constraints `web_news`
--
ALTER TABLE `web_news`
  ADD CONSTRAINT `fk_newsPublisher` FOREIGN KEY (`newsPublisher`) REFERENCES `web_member` (`memberAccount`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
