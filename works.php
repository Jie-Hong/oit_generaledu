<?php
  require_once "./php/functions.php";

  $semester = $_GET['semester'];

  if (isset($semester)) {
    if ($semester === "1041" || $semester === "1042" || $semester === "1051" || $semester === "1052" || $semester === "1061") {
    } else {
      header('Location: 404.php');
    }
  } else {
    header('Location: works.php?semester=1061');
  }
?>
<!DOCTYPE html>
<html lang="zh-Hant-TW">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, , maximum-scale=1">
  <title><?php echo $semester ?> 優秀作品 - 文韻亞東</title>
  <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css"> <!-- Font-Awesome -->
  <link rel="stylesheet" href="outdatedbrowser/outdatedbrowser.css"> <!-- outdatedbrowser 檢查瀏覽器 -->
  <script src="outdatedbrowser/outdatedbrowser.js"></script> <!-- outdatedbrowser 檢查瀏覽器 -->
  <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<header id="header">
  <section class="header-site-channel">
    <div class="container">
      <div class="header-channel-content">
        <div class="header-logo">
          <a href="index.php"><img src="images/logo.png" /></a>
          <span>本課程由教育部資訊及科技教育司支持</span>
          <div class="header-video">
            <a href="./news-detail.php?id=24">
              <!-- <img src="./video/1042.jpg"> -->
              <!-- <i class="fa fa-play-circle-o"></i> -->
            </a>
          </div>
        </div>
        <?php
        if($loggedin)
        {
        echo
          "<div class=\"header-login\">
             <ul>
               <li><a>您好，$user</a></li>
               <li><a href=\"user-edito.php\">會員編輯</a></li>
               <li><a href=\"admin/index.php\">後台管理</a></li>
               <li><a href=\"logout.php\">登出</a></li>
             </ul>
           </div>";
        }
        else
        {
          echo
            "<div class=\"header-login\">
               <ul>
                 <li><a href=\"login.php\">登入</a></li>
                 <li><a href=\"register.php\">註冊</a></li>
               </ul>
             </div>";
        }
        ?>
      </div>
    </div>
  </section>
  <nav class="navbar" role="navigation">
    <div class="container">
      <div class="navbar-collapse">
        <ul class="nav navbar-nav">
          <li><a href="news.php">最新消息</a></li>
          <li><a href="intro.php">課程計畫</a></li>
          <li><a href="forums.php">討論區</a></li>
          <li><a href="vote.php">投票活動</a></li>
          <li><a href="memory.php">亞東印記</a></li>
          <li class="active"><a href="works.php">優秀作品</a></li>
          <li><a href="picture.php">影像紀錄</a></li>
          <li><a href="activity.php">活動集錦</a></li>
          <li><a href="videosharing.php">影片分享</a></li>
          <li><a href="feedback.php">TA回饋分享</a></li>
          <li><a href="team.php">核心團隊</a></li>
          <li><a href="links.php">相關資源</a></li>
        </ul>
      </div>
    </div>
  </nav>
</header>
<section class="works">
  <div class="container">
    <h1><span>Outstanding works</span>優秀作品</h1>
  </div>
</section>
<section class="ta-main">
  <div class="container">
    <div class="breadcrumb">
      <ul>
        <li><a href="index.php">首頁</a></li>
        <li><a href="works.php">優秀作品</a></li>
        <li><a href="picture.php?semester=<?php echo $semester ?>">第<?php echo $semester ?>學期</a></li>
      </ul>
    </div>

    <div class="team-select">
      <select id="team-select" class="team-dropdown">
        <option value="1061" <?php if ($semester==="1061"){ echo "selected"; }?>>1061 學期</option>
        <option value="1051" <?php if ($semester==="1051"){ echo "selected"; }?>>1051 學期</option>
        <option value="1041" <?php if ($semester==="1041"){ echo "selected"; }?>>1041 學期</option>
      </select>
    </div>

    <div class="works-content" id="works-content">
      <?php if ( $semester === "1041") { ?>
      <div class="works-class">
        <h3>通訊一A <small>104-1優秀作品</small></h3>
        <div class="row">
          <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <div class="works-box">
              <img src="./images/works/1041/1041-ce-a/1-small-104109106.jpg" data-img="./images/works/1041/1041-ce-a/1-104109106.jpg"/>
              <span class="type">1-自我探索</span>
              <h5>辛承恩</h5>
            </div>
          </div>
          <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <div class="works-box">
              <img src="./images/works/1041/1041-ce-a/2-small-104109111.jpg" data-img="./images/works/1041/1041-ce-a/2-104109111.jpg"/>
              <span class="type">2-自我探索</span>
              <h5>郭德瑜</h5>
            </div>
          </div>
          <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <div class="works-box">
              <img src="./images/works/1041/1041-ce-a/3-small-104109106.jpg" data-img="./images/works/1041/1041-ce-a/3-104109106.jpg"/>
              <span class="type">3-自我探索</span>
              <h5>辛承恩</h5>
            </div>
          </div>
          <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <div class="works-box">
              <img src="./images/works/1041/1041-ce-a/4-small-104109116.jpg" data-img="./images/works/1041/1041-ce-a/4-104109116.jpg"/>
              <span class="type">4-情感的梳理</span>
              <h5>陳彥錩</h5>
            </div>
          </div>
          <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <div class="works-box">
              <img src="./images/works/1041/1041-ce-a/5-small-104109111.jpg" data-img="./images/works/1041/1041-ce-a/5-104109111.jpg"/>
              <span class="type">4-情感的梳理</span>
              <h5>郭德瑜</h5>
            </div>
          </div>
          <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <div class="works-box">
              <img src="./images/works/1041/1041-ce-a/6-small-104109105.jpg" data-img="./images/works/1041/1041-ce-a/6-104109105.jpg"/>
              <span class="type">4-情感的梳理</span>
              <h5>林欣宜</h5>
            </div>
          </div>
        </div>
      </div>
      <div class="works-class">
        <h3>通訊一B <small>104-1優秀作品</small></h3>
        <div class="row">
          <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <div class="works-box">
              <img src="./images/works/1041/1041-ce-b/1-small-104109222.jpg" data-img="./images/works/1041/1041-ce-b/1-104109222.jpg"/>
              <span class="type">1-自我探索</span>
              <h5>沈郁</h5>
            </div>
          </div>
          <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <div class="works-box">
              <img src="./images/works/1041/1041-ce-b/2-small-104109201.jpg" data-img="./images/works/1041/1041-ce-b/2-104109201.jpg"/>
              <span class="type">2-自我探索</span>
              <h5>洪暐珹</h5>
            </div>
          </div>
          <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <div class="works-box">
              <img src="./images/works/1041/1041-ce-b/3-small-104109242.jpg" data-img="./images/works/1041/1041-ce-b/3-104109242.jpg"/>
              <span class="type">3-自我探索</span>
              <h5>劉育維</h5>
            </div>
          </div>
          <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <div class="works-box">
              <img src="./images/works/1041/1041-ce-b/4-small-104109239.jpg" data-img="./images/works/1041/1041-ce-b/4-104109239.jpg"/>
              <span class="type">4-情感的梳理</span>
              <h5>郭彥呈</h5>
            </div>
          </div>
          <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <div class="works-box">
              <img src="./images/works/1041/1041-ce-b/5-small-104109247.jpg" data-img="./images/works/1041/1041-ce-b/5-104109247.jpg"/>
              <span class="type">4-情感的梳理</span>
              <h5>游崇恩</h5>
            </div>
          </div>
          <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <div class="works-box">
              <img src="./images/works/1041/1041-ce-b/5-small-104109246.jpg" data-img="./images/works/1041/1041-ce-b/5-104109246.jpg"/>
              <span class="type">4-情感的梳理</span>
              <h5>廖名崧</h5>
            </div>
          </div>
        </div>
      </div>
      <div class="works-class">
        <h3>醫管一A <small>104-1優秀作品</small></h3>
        <div class="row">
          <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <div class="works-box">
              <img src="./images/works/1041/1041-ha-a/1-small-104110112.jpg" data-img="./images/works/1041/1041-ha-a/1-104110112.jpg"/>
              <span class="type">1-自我探索</span>
              <h5>吳冠葶</h5>
            </div>
          </div>
          <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <div class="works-box">
              <img src="./images/works/1041/1041-ha-a/2-small-104110105.jpg" data-img="./images/works/1041/1041-ha-a/2-104110105.jpg"/>
              <span class="type">2-自我探索</span>
              <h5>汪靖涵</h5>
            </div>
          </div>
          <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <div class="works-box">
              <img src="./images/works/1041/1041-ha-a/3-small-104110113.jpg" data-img="./images/works/1041/1041-ha-a/3-104110113.jpg"/>
              <span class="type">3-自我探索</span>
              <h5>黃靖媛</h5>
            </div>
          </div>
          <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <div class="works-box">
              <img src="./images/works/1041/1041-ha-a/4-small-104110101.jpg" data-img="./images/works/1041/1041-ha-a/4-104110101.jpg"/>
              <span class="type">4-情感的梳理</span>
              <h5>羅軒</h5>
            </div>
          </div>
          <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <div class="works-box">
              <img src="./images/works/1041/1041-ha-a/5-small-104110118.jpg" data-img="./images/works/1041/1041-ha-a/5-104110118.jpg"/>
              <span class="type">4-情感的梳理</span>
              <h5>陳思慈</h5>
            </div>
          </div>
          <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <div class="works-box">
              <img src="./images/works/1041/1041-ha-a/5-small-104110101.jpg" data-img="./images/works/1041/1041-ha-a/5-104110101.jpg"/>
              <span class="type">4-情感的梳理</span>
              <h5>羅軒</h5>>
            </div>
          </div>
        </div>
      </div>
      <div class="works-class">
        <h3>醫管一B <small>104-1優秀作品</small></h3>
        <div class="row">
          <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <div class="works-box">
              <img src="./images/works/1041/1041-ha-b/1-small-104110223.jpg" data-img="./images/works/1041/1041-ha-b/1-104110223.jpg"/>
              <span class="type">1-自我探索</span>
              <h5>吳佩璇</h5>
            </div>
          </div>
          <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <div class="works-box">
              <img src="./images/works/1041/1041-ha-b/2-small-104110241.jpg" data-img="./images/works/1041/1041-ha-b/2-104110241.jpg"/>
              <span class="type">2-自我探索</span>
              <h5>周庭安</h5>
            </div>
          </div>
          <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <div class="works-box">
              <img src="./images/works/1041/1041-ha-b/3-small-104110201.jpg" data-img="./images/works/1041/1041-ha-b/3-104110201.jpg"/>
              <span class="type">3-自我探索</span>
              <h5>池含鳳</h5>
            </div>
          </div>
          <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <div class="works-box">
              <img src="./images/works/1041/1041-ha-b/4-small-104110236.jpg" data-img="./images/works/1041/1041-ha-b/4-104110236.jpg"/>
              <span class="type">4-情感的梳理</span>
              <h5>陳思廷</h5>
            </div>
          </div>
          <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <div class="works-box">
              <img src="./images/works/1041/1041-ha-b/4-small-104110223.jpg" data-img="./images/works/1041/1041-ha-b/4-104110223.jpg"/>
              <span class="type">4-情感的梳理</span>
              <h5>吳佩璇</h5>
            </div>
          </div>
          <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <div class="works-box">
              <img src="./images/works/1041/1041-ha-b/9-small-104110219.jpg" data-img="./images/works/1041/1041-ha-b/9-104110219.jpg"/>
              <span class="type">9-活動企劃書</span>
              <h5>高于雅</h5>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- ***************************************************** 分隔* **************************************************** -->
    <!-- 暫時拿feedback的模板-->
    <?php } else if ( $semester === "1051") { ?>
    <div class="ta-share-content" id="ta-share-content">


    <!--CH2-->

    <div class="works-class">
      <h3 style="font-size: 35px;">單元二－情感的流理</h3>
      <h3>愛情的滋味<h3>
    </div>
    <ul>
      <li>
        <div class="reviews">
          <div class="review-box">
            <img src="images/works/1051/ch2/1/1.jpg" data-img="images/works/1051/ch2/1/1.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/1/2.jpg" data-img="images/works/1051/ch2/1/2.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/1/3.jpg" data-img="images/works/1051/ch2/1/3.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/1/4.jpg" data-img="images/works/1051/ch2/1/4.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/1/5.jpg" data-img="images/works/1051/ch2/1/5.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/1/6.jpg" data-img="images/works/1051/ch2/1/6.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/1/7.jpg" data-img="images/works/1051/ch2/1/7.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/1/8.jpg" data-img="images/works/1051/ch2/1/8.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/1/9.jpg" data-img="images/works/1051/ch2/1/9.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/1/10.jpg" data-img="images/works/1051/ch2/1/10.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/1/11.jpg" data-img="images/works/1051/ch2/1/11.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/1/12.jpg" data-img="images/works/1051/ch2/1/12.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/1/13.jpg" data-img="images/works/1051/ch2/1/13.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/1/14.jpg" data-img="images/works/1051/ch2/1/14.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/1/15.jpg" data-img="images/works/1051/ch2/1/15.jpg" />
          </div>
        </div>
      </li>
    </ul>
    <div class="works-class">
      <h3></h3>
      <h3>呼朋引伴<h3>
    </div>
    <ul>
      <li>
        <div class="reviews">
          <div class="review-box">
            <img src="images/works/1051/ch2/2/1.jpg" data-img="images/works/1051/ch2/2/1.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/2/2.jpg" data-img="images/works/1051/ch2/2/2.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/2/3.jpg" data-img="images/works/1051/ch2/2/3.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/2/4.jpg" data-img="images/works/1051/ch2/2/4.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/2/5.jpg" data-img="images/works/1051/ch2/2/5.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/2/6.jpg" data-img="images/works/1051/ch2/2/6.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/2/7.jpg" data-img="images/works/1051/ch2/2/7.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/2/8.jpg" data-img="images/works/1051/ch2/2/8.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/2/9.jpg" data-img="images/works/1051/ch2/2/9.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/2/10.jpg" data-img="images/works/1051/ch2/2/10.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/2/11.jpg" data-img="images/works/1051/ch2/2/11.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/2/12.jpg" data-img="images/works/1051/ch2/2/12.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/2/13.jpg" data-img="images/works/1051/ch2/2/13.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/2/14.jpg" data-img="images/works/1051/ch2/2/14.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/2/15.jpg" data-img="images/works/1051/ch2/2/15.jpg" />
          </div>
        </div>
      </li>
    </ul>
    <div class="works-class">
      <h3></h3>
      <h3>行孝及時<h3>
    </div>
    <ul>
      <li>
        <div class="reviews">
          <div class="review-box">
            <img src="images/works/1051/ch2/3/1.jpg" data-img="images/works/1051/ch2/3/1.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/3/2.jpg" data-img="images/works/1051/ch2/3/2.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/3/3.jpg" data-img="images/works/1051/ch2/3/3.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/3/4.jpg" data-img="images/works/1051/ch2/3/4.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/3/5.jpg" data-img="images/works/1051/ch2/3/5.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/3/6.jpg" data-img="images/works/1051/ch2/3/6.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/3/7.jpg" data-img="images/works/1051/ch2/3/7.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/3/8.jpg" data-img="images/works/1051/ch2/3/8.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/3/9.jpg" data-img="images/works/1051/ch2/3/9.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/3/10.jpg" data-img="images/works/1051/ch2/3/10.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/3/11.jpg" data-img="images/works/1051/ch2/3/11.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/3/12.jpg" data-img="images/works/1051/ch2/3/12.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/3/13.jpg" data-img="images/works/1051/ch2/3/13.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/3/14.jpg" data-img="images/works/1051/ch2/3/14.jpg" />
          </div>
          <div class="review-box">
            <img src="images/works/1051/ch2/3/15.jpg" data-img="images/works/1051/ch2/3/15.jpg" />
          </div>
        </div>
      </li>
    </ul>


<!--CH1-->
<div class="works-class">
  <h3><h3>
  <h3 style="font-size: 35px;">單元一－自我探索</h3>
  <h3>臨終前的自我回饋<h3>
</div>
<ul>
  <li>
    <div class="reviews">
      <div class="review-box">
        <img src="images/works/1051/ch1/1/1.jpg" data-img="images/works/1051/ch1/1/1.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/1/2.jpg" data-img="images/works/1051/ch1/1/2.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/1/3.jpg" data-img="images/works/1051/ch1/1/3.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/1/4.jpg" data-img="images/works/1051/ch1/1/4.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/1/5.jpg" data-img="images/works/1051/ch1/1/5.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/1/6.jpg" data-img="images/works/1051/ch1/1/6.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/1/7.jpg" data-img="images/works/1051/ch1/1/7.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/1/8.jpg" data-img="images/works/1051/ch1/1/8.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/1/9.jpg" data-img="images/works/1051/ch1/1/9.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/1/10.jpg" data-img="images/works/1051/ch1/1/10.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/1/11.jpg" data-img="images/works/1051/ch1/1/11.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/1/12.jpg" data-img="images/works/1051/ch1/1/12.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/1/13.jpg" data-img="images/works/1051/ch1/1/13.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/1/14.jpg" data-img="images/works/1051/ch1/1/14.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/1/15.jpg" data-img="images/works/1051/ch1/1/15.jpg" />
      </div>
    </div>
  </li>
</ul>
<div class="works-class">
  <h3></h3>
  <h3>與內在的自己對話<h3>
</div>
<ul>
  <li>
    <div class="reviews">
      <div class="review-box">
        <img src="images/works/1051/ch1/2/1.jpg" data-img="images/works/1051/ch1/2/1.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/2/2.jpg" data-img="images/works/1051/ch1/2/2.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/2/3.jpg" data-img="images/works/1051/ch1/2/3.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/2/4.jpg" data-img="images/works/1051/ch1/2/4.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/2/5.jpg" data-img="images/works/1051/ch1/2/5.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/2/6.jpg" data-img="images/works/1051/ch1/2/6.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/2/7.jpg" data-img="images/works/1051/ch1/2/7.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/2/8.jpg" data-img="images/works/1051/ch1/2/8.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/2/9.jpg" data-img="images/works/1051/ch1/2/9.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/2/10.jpg" data-img="images/works/1051/ch1/2/10.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/2/11.jpg" data-img="images/works/1051/ch1/2/11.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/2/12.jpg" data-img="images/works/1051/ch1/2/12.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/2/13.jpg" data-img="images/works/1051/ch1/2/13.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/2/14.jpg" data-img="images/works/1051/ch1/2/14.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/2/15.jpg" data-img="images/works/1051/ch1/2/15.jpg" />
      </div>
    </div>
  </li>
</ul>
<div class="works-class">
  <h3></h3>
  <h3>我的好夢<h3>
</div>
<ul>
  <li>
    <div class="reviews">
      <div class="review-box">
        <img src="images/works/1051/ch1/3/1.jpg" data-img="images/works/1051/ch1/3/1.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/3/2.jpg" data-img="images/works/1051/ch1/3/2.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/3/3.jpg" data-img="images/works/1051/ch1/3/3.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/3/4.jpg" data-img="images/works/1051/ch1/3/4.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/3/5.jpg" data-img="images/works/1051/ch1/3/5.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/3/6.jpg" data-img="images/works/1051/ch1/3/6.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/3/7.jpg" data-img="images/works/1051/ch1/3/7.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/3/8.jpg" data-img="images/works/1051/ch1/3/8.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/3/9.jpg" data-img="images/works/1051/ch1/3/9.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/3/10.jpg" data-img="images/works/1051/ch1/3/10.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/3/11.jpg" data-img="images/works/1051/ch1/3/11.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/3/12.jpg" data-img="images/works/1051/ch1/3/12.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/3/13.jpg" data-img="images/works/1051/ch1/3/13.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/3/14.jpg" data-img="images/works/1051/ch1/3/14.jpg" />
      </div>
      <div class="review-box">
        <img src="images/works/1051/ch1/3/15.jpg" data-img="images/works/1051/ch1/3/15.jpg" />
      </div>
    </div>
  </li>
</ul>
  </div>
  <!-- ***************************************************** 分隔* **************************************************** -->
  <!-- 暫時拿feedback的模板-->
  <?php } else if ( $semester === "1061") { ?>
  <div class="ta-share-content" id="ta-share-content">


  <!--CH2-->

  <div class="works-class">
    <h3 style="font-size: 35px;">單元二－情感的流理</h3>
    <h3>愛情的滋味<h3>
  </div>
  <ul>
    <li>
      <div class="reviews">
        <div class="review-box">
          <img src="images/works/1061/ch2/1/1.jpg" data-img="images/works/1061/ch2/1/1.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/1/2.jpg" data-img="images/works/1061/ch2/1/2.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/1/3.jpg" data-img="images/works/1061/ch2/1/3.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/1/4.jpg" data-img="images/works/1061/ch2/1/4.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/1/5.jpg" data-img="images/works/1061/ch2/1/5.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/1/6.jpg" data-img="images/works/1061/ch2/1/6.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/1/7.jpg" data-img="images/works/1061/ch2/1/7.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/1/8.jpg" data-img="images/works/1061/ch2/1/8.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/1/9.jpg" data-img="images/works/1061/ch2/1/9.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/1/10.jpg" data-img="images/works/1061/ch2/1/10.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/1/11.jpg" data-img="images/works/1061/ch2/1/11.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/1/12.jpg" data-img="images/works/1061/ch2/1/12.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/1/13.jpg" data-img="images/works/1061/ch2/1/13.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/1/14.jpg" data-img="images/works/1061/ch2/1/14.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/1/15.jpg" data-img="images/works/1061/ch2/1/15.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/1/16.jpg" data-img="images/works/1061/ch2/1/16.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/1/17.jpg" data-img="images/works/1061/ch2/1/17.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/1/18.jpg" data-img="images/works/1061/ch2/1/18.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/1/19.jpg" data-img="images/works/1061/ch2/1/19.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/1/20.jpg" data-img="images/works/1061/ch2/1/20.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/1/21.jpg" data-img="images/works/1061/ch2/1/21.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/1/22.jpg" data-img="images/works/1061/ch2/1/22.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/1/23.jpg" data-img="images/works/1061/ch2/1/23.jpg" />
        </div>
      </div>
    </li>
  </ul>
  <div class="works-class">
    <h3></h3>
    <h3>行孝及時<h3>
  </div>
  <ul>
    <li>
      <div class="reviews">
        <div class="review-box">
          <img src="images/works/1061/ch2/2/1.jpg" data-img="images/works/1061/ch2/2/1.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/2/2.jpg" data-img="images/works/1061/ch2/2/2.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/2/3.jpg" data-img="images/works/1061/ch2/2/3.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/2/4.jpg" data-img="images/works/1061/ch2/2/4.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/2/5.jpg" data-img="images/works/1061/ch2/2/5.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/2/6.jpg" data-img="images/works/1061/ch2/2/6.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/2/7.jpg" data-img="images/works/1061/ch2/2/7.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/2/8.jpg" data-img="images/works/1061/ch2/2/8.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/2/9.jpg" data-img="images/works/1061/ch2/2/9.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/2/10.jpg" data-img="images/works/1061/ch2/2/10.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/2/11.jpg" data-img="images/works/1061/ch2/2/11.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/2/12.jpg" data-img="images/works/1061/ch2/2/12.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/2/13.jpg" data-img="images/works/1061/ch2/2/13.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/2/14.jpg" data-img="images/works/1061/ch2/2/14.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/2/15.jpg" data-img="images/works/1061/ch2/2/15.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/2/16.jpg" data-img="images/works/1061/ch2/2/16.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/2/17.jpg" data-img="images/works/1061/ch2/2/17.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/2/18.jpg" data-img="images/works/1061/ch2/2/18.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/2/19.jpg" data-img="images/works/1061/ch2/2/19.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/2/20.jpg" data-img="images/works/1061/ch2/2/20.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/2/21.jpg" data-img="images/works/1061/ch2/2/21.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/2/22.jpg" data-img="images/works/1061/ch2/2/22.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/2/23.jpg" data-img="images/works/1061/ch2/2/23.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/2/24.jpg" data-img="images/works/1061/ch2/2/24.jpg" />
        </div>
        <div class="review-box">
          <img src="images/works/1061/ch2/2/25.jpg" data-img="images/works/1061/ch2/2/25.jpg" />
        </div>
      </div>
    </li>
  </ul>

<!--CH1-->
<div class="works-class">
<h3><h3>
<h3 style="font-size: 35px;">單元一－自我探索</h3>
<h3>臨終前的自我回饋<h3>
</div>
<ul>
<li>
  <div class="reviews">
    <div class="review-box">
      <img src="images/works/1061/ch1/1/1.jpg" data-img="images/works/1061/ch1/1/1.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/1/2.jpg" data-img="images/works/1061/ch1/1/2.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/1/3.jpg" data-img="images/works/1061/ch1/1/3.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/1/4.jpg" data-img="images/works/1061/ch1/1/4.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/1/5.jpg" data-img="images/works/1061/ch1/1/5.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/1/6.jpg" data-img="images/works/1061/ch1/1/6.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/1/7.jpg" data-img="images/works/1061/ch1/1/7.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/1/8.jpg" data-img="images/works/1061/ch1/1/8.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/1/9.jpg" data-img="images/works/1061/ch1/1/9.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/1/10.jpg" data-img="images/works/1061/ch1/1/10.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/1/11.jpg" data-img="images/works/1061/ch1/1/11.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/1/12.jpg" data-img="images/works/1061/ch1/1/12.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/1/13.jpg" data-img="images/works/1061/ch1/1/13.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/1/14.jpg" data-img="images/works/1061/ch1/1/14.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/1/15.jpg" data-img="images/works/1061/ch1/1/15.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/1/16.jpg" data-img="images/works/1061/ch1/1/16.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/1/17.jpg" data-img="images/works/1061/ch1/1/17.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/1/18.jpg" data-img="images/works/1061/ch1/1/18.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/1/19.jpg" data-img="images/works/1061/ch1/1/19.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/1/20.jpg" data-img="images/works/1061/ch1/1/20.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/1/21.jpg" data-img="images/works/1061/ch1/1/21.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/1/22.jpg" data-img="images/works/1061/ch1/1/22.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/1/23.jpg" data-img="images/works/1061/ch1/1/23.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/1/24.jpg" data-img="images/works/1061/ch1/1/24.jpg" />
    </div>
  </div>
</li>
</ul>
<div class="works-class">
<h3></h3>
<h3>我的好夢<h3>
</div>
<ul>
<li>
  <div class="reviews">
    <div class="review-box">
      <img src="images/works/1061/ch1/2/1.jpg" data-img="images/works/1061/ch1/2/1.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/2/2.jpg" data-img="images/works/1061/ch1/2/2.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/2/3.jpg" data-img="images/works/1061/ch1/2/3.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/2/4.jpg" data-img="images/works/1061/ch1/2/4.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/2/5.jpg" data-img="images/works/1061/ch1/2/5.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/2/6.jpg" data-img="images/works/1061/ch1/2/6.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/2/7.jpg" data-img="images/works/1061/ch1/2/7.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/2/8.jpg" data-img="images/works/1061/ch1/2/8.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/2/9.jpg" data-img="images/works/1061/ch1/2/9.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/2/10.jpg" data-img="images/works/1061/ch1/2/10.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/2/11.jpg" data-img="images/works/1061/ch1/2/11.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/2/12.jpg" data-img="images/works/1061/ch1/2/12.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/2/13.jpg" data-img="images/works/1061/ch1/2/13.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/2/14.jpg" data-img="images/works/1061/ch1/2/14.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/2/15.jpg" data-img="images/works/1061/ch1/2/15.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/2/16.jpg" data-img="images/works/1061/ch1/2/16.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/2/17.jpg" data-img="images/works/1061/ch1/2/17.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/2/18.jpg" data-img="images/works/1061/ch1/2/18.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/2/19.jpg" data-img="images/works/1061/ch1/2/19.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/2/20.jpg" data-img="images/works/1061/ch1/2/20.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/2/21.jpg" data-img="images/works/1061/ch1/2/21.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/2/22.jpg" data-img="images/works/1061/ch1/2/22.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/2/23.jpg" data-img="images/works/1061/ch1/2/23.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/2/24.jpg" data-img="images/works/1061/ch1/2/24.jpg" />
    </div>
  </div>
</li>
</ul>
<div class="works-class">
<h3></h3>
<h3>與內在的自己對話<h3>
</div>
<ul>
<li>
  <div class="reviews">
    <div class="review-box">
      <img src="images/works/1061/ch1/3/1.jpg" data-img="images/works/1061/ch1/3/1.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/3/2.jpg" data-img="images/works/1061/ch1/3/2.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/3/3.jpg" data-img="images/works/1061/ch1/3/3.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/3/4.jpg" data-img="images/works/1061/ch1/3/4.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/3/5.jpg" data-img="images/works/1061/ch1/3/5.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/3/6.jpg" data-img="images/works/1061/ch1/3/6.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/3/7.jpg" data-img="images/works/1061/ch1/3/7.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/3/8.jpg" data-img="images/works/1061/ch1/3/8.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/3/9.jpg" data-img="images/works/1061/ch1/3/9.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/3/10.jpg" data-img="images/works/1061/ch1/3/10.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/3/11.jpg" data-img="images/works/1061/ch1/3/11.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/3/12.jpg" data-img="images/works/1061/ch1/3/12.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/3/13.jpg" data-img="images/works/1061/ch1/3/13.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/3/14.jpg" data-img="images/works/1061/ch1/3/14.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/3/15.jpg" data-img="images/works/1061/ch1/3/15.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/3/16.jpg" data-img="images/works/1061/ch1/3/16.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/3/17.jpg" data-img="images/works/1061/ch1/3/17.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/3/18.jpg" data-img="images/works/1061/ch1/3/18.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/3/19.jpg" data-img="images/works/1061/ch1/3/19.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/3/20.jpg" data-img="images/works/1061/ch1/3/20.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/3/21.jpg" data-img="images/works/1061/ch1/3/21.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/3/22.jpg" data-img="images/works/1061/ch1/3/22.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/3/23.jpg" data-img="images/works/1061/ch1/3/23.jpg" />
    </div>
    <div class="review-box">
      <img src="images/works/1061/ch1/3/24.jpg" data-img="images/works/1061/ch1/3/24.jpg" />
    </div>
  </div>
</li>
</ul>
</div>
    <!--年度資料往上插!-->
    <?php } ?>
  </div>
</section>
<footer id="footer">
  <div class="subfooter">
    <div class="container">
      <div class="row">
        <div class="col-xxs-12 col-md-7">
          <div class="footer-nav">
            <ul>
              <li><a href="index.php">首頁</a></li>
              <li><a href="news.php">最新消息</a></li>
              <li><a href="intro.php">課程計畫</a></li>
              <li><a href="forums.php">討論區</a></li>
              <li><a href="works.php">優秀作品</a></li>
              <li><a href="picture.php">影像紀錄</a></li>
              <li><a href="team.php">核心團隊</a></li>
            </ul>
          </div>
        </div>
        <div class="col-xxs-12 col-md-5">
          <div class="school">
            <p>亞東技術學院 通識教育中心 OIT Center for General Education</p>
          </div>
          <div class="plan">
            <p>文韻亞東 ─ 中文閱讀書寫課程革新計畫</p>
          </div>
          <div class="hss">
            <p>本課程由教育部資訊及科技教育司支持</p>
          </div>
        </div>
      </div>
    </div> <!-- container -->
  </div> <!-- subfooter -->
  <div class="copyright">
    <div class="container">
      <div class="text">
        <small>
          <address class="author">
            Copyright  2015-2016.
          </address>
          <a href="http://gecw.oit.edu.tw/bin/home.php" title="亞東技術學院通識教育中心" target="_blank">亞東技術學院通識教育中心</a> All Rights Reserved.
        </small>
      </div>
      <div class="total">
        <?php require_once "./statistics.php"; ?>
      </div>
    </div>
  </div>
</footer>
<div id="works-lightbox" class="works-lightobx">
  <div class="works-lightobx-center">
    <img src="">
  </div>
</div>
<div id="outdated"></div> <!-- outdatedbrowser 檢查瀏覽器 -->
<script src="js/style.js"></script>
<script type="text/javascript"><!-- 學期切換的事件聆聽javascript-->
  document.getElementById("team-select").addEventListener("change", function() {
    document.location.href= "works.php?semester=" + this.value;
  });
</script>
<script src="outdatedbrowser/dom-outdatedbrowser.js"></script> <!-- outdatedbrowser 檢查瀏覽器 -->
<script src="js/works.js"></script>
</body>
</html>
