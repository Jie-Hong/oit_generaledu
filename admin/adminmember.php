<?php
  require_once "../php/functions.php";
  $sql = "SELECT * FROM web_news ORDER BY newsID DESC";
  $result = $db->query($sql);
  /*
    $query  = "SELECT * FROM web_news ORDER BY newsID DESC";
    $result = $conn->query($query);
    if (!$result) die($conn->error);
  */

?>
<!DOCTYPE html>
<html lang="zh-Hant-TW">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, , maximum-scale=1">
    <title>後台管理 - 文韻亞東</title>

    <!-- Kit(bootstrap jquery) -->
    <script src="js/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
    <script src="js/pagination.js" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
    <!-- 日曆 -->
    <script src="js/flatpickr.js" crossorigin="anonymous"></script>
    <!-- Font-Awesome -->
    <link rel="stylesheet" href="../font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css" crossorigin="anonymous">
    <script src="js/bootstrap.min.js" crossorigin="anonymous"></script>
    <!-- 分頁按鈕 -->
    <link rel="stylesheet" type="text/css" href="css/pagination.css">
    <!-- Style -->
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!--日歷 -->
    <link rel="stylesheet" type="text/css" href="css/flatpickr.min.css">
    <script type="text/javascript">
        var day = "";
        var today = "";

        function myFun() {
            if (day = "") {
                console.log(day);
                console.log(document.getElementById("dropdownMenuButton").value);
                console.log(document.getElementById("announcementtitle").value);
                console.log(document.getElementById("announcementtext").value);
            } else {
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1; //January is 0!
                var yyyy = today.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd
                }
                if (mm < 10) {
                    mm = '0' + mm
                }
                today = yyyy + '-' + mm + '-' + dd;
                console.log(today);
                console.log(document.getElementById("dropdownMenuButton").value);
                console.log(document.getElementById("announcementtitle").value);
                console.log(document.getElementById("announcementtext").value);
            }

        }
    </script>
    <!-- End-->
</head>

<body>
    <section>
        <div class="leftpanel">
            <div class="logopanel">
                <h1><span>[</span> 文韻亞東 <span>]</span></h1>
            </div>
            <div class="leftpanelinner">
                <h5 class="sidebartitle">選單</h5>
                <ul class="nav">
                    <ul class="list-group nav">
                        <li><a href="index.php"><i class="fa fa-home"></i> <span>後台主頁</span></a></li>
                        <li><a href="adminnew.php"><i class="fa fa-file-text"></i> <span>最新消息</span></a></li>
                        <li><a href="#"><i class="fa fa-edit"></i> <span>討論區</span></a></li>
                        <li class="active"><a href="#"><i class="fa fa-users"></i> <span>會員管理</span></a></li>
                        <li><a href="#"><i class="fa fa-gift"></i> <span>會員邀請碼</span></a></li>
                    </ul>
            </div>
        </div> <!-- left -->
        <div class="mainpanel">
            <div class="headerbar">
                <div class="header-right">
                    <?php
                    if ($loggedin)
                    {
                        echo $level;
                        if($level<=2){
                        echo "<div class=\"header-login\">
                            <ul class=\"headermenu\">
                            <li><a href=\"../index.php\">首頁</a></li>
                            <li><a><img src=\"User.png\" alt=\"\" style=\"width:30px;height:30px;float:left\">您好，$usernick  </a></li>
                            <li><a href=\"../logout.php\">登出</a></li>
                            </ul>
                        </div>";
                        }else{
                        echo "'<script type=\"text/javascript\">
                        alert(\"權限不足！\");
                        window.history.go(-1); 
                        </script>'";
                        }
                    }
                    else
                    {
                        echo "'<script type=\"text/javascript\">
                        window.history.go(-1); 
                        alert(\"未登入!\");
                        </script>'";
                    }
                     ?>
                </div>
            </div> <!-- headerbar -->
            <div class="pageheader">
                <h2><i class="fa fa-home"></i> 後台主頁 <span>Subtitle goes here...</span></h2>
            </div>
            <div class="contentpanel">
            </div>
        </div>
        </div> <!-- main -->
    </section>
    <table class="table">
    </table>
</body>

<html />