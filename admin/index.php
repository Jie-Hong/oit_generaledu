<?php
  require_once "../php/functions.php";
  $sql = "SELECT * FROM web_news ORDER BY newsID DESC";
  $result = $db->query($sql);
  /////////////////////////////////////////////////////
  $todaytime = date('Y-m-d');
  $sql2 = "SELECT * FROM  web_statistics WHERE today_time = '$todaytime'";
  $result2 = $db->query($sql2);
  $result3 = $db->query($sql2);


  $sql4 = "SELECT sum(today_all) FROM  web_statistics";
  $sth2 = $db->prepare($sql4);
  $sth2->execute();
  $todayall = $sth2->fetchColumn(0);


  $sql3 = "SELECT sum(today_ip) FROM  web_statistics";
  $sth = $db->prepare($sql3);
  $sth->execute();
  $todayip = $sth->fetchColumn(0);

?>
<!DOCTYPE html>
<html lang="zh-Hant-TW">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, , maximum-scale=1">
  <title>後台管理 - 文韻亞東</title>
  <!-- Kit(bootstrap jquery) -->
  <script src="js/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
  <link rel="stylesheet" href="../font-awesome/css/font-awesome.min.css"> <!-- Font-Awesome -->
  <link rel="stylesheet" href="css/bootstrap.min.css" crossorigin="anonymous">
  <script src="js/bootstrap.min.js" crossorigin="anonymous"></script>
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <!-- End-->
  <script type="text/javascript">
    function startTime() {
      var today = new Date();
      var h = today.getHours();
      var m = today.getMinutes();
      var s = today.getSeconds(); // 在小于10的数字钱前加一个‘0’
      m = checkTime(m);
      s = checkTime(s);
      document.getElementById('txt').innerHTML = h + ":" + m + ":" + s;
      t = setTimeout(function () {
        startTime()
      }, 500);
    }

    function checkTime(i) {
      if (i < 10) {
        i = "0" + i;
      }
      return i;
    }
  </script>
</head>

<body onload="startTime()">
  <section>
    <div class="leftpanel">
      <div class="logopanel">
        <h1><span>[</span> 文韻亞東 <span>]</span></h1>
      </div>
      <div class="leftpanelinner">
        <h5 class="sidebartitle">選單</h5>
        <ul class="nav">
          <ul class="list-group nav">
            <li class="active"><a href="index.php"><i class="fa fa-home"></i> <span>後台主頁</span></a></li>
            <li><a href="adminnew.php"><i class="fa fa-file-text"></i> <span>最新消息</span></a></li>
            <li><a href="#"><i class="fa fa-edit"></i> <span>討論區</span></a></li>
            <li><a href="adminmember.php"><i class="fa fa-users"></i> <span>會員管理</span></a></li>
            <li><a href="#"><i class="fa fa-gift"></i> <span>會員邀請碼</span></a></li>
          </ul>
      </div>
    </div> <!-- left -->
    <div class="mainpanel">
      <div class="headerbar">
        <div class="header-right">
          <?php
          // echo $token;
          if ($loggedin)
          {
            if($level<=4){
              echo "<div class=\"header-login\">
                <ul class=\"headermenu\">
                  <li><a href=\"../index.php\">首頁</a></li>
                  <li><a><img src=\"User.png\" alt=\"\" style=\"width:30px;height:30px;float:left\">您好，$usernick  </a></li>
                  <li><a href=\"../logout.php\">登出</a></li>
                </ul>
              </div>";
            }else{
              echo "'<script type=\"text/javascript\">
              alert(\"權限不足！\");
              window.history.go(-1); 
            </script>'";
            }
          }
          else
          {
            echo "'<script type=\"text/javascript\">
            window.history.go(-1); 
            alert(\"未登入!\");
            </script>'";
          }
        ?>
        </div>
      </div> <!-- headerbar -->
      <div class="pageheader">
        <div class="row">
          <div class="col-10">
            <h2><i class="fa fa-home"></i> 後台主頁 <span>Subtitle goes here...</span></h2>
          </div>
          <div class="col-2">
            <div class="row badge badge-dark">
              <h5>現在時間：<span id="txt"></span></h5>
            </div>
          </div>
        </div>
      </div>
      <div class="contentpanel">
        <div class="row">
          <!-- 今日流量 -->
          <div class="col-sm-3 col-md-3">
            <div class="panel panel-info panel-stat">
              <div class="panel-heading">
                <div class="stat">
                  <div class="row">
                    <div class="col-xs-4">
                      <i class="fa fa-area-chart"></i>
                    </div>
                    <div class="col-xs-8">
                      <small class="stat-label">今日流量</small>
                      <h1>
                        <?php
                        while ($row = $result2->fetch()){
                            echo
                            "
                            <p>{$row["today_all"]}</p>

                            ";
                        }
                      ?>
                      </h1>
                    </div>
                  </div> <!-- row -->
                </div>
              </div>
            </div>
          </div>
          <!-- 今日瀏覽不重複人數 -->
          <div class="col-sm-3 col-md-3">
            <div class="panel panel-info panel-stat">
              <div class="panel-heading">
                <div class="stat">
                  <div class="row">
                    <div class="col-xs-4">
                      <i class="fa fa-area-chart"></i>
                    </div>
                    <div class="col-xs-8">
                      <small class="stat-label">今日瀏覽不重複人數</small>
                      <h1>
                        <?php
                      while ($row = $result3->fetch()){
                      echo
                       "
                         <p>{$row["today_ip"]}</p>
                       ";
                      }
                      ?>
                      </h1>
                    </div>
                  </div> <!-- row -->
                </div>
              </div>
            </div>
          </div>
          <!-- 總瀏覽不重複人數 -->
          <div class="col-sm-3 col-md-3">
            <div class="panel panel-info panel-stat">
              <div class="panel-heading">
                <div class="stat">
                  <div class="row">
                    <div class="col-xs-4">
                      <i class="fa fa-area-chart"></i>
                    </div>
                    <div class="col-xs-8">
                      <small class="stat-label">總瀏覽不重複人數</small>
                      <h1>
                        <?php
                       echo
                       "              
                         <p>$todayip</p>
                       ";
                      ?>
                      </h1>
                    </div>
                  </div> <!-- row -->
                </div>
              </div>
            </div>
          </div>
          <!-- 總瀏覽人數 -->
          <div class="col-sm-3 col-md-3">
            <div class="panel panel-info panel-stat">
              <div class="panel-heading">
                <div class="stat">
                  <div class="row">
                    <div class="col-xs-4">
                      <i class="fa fa-area-chart"></i>
                    </div>
                    <div class="col-xs-8">
                      <small class="stat-label">總瀏覽人數</small>
                      <h1>
                        <?php
                       echo
                       "
                         <p>$todayall</p>
                       ";
                      ?>
                      </h1>
                    </div>
                  </div> <!-- row -->
                </div>
              </div>
            </div>
          </div>
          <!-- col -->
        </div>
      </div>
      <div class="contentpanel">
        <canvas id="canvas" style="width:'400'; height:'300';"></canvas>
        <?php
          echo "<script type=\"text/javascript\">
          var ctx = document.getElementById(\"canvas\").getContext(\"2d\");
          var myChart = new Chart(ctx, {
              type: 'line', // line 表示是 曲線圖，當然也可以設置其他的圖表類型 如直條圖 : bar  或者其他
              data: {
                  labels : [\"1月\",\"2月\",\"3月\",\"4月\",\"5月\",\"6月\",\"7月\"], //按時間點 可以分為星期，月，年
                  datasets : [
                      {
                          label: \"瀏覽人數\",  //當前數據的說明
                          fill: true,  //是否要顯示數據部分陰影面積塊  false:不顯示
                          borderColor: \"rgba(200,187,205,1)\",//陣列曲線顏色
                          pointBackgroundColor: \"#fff\", //陣列點的顏色
                          data: [80, 90, 666, 30, 67, 59, 88],  //填充的數據
                      },
                      {
                          label: \"456\",  //當前數據的說明
                          fill: true,  //是否要顯示數據部分陰影面積塊 false:不顯示
                          borderColor: \"rgba(75,192,192,1)\",//陣列曲線顏色
                          pointBackgroundColor: \"#fff\", //陣列點的顏色
                          data: [21, 34, 35, 50, 45, 21, 70],  //填充的數據
                      }
                  ]
              }
          });
              </script>";
        ?>
      </div>
    </div> <!-- main -->
  </section>
</body>

</html>