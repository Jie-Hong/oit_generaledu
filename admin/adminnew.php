<?php
  require_once "../php/functions.php";
  $sql = "SELECT * FROM web_news ORDER BY newsID DESC";
  $result = $db->query($sql);
  /*
    $query  = "SELECT * FROM web_news ORDER BY newsID DESC";
    $result = $conn->query($query);
    if (!$result) die($conn->error);
  */

?>
<!DOCTYPE html>
<html lang="zh-Hant-TW">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, , maximum-scale=1">
    <title>後台管理 - 文韻亞東</title>

    <!-- Kit(bootstrap jquery) -->
    <script src="js/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
    <script src="js/pagination.js" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
    <!-- 日曆 -->
    <script src="js/flatpickr.js" crossorigin="anonymous"></script>
    <!-- Font-Awesome -->
    <link rel="stylesheet" href="../font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css" crossorigin="anonymous">
    <script src="js/bootstrap.min.js" crossorigin="anonymous"></script>
    <!-- 分頁按鈕 -->
    <link rel="stylesheet" type="text/css" href="css/pagination.css">
    <!-- Style -->
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!--日歷 -->
    <link rel="stylesheet" type="text/css" href="css/flatpickr.min.css">
    <script type="text/javascript">
        var day = "";
        var today = "";

        function startTime() {
            var today = new Date();
            var h = today.getHours();
            var m = today.getMinutes();
            var s = today.getSeconds(); // 在小于10的数字钱前加一个‘0’
            m = checkTime(m);
            s = checkTime(s);
            document.getElementById('txt').innerHTML = h + ":" + m + ":" + s;
            t = setTimeout(function () {
                startTime()
            }, 500);
        }

        function checkTime(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }
        
        function myFun() {
            var content = tinyMCE.get('editor1').getContent();;
            // console.log(content);
            if (day != "") {
                //console.log(day);
                //console.log(document.getElementById("dropdownMenuButton").value);
                //console.log(document.getElementById("announcementtitle").value);
                // console.log(document.getElementById("announcementtext").value);
            } else {
                var todayMc = new Date();
                var dd = todayMc.getDate();
                var mm = todayMc.getMonth() + 1; //January is 0!
                var yyyy = todayMc.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd
                }
                if (mm < 10) {
                    mm = '0' + mm
                }
                todayMc = yyyy + '-' + mm + '-' + dd;
                day=todayMc;
                //console.log(day);
                //console.log(document.getElementById("dropdownMenuButton").value);
                //console.log(document.getElementById("announcementtitle").value);
                // console.log(document.getElementById("announcementtext").value);
            }
            // document.getElementById("dropdownMenuButton").value
            var data={
                "title":document.getElementById("announcementtitle").value,
                "content":content,
                "date":day,
                "type":"1",
            };
            $.ajax({
                type: 'POST',
                data: JSON.stringify(data),
                headers: {
                "Authorization": "Basic "+"<?php echo $token?>"
                },
                contentType:"application/json;charset=utf-8",
                url: './InsertNews.php',
                success: function (response) {
                //    console.log(response);
                }
            });
        }
    </script>
    <!-- TinyMCE v4.7.6 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.7.6/tinymce.min.js"></script>
    <!-- <script src="./js/tinymce/tinymce.min.js"></script> -->
    <script src="js/zh_TW.js"></script>
    <!-- ckeditor -->
    <!-- <script src="js/ckeditor/ckeditor.js"></script> -->
    <!-- <script src="js/ckeditor/config.js" type="text/javascript" charset="utf-8"></script> -->
    <!-- End-->
</head>

<body onload="startTime()">
    <section>
        <div class="leftpanel">
            <div class="logopanel">
                <h1><span>[</span> 文韻亞東 <span>]</span></h1>
            </div>
            <div class="leftpanelinner">
                <h5 class="sidebartitle">選單</h5>
                <ul class="nav">
                    <ul class="list-group nav">
                        <li><a href="index.php"><i class="fa fa-home"></i> <span>後台主頁</span></a></li>
                        <li class="active"><a href="adminnew.php"><i class="fa fa-file-text"></i> <span>最新消息</span></a></li>
                        <li><a href="#"><i class="fa fa-edit"></i> <span>討論區</span></a></li>
                        <li><a href="adminmember.php"><i class="fa fa-users"></i> <span>會員管理</span></a></li>
                        <li><a href="#"><i class="fa fa-gift"></i> <span>會員邀請碼</span></a></li>
                    </ul>
            </div>
        </div> <!-- left -->
        <div class="mainpanel">
            <div class="headerbar">
                <div class="header-right">
                    <?php
                    if ($loggedin)
                    {
                        if($level<=4){
                        echo "<div class=\"header-login\">
                            <ul class=\"headermenu\">
                            <li><a href=\"../index.php\">首頁</a></li>
                            <li><a><img src=\"User.png\" alt=\"\" style=\"width:30px;height:30px;float:left\">您好，$usernick  </a></li>
                            <li><a href=\"../logout.php\">登出</a></li>
                            </ul>
                        </div>";
                        }else{
                        echo "'<script type=\"text/javascript\">
                        alert(\"權限不足！\");
                        window.location=\"../index.php\"; 
                        </script>'";
                        }
                    }
                    else
                    {
                        echo "'<script type=\"text/javascript\">
                        window.location=\"../index.php\"; 
                        alert(\"未登入!\");
                        </script>'";
                    }
                     ?>
                </div>
            </div> <!-- headerbar -->
            <div class="pageheader">
                <div class="row">
                    <div class="col-10">
                        <h2><i class="fa fa-home"></i> 最新公告 <span>Subtitle goes here...</span></h2>
                    </div>
                    <div class="col-2">
                        <div class="row badge badge-dark">
                            <h5>現在時間：<span id="txt"></span></h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="contentpanel">
                <div>
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">
                        新增公告
                    </button>
                    <!-- Modal -->
                    <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog"
                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <script>
                            tinyMCE.init({
                            // 初始化參數設定[註1]
                            selector: "textarea", // 目標物件
                            auto_focus: "editor1", // 聚焦物件
                            height: '500px',
                            // images_upload_url: "./postAcceptor.php" + '?method=country-img', // 圖片上傳的具體地址，該選項一定需要設置，才會出現圖片上傳選項
                            theme: "modern", // 模板風格
                            plugins: [ //image跟imagetools为开启图片上传的插件功能 fullpage
                                'advlist autolink lists link image charmap print preview hr anchor pagebreak imagetools',
                                'searchreplace visualblocks visualchars code ',
                                'insertdatetime media nonbreaking save table contextmenu directionality',
                                'emoticons paste textcolor colorpicker textpattern imagetools codesample'
                            ], // 套件設定: 進階清單、自動連結、連結、上傳圖片、清單、特殊字元表、列印、預覽
                            menubar: 'edit insert view format table tools',
                            toolbar1: ' newnote print preview | undo redo | insert | styleselect | forecolor backcolor bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image emoticons media codesample',
                            image_advtab: true, //開啟圖片上傳的高級選項功能
                            images_upload_handler: function (blobInfo, success, failure) {
                            var xhr, formData;
                            xhr = new XMLHttpRequest();
                            xhr.withCredentials = false;
                            xhr.open('POST', '../postAcceptor.php');
                            xhr.onload = function() {
                            var json;
                            // console.log(xhr);
                            if (xhr.status == 400) {
                                failure('HTTP Error:' + xhr.status+"上傳文件錯誤,確認有無中文");
                                return;
                            }
                            if (xhr.status != 200) {
                                failure('HTTP Error: ' + xhr.status);
                                return;
                            }
                            json = JSON.parse(xhr.responseText);
                            if (!json || typeof json.location != 'string') {
                                failure('Invalid JSON: ' + xhr.responseText);
                                return;
                            }
                            success(json.location);
                            };
                            formData = new FormData();
                            formData.append('file', blobInfo.blob(), blobInfo.filename());

                            xhr.send(formData);
                        }

                        });
                        
                        // Bootstrap會阻止對話框中內容的所有焦點事件。將此腳本添加到您的頁面，它將允許用戶在編輯器中單擊。
                        $(document).on('focusin', function(e) {
                            if ($(e.target).closest(".mce-window").length) {
                                e.stopImmediatePropagation();
                            }
                        });
                    </script>
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">新增公告</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <h5>請選擇日期:</h5>
                                    <?php
                                    $today = getdate();
                                    date("Y/m/d H:i");  //日期格式化
                                    $year=$today["year"]; //年 
                                    $month=$today["mon"]; //月
                                    $day=$today["mday"];  //日                       
                                    if(strlen($month)=='1')$month='0'.$month;
                                    if(strlen($day)=='1')$day='0'.$day;
                                    $today=$year."-".$month."-".$day;
                                    echo "<input id=\"time\" class=\"flatpickr form-control\" data-date-format=\"Y-m-d\"  placeholder=\"...\" data-default-date=\"today\" >";
                                    ?>
                                    <?php
                                     echo "<script type=\"text/javascript\">
                                     $(\"#time\").flatpickr();   // jQuery初始化方法
                                     $(\"#time\").flatpickr({
                                        // defaultDate: new Date(new Date() - 1000*24*3600 ),//默認昨天，計算日期表達式
                                        onChange: function(dateObj, dateStr, instance) {
                                        // 　　var day = Math.floor((new Date() -  new Date(dateObj))/24000/3600);
                                        day=dateStr;

                                        },
                                     });  
                                     </script>";
                                    ?>
                                    <h5>請選擇類型:</h5>
                                    <div class="dropdown" style="margin-top:5px">
                                        <input class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                                            value="Type" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        </input>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item">資訊</a>
                                            <a class="dropdown-item">活動</a>
                                        </div>
                                    </div>
                                    <?php
                                     echo "<script type=\"text/javascript\"> 
                                     $(\".dropdown-menu a\" ).click(function() {
                                        var selText = $(this).text();
                                        // console.log(selText);
                                        // console.log($(this).parents('.dropdown').find('.dropdown-toggle'));
                                        $(this).parents('.dropdown').find('.dropdown-toggle').val(selText);
                                      });                                    
                                     </script>";
                                    ?>
                                    <h5 style="margin-top:5px;">請輸入標題:</h5>
                                    <div class="input-group input-group-sm mb-3">
                                        <input type="text" class="form-control" id="announcementtitle" aria-label="Small"
                                            aria-describedby="inputGroup-sizing-sm">
                                    </div>
                                    <h5 style="margin-top:5px;">請輸入公告:</h5>
                                    <textarea id="editor1"></textarea>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" id="send" data-dismiss="modal" onClick="myFun()" class="btn btn-primary">送出</button>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="data-container" style="margin-top:10px">

                    <table class="table table-striped">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">日期</th>
                                <th scope="col">分類</th>
                                <th scope="col">標題</th>
                                <th scope="col">瀏覽人數</th>
                            </tr>
                        </thead>
                        <tbody id="one">

                        </tbody>
                    </table>
                </div>
                <div id="demo"></div>
                <?php
                echo "<script type=\"text/javascript\">
                var template=function(data){
                    return data;
                }
                $('#demo').pagination({
                    dataSource: function(done) {
                        $.ajax({
                            type: 'POST',
                            url: 'http://localhost/oitge/php/getNew.php',
                            dataType: 'json',
                            success: function(response) {
                                var test=[];
                                for(var i =0 ; i<response.length;i++){
                                    test.push(response[i]);
                                }
                                done(test);
                            }
                        });
                     },
                    pageSize: 10,
                    locator: 'data.date',
                    className: 'paginationjs-theme-blue paginationjs-big',
                    callback: function(data, pagination) {
                        $(\"#one\").empty();
                        for(var i=0;i<data.length;i++){
                            $(\"#one\").append(\"<tr class='newslist'><th scope='row'>\"+(i+1)+\"</th>\"
                            +\"<td>\"+data[i].date+\"</td>\"
                            +\"<td><span class='type' data-type=\"+data[i].type+\">\"+data[i].typeName+\"</span></td>\"
                            +\"<td>\"+data[i].title+\"+</td>\"
                            +\"<td>\"+data[i].viewer+\"</td>\"
                            +\"</tr>\");         // 追加新元素
                        }
                    }
                })
                </script>";
                
                ?>
            </div>
        </div>
        </div> <!-- main -->
    </section>
    <table class="table">

    </table>
</body>

<html />