<?php
require_once 'checkToken.php';
if($account!=NULL){
    function get_client_ip() {
        foreach (array(
            'HTTP_CLIENT_IP',
            'HTTP_X_FORWARDED_FOR',
            'HTTP_X_FORWARDED',
            'HTTP_X_CLUSTER_CLIENT_IP',
            'HTTP_FORWARDED_FOR',
            'HTTP_FORWARDED',
            'REMOTE_ADDR') as $key) {
            if (array_key_exists($key, $_SERVER)) {
                foreach (explode(',', $_SERVER[$key]) as $ip) {
                    $ip = trim($ip);
                    if ((bool) filter_var($ip, FILTER_VALIDATE_IP,
                        FILTER_FLAG_IPV4 |
                        FILTER_FLAG_NO_PRIV_RANGE |
                        FILTER_FLAG_NO_RES_RANGE)) {
                        return $ip;
                    }
                }
            }
        }
        return $ip;
    }
    $data = file_get_contents('php://input');
    $data = json_decode($data, 1);
    $title = $data['title'];
    $content = explode('"', $data['content']);
    $content = implode('\"', $content);
    $date = $data['date'];
    $type = $data['type'];
    $ip = get_client_ip();
    //計算個數
    $sql="SELECT COUNT(*) FROM `web_news`";
    $resultCnt=$db->query($sql);
    $resultCnt->execute();
    $rowCount=$resultCnt->fetchColumn();
    $rowCount=$rowCount+1;
    //新增部分
    $query = "INSERT INTO `web_news` (`newsID`, `newsTitle`, `newsContent`, `newsDate`, `newsIP`, `newsPublisher`, `newsType`, `newsViewer`, `newsVisible`) VALUES ('$rowCount','$title', '$content', '$date', '$ip', '$account', '$type', '0', '1')";
    // $query = "INSERT INTO `web_news` (`newsID`, `newsTitle`, `newsContent`, `newsDate`, `newsIP`, `newsPublisher`, `newsType`, `newsViewer`, `newsVisible`) VALUES (NULL, 'test', 'test', '2018-09-15', '192.168.0.1', '104111236', '1', '0', '1')";
    // $result = 0;
    if($account){
        $result = $db->query($query);
        if($result){
            echo "true";
        }else{
            echo "傳值錯誤<br>";
            echo "false";
        }
    }else{
        echo "登入錯誤<br>";
        echo "false";
    }
}else{
    echo "沒有權限";
}


?>