﻿<?php
  require_once './php/functions.php';
  $error = $user = $pass = $level = "";

  if (isset($_POST['username']))
  {
    $user = $_POST['username'];
    $pass = $_POST['password'];
      if ($user == "" || $pass == "")
        $error = "請輸入使用者名稱或密碼<br>";
      else
      {
        $sql = "SELECT memberAccount,memberPassword,memberLevel,memberNickname FROM web_member WHERE memberAccount=\"$user\"";
        $result = $db->query($sql);
        $db = null;
        $row = $result->rowCount();
        $rowMember = $result->fetch();
		    $passwordHash=$rowMember['memberPassword'];
        $passwordCheck = password_verify($pass,$passwordHash);
        $userlevel = $rowMember['memberLevel'];
        $usernick = $rowMember['memberNickname'];
        $usertoken = $rowMember['memberPassword'];

        if ($row == 0 || $passwordCheck === false)
        {
          $error = "1";
        } else {
          $_SESSION['user'] = $user;
          //$_SESSION['pass'] = $pass;
          $_SESSION['nick'] = $usernick;
          $_SESSION['level'] = $userlevel;
          $_SESSION['token']=$usertoken;
          header('Location: index.php');
        }
      }
    }
?>
<!DOCTYPE html>
<html lang="zh-Hant-TW">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, , maximum-scale=1">
  <title>登入 - 文韻亞東</title>
  <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css"> <!-- Font-Awesome -->
  <link rel="stylesheet" href="outdatedbrowser/outdatedbrowser.css"> <!-- outdatedbrowser 檢查瀏覽器 -->
  <script src="outdatedbrowser/outdatedbrowser.js"></script> <!-- outdatedbrowser 檢查瀏覽器 -->
  <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body class="login-bg">
<div class="login-box">
  <h1>Login</h1>
  <?php
    if ($error == 1){
      echo "<p>登入失敗，帳號或密碼錯誤</p>";
    }
      echo
        "<form action=\"login.php\" method=\"post\" class=\"form-login\">
          <div class=\"form__field\">
            <label class=\"fontawesome-user\" for=\"login__username\"><i class=\"fa fa-user\"></i></label>
            <input id=\"login__username\" type=\"text\" class=\"form__input\" placeholder=\"帳號 Username\" name=\"username\"  required>
          </div>
          <div class=\"form__field\">
            <label class=\"fontawesome-lock\" for=\"login__password\"><i class=\"fa fa-key\"></i></label>
            <input id=\"login__password\" type=\"password\" class=\"form__input\" placeholder=\"密碼 Password\" name=\"password\" required>
          </div>";
    ?>
          <div class="form__field">
            <input type="submit" value="Sign In">
            </div>
        </form>
        <p class="text-center">沒有帳號嗎? <a href="register.php">馬上註冊</a> <i class="fa fa-arrow-right"></i></p>
</div>
<div id="outdated"></div> <!-- outdatedbrowser 檢查瀏覽器 -->
<script src="js/style.js"></script>
<script src="outdatedbrowser/dom-outdatedbrowser.js"></script> <!-- outdatedbrowser 檢查瀏覽器 -->
</body>
</html>