<?php
  require_once './php/functions.php';
  
  if (isset($_SESSION['user']))
  {
	  destroySession();
	  header('Location: index.php');
  }
  else
  {
	  header('Location: 404.php');
	  }
?>