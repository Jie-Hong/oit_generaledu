<?php
  require_once "./php/functions.php";

  $sql = "SELECT * FROM web_news ORDER BY newsID DESC LIMIT 0, 5 ";
  $result = $db->query($sql);
  /*
    $query  = "SELECT * FROM web_news ORDER BY newsID DESC";
    $result = $conn->query($query);
    if (!$result) die($conn->error);
  */
$todaytime = date('Y-m-d');
$sth777=$db->prepare("select count(*) from web_statistics WHERE today_time = '$todaytime'");
$sth777->execute();
$rowCount=$sth777->fetchColumn();

if ( $rowCount == 0 ) {
  $sth222=$db->prepare("INSERT INTO web_statistics VALUES (0, 0, '$todaytime') ");
  $sth222->execute();
}

/////////////////////////////////////////////

$todaytime = date('Y-m-d');
$sql2 = "SELECT * FROM  web_statistics WHERE today_time = '$todaytime'";
$result2 = $db->query($sql2);

$sql4 = "SELECT sum(today_all) FROM  web_statistics";
$sth2 = $db->prepare($sql4);
$sth2->execute();
$todayall = $sth2->fetchColumn(0);


$sql3 = "SELECT sum(today_ip) FROM  web_statistics";
$sth = $db->prepare($sql3);
$sth->execute();
$todayip = $sth->fetchColumn(0);


?>
<!DOCTYPE html>
<html lang="zh-Hant-TW">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, , maximum-scale=1">
  <title>文韻亞東</title>
  <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css"> <!-- Font-Awesome -->
  <link rel="stylesheet" href="outdatedbrowser/outdatedbrowser.css"> <!-- outdatedbrowser 檢查瀏覽器 -->
  <script src="outdatedbrowser/outdatedbrowser.js"></script> <!-- outdatedbrowser 檢查瀏覽器 -->
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" type="text/css" href="css/banner.css"> <!-- Banner -->
</head>
<body>
<header id="header">
  <section class="header-site-channel">
    <div class="container">
      <div class="header-channel-content">
        <div class="header-logo">
          <a href="index.php"><img src="images/logo.png" /></a>
          <span>本課程由教育部資訊及科技教育司支持</span>
          <div class="header-video">
            <a href="./news-detail.php?id=24">
              <!-- <img src="./video/1042.jpg"> -->
              <!-- <i class="fa fa-play-circle-o"></i> -->
            </a>
          </div>
        </div>
        <?php
          if ($loggedin)
          {
            echo "<div class=\"header-login\">
                    <ul>
                      <li><a>您好，$user</a></li>
                      <li><a href=\"user-edito.php\">會員編輯</a></li>
                      <li><a href=\"admin/index.php\">後台管理</a></li>
                      <li><a href=\"logout.php\">登出</a></li>
                    </ul>
                  </div>";
          }
          else
          {
            echo "<div class=\"header-login\">
                    <ul>
                      <li><a href=\"login.php\">登入</a></li>
                      <li><a href=\"register.php\">註冊</a></li>
                    </ul>
                  </div>";
          }
        ?>
      </div>
    </div>
  </section>
  <nav class="navbar" role="navigation">
    <div class="container">
      <div class="navbar-collapse">
        <ul class="nav navbar-nav">
          <li><a href="news.php">最新消息</a></li>
          <li><a href="intro.php">課程計畫</a></li>
          <li><a href="forums.php">討論區</a></li>
          <li><a href="vote.php">投票活動</a></li>
          <li><a href="memory.php">亞東印記</a></li>
          <li><a href="works.php">優秀作品</a></li>
          <li><a href="picture.php">影像紀錄</a></li>
          <li><a href="activity.php">活動集錦</a></li>
          <li><a href="videosharing.php">影片分享</a></li>
          <li><a href="feedback.php">TA回饋分享</a></li>
          <li><a href="team.php">核心團隊</a></li>
          <li><a href="links.php">相關資源</a></li>
        </ul>
      </div>
    </div>
  </nav>
</header>
<section class="carousel">
  <div class="container">
    <div id="carousel-content" class="carousel-content">
      <div class="carousel-box">
        <ul>
          <li class="active"><img src="images/carousel/01.jpg" /></li>
          <li class="right"><img src="images/carousel/02.jpg" /></li>
          <li><img src="images/carousel/03.jpg" /></li>
          <li><img src="images/carousel/04.jpg" /></li>
          <li><img src="images/carousel/05.jpg" /></li>
        </ul>
      </div>
      <ol id="carousel-indicators" class="carousel-indicators">
        <li data-slideto="0" class="active"></li>
        <li data-slideto="1"></li>
        <li data-slideto="2"></li>
        <li data-slideto="3"></li>
        <li data-slideto="4"></li>
      </ol>
    </div>
    <div class="announcement-top">
    </div>
  </div>
  <a href="javascript: void(0)" class="carousel-btn" id="carousel-left-btn"><i class="fa fa-chevron-left"></i></a>
  <a href="javascript: void(0)" class="carousel-btn" id="carousel-right-btn"><i class="fa fa-chevron-right"></i></a>
</section>
<section class="announcement-index">
  <div class="container">
    <div class="announcement-contents">
      <h2>What's News<p><span>最新消息</span></p></h2>
      <div class="row">
        <div class="col-xs-12 col-md-9">
          <ul class="newslist">
          <?php
            while ($row = $result->fetch()){
              $title=htmlspecialchars($row['newsTitle'], ENT_QUOTES);
              $date=substr($row['newsDate'],0,10);
              $newsType=$row['newsType'];
              //顯示消息類別
              switch($newsType){
                case '1':
                  $newsTypeName = '資訊';
                  break;
                case '2':
                  $newsTypeName = '活動';
                break;
                case '3':
                  $newsTypeName = '其他';
                break;
              }

              echo
                "<li>
                  <span class=\"date\">$date</span>
                  <span class=\"type\" data-type=\"$newsType\">$newsTypeName</span>
                  <a href=\"news-detail.php?id={$row['newsID']}\">$title</a>
                </li>";
            }
          ?>
          </ul>
          <div class="more-announcement">
            <a href="news.php"><i class="fa fa-chevron-circle-right"></i>更多公告.....</a>
          </div>
        </div>
        <div class="col-xs-12 col-md-3">
          <div class="activity-img-list">
            <ul>
              <li><a href="./vote-detail.php?id=3"><img src="./images/vote/06.png" /></a></li>
              <li><a href="./memory.php#2016-12"><img src="./images/memory/cover-2018-9.jpg" /></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="students-works">
  <div class="container">
    <div class="students-works-title">
      <h2>學生優秀作品<span>Student Outstanding Works</span></h2>
      <div class="more-students-works">
        <a href="works.php">More 更多作品<i class="fa fa-chevron-right"></i></a>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-6 col-md-4">
        <div class="students-works-box">
          <figure>
            <img src="./images/works/1041/1041-ce-b/1-small-104109222.jpg" alt="">
          </figure>
          <div class="text">
            <p class="tag">1041：自我探索</p>
            <h3>沈郁</h3>
            <p>興趣要如何才能發展或工作...</p>
          </div>
        </div>
      </div>
      <div class="col-xs-6 col-md-4">
        <div class="students-works-box">
          <figure>
            <img src="./images/works/1041/1041-ce-a/6-small-104109105.jpg" alt="">
          </figure>
          <div class="text">
            <p class="tag">1041：情感的梳理</p>
            <h3>林欣宜</h3>
            <p>許多文章或評論，都會讚賞杜十娘的寧死不屈，批評李甲的負心...</p>
          </div>
        </div>
      </div>
      <div class="col-xs-6 col-md-4">
        <div class="students-works-box">
          <figure>
            <img src="./images/works/1041/1041-ha-b/1-small-104110223.jpg" alt="">
          </figure>
          <div class="text">
            <p class="tag">1041：自我探索</p>
            <h3>吳佩璇</h3>
            <p>對你來說什麼是有用?什麼事沒用?</p>
          </div>
        </div>
      </div>
  </div>
</section>

<section class="related-records">
  <div class="container">
    <div class="col-md-8">
    <div class="col-md-4">
      <div class="related-links">
        <h2>相關連結 <span>links</span></h2>
        <ul>
          <li><a href="http://www.oit.edu.tw/bin/home.php" title="亞東技術學院" target="_blank"><img src="images/links/oit.png"></a></li>
        </ul>
      </div>
      <div class="statistics">
        <h2>網站統計</h2>
        <div class="data">
          <?php
          while ($row = $result2->fetch()){
           echo
            "
               <p>今日瀏覽不重複人數：{$row["today_ip"]}</p>
               <p>今日瀏覽人數： {$row["today_all"]}</p>
               <p>總瀏覽不重複人數：$todayip</p>
               <p>總瀏覽人數：$todayall</p>
            ";
          }
          ?>
          <p>開始統計時間：2016/08/09</p>
        </div>
      </div>
    </div>
  </div>
</section>
<footer id="footer">
  <div class="subfooter">
    <div class="container">
      <div class="row">
        <div class="col-xxs-12 col-md-7">
          <div class="footer-nav">
            <ul>
              <li><a href="index.php">首頁</a></li>
              <li><a href="news.php">最新消息</a></li>
              <li><a href="intro.php">課程計畫</a></li>
              <li><a href="forums.php">討論區</a></li>
              <li><a href="works.php">優秀作品</a></li>
              <li><a href="picture.php">影像紀錄</a></li>
              <li><a href="team.php">核心團隊</a></li>
            </ul>
          </div>
        </div>
        <div class="col-xxs-12 col-md-5">
          <div class="school">
            <p>亞東技術學院 通識教育中心 OIT Center for General Education</p>
          </div>
          <div class="plan">
            <p>文韻亞東 ─ 中文閱讀書寫課程革新計畫</p>
          </div>
          <div class="hss">
            <p>本課程由教育部資訊及科技教育司支持</p>
          </div>
        </div>
      </div>
    </div> <!-- container -->
  </div> <!-- subfooter -->
  <div class="copyright">
    <div class="container">
      <div class="text">
        <small>
          <address class="author">
            Copyright  2015-2016.
          </address>
          <a href="http://gecw.oit.edu.tw/bin/home.php" title="亞東技術學院通識教育中心" target="_blank">亞東技術學院通識教育中心</a> All Rights Reserved.
        </small>
      </div>
      <div class="total">
        <?php require_once "./statistics.php"; ?>
      </div>
    </div>
  </div>
</footer>
<div id="outdated"></div> <!-- outdatedbrowser 檢查瀏覽器 -->
<script src="js/style.js"></script>
<script src="js/banner.js"></script>
<script src="outdatedbrowser/dom-outdatedbrowser.js"></script> <!-- outdatedbrowser 檢查瀏覽器 -->
</body>
</html>
