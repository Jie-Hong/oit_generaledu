<?php
  require_once "./php/functions.php";

  $semester = $_GET['semester'];

  if (isset($semester)) {
    if ($semester === "1041" || $semester === "1042" || $semester === "1051" || $semester === "1052" || $semester === "1061" || $semester === "1062" || $semester === "1071") {
    } else {
      header('Location: 404.php');
    }
  } else {
    header('Location: team.php?semester=1071');
  }
?>
<!DOCTYPE html>
<html lang="zh-Hant-TW">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, , maximum-scale=1">
  <title>
    <?php echo $semester ?> 核心團隊 - 文韻亞東</title>
  <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css"> <!-- Font-Awesome -->
  <link rel="stylesheet" href="outdatedbrowser/outdatedbrowser.css"> <!-- outdatedbrowser 檢查瀏覽器 -->
  <script src="outdatedbrowser/outdatedbrowser.js"></script> <!-- outdatedbrowser 檢查瀏覽器 -->
  <link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>
  <header id="header">
    <section class="header-site-channel">
      <div class="container">
        <div class="header-channel-content">
          <div class="header-logo">
            <a href="index.php"><img src="images/logo.png" /></a>
            <span>本課程由教育部資訊及科技教育司支持</span>
            <div class="header-video">
              <a href="./news-detail.php?id=24">
                <!-- <img src="./video/1042.jpg"> -->
                <i class=""></i>
              </a>
            </div>
          </div>
          <?php
        if($loggedin)
        {
        echo
          "<div class=\"header-login\">
             <ul>
               <li><a>您好，$user</a></li>
               <li><a href=\"user-edito.php\">會員編輯</a></li>
               <li><a href=\"admin/index.php\">後台管理</a></li>
               <li><a href=\"logout.php\">登出</a></li>
             </ul>
           </div>";
        }
        else
        {
          echo
            "<div class=\"header-login\">
               <ul>
                 <li><a href=\"login.php\">登入</a></li>
                 <li><a href=\"register.php\">註冊</a></li>
               </ul>
             </div>";
        }
        ?>
        </div>
      </div>
    </section>
    <nav class="navbar" role="navigation">
      <div class="container">
        <div class="navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="news.php">最新消息</a></li>
            <li><a href="intro.php">課程計畫</a></li>
            <li><a href="forums.php">討論區</a></li>
            <li><a href="vote.php">投票活動</a></li>
            <li><a href="memory.php">亞東印記</a></li>
            <li><a href="works.php">優秀作品</a></li>
            <li><a href="picture.php">影像紀錄</a></li>
            <li><a href="activity.php">活動集錦</a></li>
            <li><a href="videosharing.php">影片分享</a></li>
            <li><a href="feedback.php">TA回饋分享</a></li>
            <li class="active"><a href="team.php">核心團隊</a></li>
            <li><a href="links.php">相關資源</a></li>
          </ul>
        </div>
      </div>
    </nav>
  </header>
  <section class="team-title">
    <div class="container">
      <h1><span>Team</span>核心團隊</h1>
    </div>
  </section>
  <article class="team-main">
    <div class="container">
      <div class="breadcrumb">
        <ul>
          <li><a href="index.php">首頁</a></li>
          <li><a href="team.php">核心團隊</a></li>
          <li><a href="team.php?semester=<?php echo $semester ?>">第
              <?php echo $semester ?>學期</a></li>
        </ul>
      </div>
      <div class="team-select">
        <select id="team-select" class="team-dropdown">
          <option value="1071" <?php if ($semester==="1071" ){ echo "selected" ; }?>>1071 學期</option>
          <option value="1062" <?php if ($semester==="1062" ){ echo "selected" ; }?>>1062 學期</option>
          <option value="1061" <?php if ($semester==="1061" ){ echo "selected" ; }?>>1061 學期</option>
          <option value="1052" <?php if ($semester==="1052" ){ echo "selected" ; }?>>1052 學期</option>
          <option value="1051" <?php if ($semester==="1051" ){ echo "selected" ; }?>>1051 學期</option>
          <option value="1042" <?php if ($semester==="1042" ){ echo "selected" ; }?>>1042 學期</option>
          <option value="1041" <?php if ($semester==="1041" ){ echo "selected" ; }?>>1041 學期</option>
        </select>
      </div>
      <div class="team-content">
        <?php if ( $semester === "1041") { ?>
        <section class="team-teacher">
          <h2><span>計畫教師群</span></h2>
          <ul>
            <li>
              <img src="images/team/teacher-01.jpg">
              <h3>林智莉</h3>
              <span class="titles">亞東技術學院通識教育中心副教授兼中心主任</span>
              <span class="educational">政治大學中國文學系博士</span>
              <span class="email"><a href="mailto:fb055@mail.oit.edu.tw" class="email" target="_blank" title="信箱">fb055@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/teacher-02.jpg">
              <h3>吳憶蘭</h3>
              <span class="titles">亞東技術學院通識教育中心副教授</span>
              <span class="educational">文化大學中國文學系博士</span>
              <span class="email"><a href="mailto:fb026@mail.oit.edu.tw" class="email" target="_blank" title="信箱">fb026@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/teacher-03.jpg">
              <h3>傅孝維</h3>
              <span class="titles">亞東技術學院通識教育中心副教授</span>
              <span class="educational">University of Hull心理諮商學系博士</span>
              <span class="email"><a href="mailto:fb045@mail.oit.edu.tw" class="email" target="_blank" title="信箱">fb045@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/teacher-04.jpg">
              <h3>黃啟峰</h3>
              <span class="titles">亞東技術學院通識教育中心助理教授</span>
              <span class="educational">中央大學中國文學系博士</span>
              <span class="email"><a href="mailto:chifeng@mail.oit.edu.tw" class="email" target="_blank" title="信箱">chifeng@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/teacher-05.jpg">
              <h3>李慧琪</h3>
              <span class="titles">亞東技術學院通識教育中心助理教授</span>
              <span class="educational">中央大學中國文學系博士</span>
              <span class="email"><a href="mailto:midoly1127@gmail.com" class="email" target="_blank" title="信箱">midoly1127@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/teacher-06.jpg">
              <h3>謝君讚</h3>
              <span class="titles">亞東技術學院通識教育中心助理教授</span>
              <span class="educational">中央大學中國文學系博士</span>
              <span class="email"><a href="mailto:puppet7038@yahoo.com.tw" class="email" target="_blank" title="信箱">puppet7038@yahoo.com.tw</a></span>
            </li>
            <li>
              <img src="images/team/teacher-07.jpg">
              <h3>蕭涵珍</h3>
              <span class="titles">亞東技術學院通識教育中心助理教授</span>
              <span class="educational">日本東京大學人文社會系研究科博士</span>
              <span class="email"><a href="mailto:jeanneshiau@gmail.com" class="email" target="_blank" title="信箱">jeanneshiau@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/teacher-08.jpg">
              <h3>張又勻</h3>
              <span class="titles">亞東技術學院通識教育中心講師</span>
              <span class="educational">美國德州大學教育研究所碩士班</span>
              <span class="email"><a href="mailto:Pb108@mail.oit.edu.tw" class="email" target="_blank" title="信箱">Pb108@mail.oit.edu.tw</a></span>
            </li>
          </ul>
        </section>
        <section class="team-ta">
          <h2><span>教學/網頁/行政助理群</span></h2>
          <ul>
            <li>
              <img src="images/team/ta-07.jpg">
              <h3>陳姿穎</h3>
              <span class="titles">行政助理</span>
              <span class="educational">亞東技術學院通識教育中心</span>
              <span class="email"><a href="mailto:ot092@mail.oit.edu.tw" class="email" target="_blank" title="信箱">ot092@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/ta-01.jpg">
              <h3>黃國華</h3>
              <span class="titles">教學助理</span>
              <span class="educational">臺灣大學中國文學系碩士班</span>
              <span class="email"><a href="mailto:r04121004@ntu.edu.tw" class="email" target="_blank" title="信箱">r04121004@ntu.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/ta-02.jpg">
              <h3>楊平軒</h3>
              <span class="titles">教學助理</span>
              <span class="educational">臺灣大學台灣文學研究所碩士班</span>
              <span class="email"><a href="mailto:ccok12345678@yahoo.com.tw" class="email" target="_blank" title="信箱">ccok12345678@yahoo.com.tw</a></span>
            </li>
            <li>
              <img src="images/team/ta-03.jpg">
              <h3>林昱瑄</h3>
              <span class="titles">教學助理</span>
              <span class="educational">政治大學中國文學系碩士班</span>
              <span class="email"><a href="mailto:103151025@nccu.edu.tw" class="email" target="_blank" title="信箱">103151025@nccu.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/ta-04.jpg">
              <h3>黃佳雯</h3>
              <span class="titles">教學助理</span>
              <span class="educational">政治大學中國文學系碩士班</span>
              <span class="email"><a href="mailto:imagine5.tw@yahoo.com.tw" class="email" target="_blank" title="信箱">imagine5.tw@yahoo.com.tw</a></span>
            </li>
            <li>
              <img src="images/team/ta-05.jpg">
              <h3>柯奕吉</h3>
              <span class="titles">網頁助理</span>
              <span class="educational">亞東技術學院資訊管理系</span>
              <span class="email"><a href="mailto:jimmy010679@gmail.com" class="email" target="_blank" title="信箱">jimmy010679@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/ta-06.jpg">
              <h3>陳忠平</h3>
              <span class="titles">網頁助理</span>
              <span class="educational">亞東技術學院資訊管理系</span>
              <span class="email"><a href="mailto:surisuz0000@gmail.com" class="email" target="_blank" title="信箱">surisuz0000@gmail.com</a></span>
            </li>
          </ul>
        </section>
        <!-- ***************************************************** 分隔* **************************************************** -->
        <?php } else if ( $semester === "1042") { ?>
        <section class="team-teacher">
          <h2><span>計畫教師群</span></h2>
          <ul>
            <li>
              <img src="images/team/teacher-01.jpg">
              <h3>林智莉</h3>
              <span class="titles">亞東技術學院通識教育中心副教授兼中心主任</span>
              <span class="educational">政治大學中國文學系博士</span>
              <span class="email"><a href="mailto:fb055@mail.oit.edu.tw" class="email" target="_blank" title="信箱">fb055@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/teacher-02.jpg">
              <h3>吳憶蘭</h3>
              <span class="titles">亞東技術學院通識教育中心副教授</span>
              <span class="educational">文化大學中國文學系博士</span>
              <span class="email"><a href="mailto:fb026@mail.oit.edu.tw" class="email" target="_blank" title="信箱">fb026@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/teacher-03.jpg">
              <h3>傅孝維</h3>
              <span class="titles">亞東技術學院通識教育中心副教授</span>
              <span class="educational">University of Hull心理諮商學系博士</span>
              <span class="email"><a href="mailto:fb045@mail.oit.edu.tw" class="email" target="_blank" title="信箱">fb045@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/teacher-04.jpg">
              <h3>黃啟峰</h3>
              <span class="titles">亞東技術學院通識教育中心助理教授</span>
              <span class="educational">中央大學中國文學系博士</span>
              <span class="email"><a href="mailto:chifeng@mail.oit.edu.tw" class="email" target="_blank" title="信箱">chifeng@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/teacher-05.jpg">
              <h3>李慧琪</h3>
              <span class="titles">亞東技術學院通識教育中心助理教授</span>
              <span class="educational">中央大學中國文學系博士</span>
              <span class="email"><a href="mailto:midoly1127@gmail.com" class="email" target="_blank" title="信箱">midoly1127@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/teacher-06.jpg">
              <h3>謝君讚</h3>
              <span class="titles">亞東技術學院通識教育中心助理教授</span>
              <span class="educational">中央大學中國文學系博士</span>
              <span class="email"><a href="mailto:puppet7038@yahoo.com.tw" class="email" target="_blank" title="信箱">puppet7038@yahoo.com.tw</a></span>
            </li>
            <li>
              <img src="images/team/teacher-09.jpg">
              <h3>楊雅筑</h3>
              <span class="titles">亞東技術學院通識教育中心助理教授</span>
              <span class="educational">國立臺灣師範大學國文所博士班</span>
              <span class="email"><a href="mailto:may20001025@gmail.com" class="email" target="_blank" title="信箱">may20001025@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/teacher-08.jpg">
              <h3>張又勻</h3>
              <span class="titles">亞東技術學院通識教育中心講師</span>
              <span class="educational">美國德州大學教育研究所碩士班</span>
              <span class="email"><a href="mailto:Pb108@mail.oit.edu.tw" class="email" target="_blank" title="信箱">Pb108@mail.oit.edu.tw</a></span>
            </li>
          </ul>
        </section>
        <section class="team-ta">
          <h2><span>教學/網頁助理群</span></h2>
          <ul>
            <li>
              <img src="images/team/ta-07.jpg">
              <h3>陳姿穎</h3>
              <span class="titles">行政助理</span>
              <span class="educational">亞東技術學院通識教育中心中心助理</span>
              <span class="email"><a href="mailto:ot092@mail.oit.edu.tw" class="email" target="_blank" title="信箱">ot092@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/ta-08.jpg">
              <h3>曹育愷</h3>
              <span class="titles">教學助理</span>
              <span class="educational">政治大學中國文學系碩士班</span>
              <span class="email"><a href="mailto:104151007@nccu.edu.tw" class="email" target="_blank" title="信箱">104151007@nccu.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/ta-09.jpg">
              <h3>黃榆諭</h3>
              <span class="titles">教學助理</span>
              <span class="educational">政治大學中國文學系碩士班</span>
              <span class="email"><a href="mailto:asd3632838zxcv@gmail.com" class="email" target="_blank" title="信箱">asd3632838zxcv@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/ta-10.jpg">
              <h3>趙正媛</h3>
              <span class="titles">教學助理</span>
              <span class="educational">臺灣大學台灣文學研究所碩士班</span>
              <span class="email"><a href="mailto:nobidaku@gmail.com" class="email" target="_blank" title="信箱">nobidaku@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/ta-11.jpg">
              <h3>洪梅馨</h3>
              <span class="titles">教學助理</span>
              <span class="educational">輔仁大學中國文學系博士班</span>
              <span class="email"><a href="mailto:sunpenlouis@gmail.com" class="email" target="_blank" title="信箱">sunpenlouis@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/ta-05.jpg">
              <h3>柯奕吉</h3>
              <span class="titles">網頁助理</span>
              <span class="educational">亞東技術學院資訊管理系</span>
              <span class="email"><a href="mailto:jimmy010679@gmail.com" class="email" target="_blank" title="信箱">jimmy010679@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/ta-12.jpg">
              <h3>陳柏安</h3>
              <span class="titles">網頁助理</span>
              <span class="educational">亞東技術學院資訊管理系</span>
              <span class="email"><a href="mailto:chenpoanandrew@gmail.com" class="email" target="_blank" title="信箱">chenpoanandrew@gmail.com</a></span>
            </li>
          </ul>
        </section>
        <!-- ***************************************************** 分隔* **************************************************** -->
        <?php } else if ( $semester === "1051") { ?>
        <section class="team-teacher">
          <h2><span>計畫教師群</span></h2>
          <ul>
            <li>
              <img src="images/team/teacher-01.jpg">
              <h3>林智莉</h3>
              <span class="titles">亞東技術學院通識教育中心副教授</span>
              <span class="educational">政治大學中國文學系博士</span>
              <span class="email"><a href="mailto:fb055@mail.oit.edu.tw" class="email" target="_blank" title="信箱">fb055@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/teacher-02.jpg">
              <h3>吳憶蘭</h3>
              <span class="titles">亞東技術學院通識教育中心副教授</span>
              <span class="educational">文化大學中國文學系博士</span>
              <span class="email"><a href="mailto:fb026@mail.oit.edu.tw" class="email" target="_blank" title="信箱">fb026@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/teacher-03.jpg">
              <h3>傅孝維</h3>
              <span class="titles">亞東技術學院通識教育中心副教授</span>
              <span class="educational">University of Hull心理諮商學系博士</span>
              <span class="email"><a href="mailto:fb045@mail.oit.edu.tw" class="email" target="_blank" title="信箱">fb045@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/teacher-04.jpg">
              <h3>黃啟峰</h3>
              <span class="titles">亞東技術學院通識教育中心助理教授</span>
              <span class="educational">中央大學中國文學系博士</span>
              <span class="email"><a href="mailto:chifeng@mail.oit.edu.tw" class="email" target="_blank" title="信箱">chifeng@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/teacher-06.jpg">
              <h3>謝君讚</h3>
              <span class="titles">亞東技術學院通識教育中心助理教授</span>
              <span class="educational">中央大學中國文學系博士</span>
              <span class="email"><a href="mailto:puppet7038@yahoo.com.tw" class="email" target="_blank" title="信箱">puppet7038@yahoo.com.tw</a></span>
            </li>
            <li>
              <img src="images/team/teacher-09.jpg">
              <h3>楊雅筑</h3>
              <span class="titles">亞東技術學院通識教育中心助理教授</span>
              <span class="educational">國立臺灣師範大學國文所博士班</span>
              <span class="email"><a href="mailto:may20001025@gmail.com" class="email" target="_blank" title="信箱">may20001025@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/teacher-10.jpg">
              <h3>許聖和</h3>
              <span class="titles">亞東技術學院通識教育中心助理教授</span>
              <span class="educational">政治大學中國文學系博士</span>
              <span class="email"><a href="mailto:hsusean17@gmail.com" class="email" target="_blank" title="信箱">hsusean17@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/teacher-08.jpg">
              <h3>張又勻</h3>
              <span class="titles">亞東技術學院通識教育中心講師</span>
              <span class="educational">美國德州大學教育研究所碩士班</span>
              <span class="email"><a href="mailto:Pb108@mail.oit.edu.tw" class="email" target="_blank" title="信箱">Pb108@mail.oit.edu.tw</a></span>
            </li>
          </ul>
        </section>
        <section class="team-ta">
          <h2><span>教學/網頁助理群</span></h2>
          <ul>
            <li>
              <img src="images/team/ta-07.jpg">
              <h3>陳姿穎</h3>
              <span class="titles">行政助理</span>
              <span class="educational">亞東技術學院通識教育中心中心助理</span>
              <span class="email"><a href="mailto:ot092@mail.oit.edu.tw" class="email" target="_blank" title="信箱">ot092@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/ta-11.jpg">
              <h3>洪梅馨</h3>
              <span class="titles">教學助理</span>
              <span class="educational">輔仁大學中國文學系博士班</span>
              <span class="email"><a href="mailto:sunpenlouis@gmail.com" class="email" target="_blank" title="信箱">sunpenlouis@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/ta-14.jpg">
              <h3>吳佩璇</h3>
              <span class="titles">教學助理</span>
              <span class="educational">亞東技術學院醫務管理系</span>
              <span class="email"><a href="mailto:battywu@gmail.com" class="email" target="_blank" title="信箱">battywu@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/ta-15.jpg">
              <h3>游巧歆</h3>
              <span class="titles">教學助理</span>
              <span class="educational">亞東技術學院醫務管理系</span>
              <span class="email"><a href="mailto:hi4u2hss13@gmail.com" class="email" target="_blank" title="信箱">hi4u2hss13@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/ta-16.jpg">
              <h3>林建宇</h3>
              <span class="titles">教學助理</span>
              <span class="educational">亞東技術學院資訊管理系</span>
              <span class="email"><a href="mailto:qwas860124@yahoo.com.tw" class="email" target="_blank" title="信箱">qwas860124@yahoo.com.tw</a></span>
            </li>
            <li>
              <img src="images/team/ta-17.jpg">
              <h3>卜政嘉</h3>
              <span class="titles">教學助理</span>
              <span class="educational">亞東技術學院資訊管理系</span>
              <span class="email"><a href="mailto:miunna0320@gmail.com" class="email" target="_blank" title="信箱">miunna0320@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/ta-18.jpg">
              <h3>丁涵茹</h3>
              <span class="titles">教學助理</span>
              <span class="educational">亞東技術學院工商業設計系</span>
              <span class="email"><a href="mailto:cat45603@gmail.com" class="email" target="_blank" title="信箱">cat45603@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/ta-05.jpg">
              <h3>柯奕吉</h3>
              <span class="titles">網頁助理</span>
              <span class="educational">亞東技術學院資訊管理系</span>
              <span class="email"><a href="mailto:jimmy010679@gmail.com" class="email" target="_blank" title="信箱">jimmy010679@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/ta-13.jpg">
              <h3>呂亞庭</h3>
              <span class="titles">網頁助理</span>
              <span class="educational">亞東技術學院資訊管理系</span>
              <span class="email"><a href="mailto:twothree416@gmail.com" class="email" target="_blank" title="信箱">twothree416@gmail.com</a></span>
            </li>
          </ul>
        </section>
        <!-- ***************************************************** 分隔* **************************************************** -->
        <?php } else if ( $semester === "1052") { ?>
        <section class="team-teacher">
          <h2><span>計畫教師群</span></h2>
          <ul>
            <li>
              <img src="images/team/teacher-01.jpg">
              <h3>林智莉</h3>
              <span class="titles">亞東技術學院通識教育中心副教授</span>
              <span class="educational">政治大學中國文學系博士</span>
              <span class="email"><a href="mailto:fb055@mail.oit.edu.tw" class="email" target="_blank" title="信箱">fb055@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/teacher-02.jpg">
              <h3>吳憶蘭</h3>
              <span class="titles">亞東技術學院通識教育中心副教授</span>
              <span class="educational">文化大學中國文學系博士</span>
              <span class="email"><a href="mailto:fb026@mail.oit.edu.tw" class="email" target="_blank" title="信箱">fb026@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/teacher-03.jpg">
              <h3>傅孝維</h3>
              <span class="titles">亞東技術學院通識教育中心副教授</span>
              <span class="educational">University of Hull心理諮商學系博士</span>
              <span class="email"><a href="mailto:fb045@mail.oit.edu.tw" class="email" target="_blank" title="信箱">fb045@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/teacher-04.jpg">
              <h3>黃啟峰</h3>
              <span class="titles">亞東技術學院通識教育中心助理教授</span>
              <span class="educational">中央大學中國文學系博士</span>
              <span class="email"><a href="mailto:chifeng@mail.oit.edu.tw" class="email" target="_blank" title="信箱">chifeng@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/teacher-06.jpg">
              <h3>謝君讚</h3>
              <span class="titles">亞東技術學院通識教育中心助理教授</span>
              <span class="educational">中央大學中國文學系博士</span>
              <span class="email"><a href="mailto:puppet7038@yahoo.com.tw" class="email" target="_blank" title="信箱">puppet7038@yahoo.com.tw</a></span>
            </li>
            <li>
              <img src="images/team/teacher-09.jpg">
              <h3>楊雅筑</h3>
              <span class="titles">亞東技術學院通識教育中心助理教授</span>
              <span class="educational">國立臺灣師範大學國文所博士班</span>
              <span class="email"><a href="mailto:may20001025@gmail.com" class="email" target="_blank" title="信箱">may20001025@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/teacher-11.jpg">
              <h3>徐其寧</h3>
              <span class="titles">亞東技術學院通識教育中心助理教授</span>
              <span class="educational">清華大學中國文學系博士班</span>
              <span class="email"><a href="mailto:hsu.chining@gmail.com" class="email" target="_blank" title="信箱">hsu.chining@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/teacher-08.jpg">
              <h3>張又勻</h3>
              <span class="titles">亞東技術學院通識教育中心講師</span>
              <span class="educational">美國德州大學教育研究所碩士班</span>
              <span class="email"><a href="mailto:Pb108@mail.oit.edu.tw" class="email" target="_blank" title="信箱">Pb108@mail.oit.edu.tw</a></span>
            </li>
          </ul>
        </section>
        <section class="team-ta">
          <h2><span>教學/網頁助理群</span></h2>
          <ul>
            <li>
              <img src="images/team/ta-07.jpg">
              <h3>陳姿穎</h3>
              <span class="titles">行政助理</span>
              <span class="educational">亞東技術學院通識教育中心中心助理</span>
              <span class="email"><a href="mailto:ot092@mail.oit.edu.tw" class="email" target="_blank" title="信箱">ot092@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/ta-11.jpg">
              <h3>洪梅馨</h3>
              <span class="titles">教學助理</span>
              <span class="educational">輔仁大學中國文學系博士班</span>
              <span class="email"><a href="mailto:sunpenlouis@gmail.com" class="email" target="_blank" title="信箱">sunpenlouis@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/ta-14.jpg">
              <h3>吳佩璇</h3>
              <span class="titles">教學助理</span>
              <span class="educational">亞東技術學院醫務管理系</span>
              <span class="email"><a href="mailto:battywu@gmail.com" class="email" target="_blank" title="信箱">battywu@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/ta-15.jpg">
              <h3>游巧歆</h3>
              <span class="titles">教學助理</span>
              <span class="educational">亞東技術學院醫務管理系</span>
              <span class="email"><a href="mailto:hi4u2hss13@gmail.com" class="email" target="_blank" title="信箱">hi4u2hss13@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/ta-19.jpg">
              <h3>陳雅伶</h3>
              <span class="titles">教學助理</span>
              <span class="educational">亞東技術學院工商業設計系</span>
              <span class="email"><a href="mailto:104107245@mail.oit.edu.tw" class="email" target="_blank" title="信箱">104107245@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/ta-20.jpg">
              <h3>羅亦</h3>
              <span class="titles">教學助理</span>
              <span class="educational">亞東技術學院工業管理系</span>
              <span class="email"><a href="mailto:kot850609@gmail.com" class="email" target="_blank" title="信箱">kot850609@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/ta-21.jpg">
              <h3>凃諭伶</h3>
              <span class="titles">教學助理</span>
              <span class="educational">亞東技術學院工商業設計系</span>
              <span class="email"><a href="mailto:kise04gata@gmail.com" class="email" target="_blank" title="信箱">kise04gata@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/ta-05.jpg">
              <h3>柯奕吉</h3>
              <span class="titles">網頁助理</span>
              <span class="educational">亞東技術學院資訊管理系</span>
              <span class="email"><a href="mailto:jimmy010679@gmail.com" class="email" target="_blank" title="信箱">jimmy010679@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/ta-13.jpg">
              <h3>呂亞庭</h3>
              <span class="titles">網頁助理</span>
              <span class="educational">亞東技術學院資訊管理系</span>
              <span class="email"><a href="mailto:twothree416@gmail.com" class="email" target="_blank" title="信箱">twothree416@gmail.com</a></span>
            </li>
          </ul>
        </section>
        <!-- ***************************************************** 分隔* **************************************************** -->
        <?php } else if ( $semester === "1061") { ?>
        <section class="team-teacher">
          <h2><span>計畫教師群</span></h2>
          <ul>
            <li>
              <img src="images/team/teacher-01.jpg">
              <h3>林智莉</h3>
              <span class="titles">亞東技術學院通識教育中心副教授</span>
              <span class="educational">政治大學中國文學系博士</span>
              <span class="email"><a href="mailto:fb055@mail.oit.edu.tw" class="email" target="_blank" title="信箱">fb055@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/teacher-02.jpg">
              <h3>吳憶蘭</h3>
              <span class="titles">亞東技術學院通識教育中心副教授</span>
              <span class="educational">文化大學中國文學系博士</span>
              <span class="email"><a href="mailto:fb026@mail.oit.edu.tw" class="email" target="_blank" title="信箱">fb026@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/teacher-03.jpg">
              <h3>傅孝維</h3>
              <span class="titles">亞東技術學院通識教育中心副教授</span>
              <span class="educational">University of Hull心理諮商學系博士</span>
              <span class="email"><a href="mailto:fb045@mail.oit.edu.tw" class="email" target="_blank" title="信箱">fb045@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/teacher-04.jpg">
              <h3>黃啟峰</h3>
              <span class="titles">亞東技術學院通識教育中心助理教授</span>
              <span class="educational">中央大學中國文學系博士</span>
              <span class="email"><a href="mailto:chifeng@mail.oit.edu.tw" class="email" target="_blank" title="信箱">chifeng@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/teacher-06.jpg">
              <h3>謝君讚</h3>
              <span class="titles">亞東技術學院通識教育中心助理教授</span>
              <span class="educational">中央大學中國文學系博士</span>
              <span class="email"><a href="mailto:puppet7038@yahoo.com.tw" class="email" target="_blank" title="信箱">puppet7038@yahoo.com.tw</a></span>
            </li>
            <li>
              <img src="images/team/teacher-09.jpg">
              <h3>楊雅筑</h3>
              <span class="titles">亞東技術學院通識教育中心助理教授</span>
              <span class="educational">國立臺灣師範大學國文所博士班</span>
              <span class="email"><a href="mailto:may20001025@gmail.com" class="email" target="_blank" title="信箱">may20001025@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/teacher-11.jpg">
              <h3>徐其寧</h3>
              <span class="titles">亞東技術學院通識教育中心助理教授</span>
              <span class="educational">清華大學中國文學系博士班</span>
              <span class="email"><a href="mailto:hsu.chining@gmail.com" class="email" target="_blank" title="信箱">hsu.chining@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/teacher-08.jpg">
              <h3>張又勻</h3>
              <span class="titles">亞東技術學院通識教育中心講師</span>
              <span class="educational">美國德州大學教育研究所碩士班</span>
              <span class="email"><a href="mailto:Pb108@mail.oit.edu.tw" class="email" target="_blank" title="信箱">Pb108@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/teacher-05.jpg">
              <h3>李慧琪</h3>
              <span class="titles">亞東技術學院通識教育中心助理教授</span>
              <span class="educational">中央大學中國文學系博士</span>
              <span class="email"><a href="mailto:midoly1127@gmail.com" class="email" target="_blank" title="信箱">midoly1127@gmail.com</a></span>
            </li>
          </ul>
        </section>
        <section class="team-ta">
          <h2><span>教學/網頁助理群</span></h2>
          <ul>
            <li>
              <img src="images/team/ta-07.jpg">
              <h3>陳姿穎</h3>
              <span class="titles">行政助理</span>
              <span class="educational">亞東技術學院通識教育中心中心助理</span>
              <span class="email"><a href="mailto:ot092@mail.oit.edu.tw" class="email" target="_blank" title="信箱">ot092@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/ta-31.jpg">
              <h3>吳佳樺</h3>
              <span class="titles">教學助理</span>
              <span class="educational">亞東技術學院資訊管理系</span>
              <span class="email"><a href="mailto:jk60927@gmail.com" class="email" target="_blank" title="信箱">jk60927@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/ta-32.jpg">
              <h3>洪鈞頎</h3>
              <span class="titles">教學助理</span>
              <span class="educational">亞東技術學院工商業設計系</span>
              <span class="email"><a href="mailto:hung104107241@gmail.com" class="email" target="_blank" title="信箱">hung104107241@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/ta-33.jpg">
              <h3>張宸禎</h3>
              <span class="titles">教學助理</span>
              <span class="educational">亞東技術學院醫務管理系</span>
              <span class="email"><a href="mailto:matcha0526@gmail.com" class="email" target="_blank" title="信箱">matcha0526@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/ta-34.jpg">
              <h3>劉姿敏</h3>
              <span class="titles">教學助理</span>
              <span class="educational">亞東技術學院工商業設計系</span>
              <span class="email"><a href="mailto:h73528532@gmail.com" class="email" target="_blank" title="信箱">h73528532@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/ta-35.jpg">
              <h3>江蒂容</h3>
              <span class="titles">教學助理</span>
              <span class="educational">亞東技術學院工商業設計系</span>
              <span class="email"><a href="mailto:aaa22250247@gamil.com" class="email" target="_blank" title="信箱">aaa22250247@gamil.com</a></span>
            </li>
            <li>
              <img src="images/team/ta-20.jpg">
              <h3>羅亦</h3>
              <span class="titles">教學助理</span>
              <span class="educational">亞東技術學院工業管理系</span>
              <span class="email"><a href="mailto:kot850609@gmail.com" class="email" target="_blank" title="信箱">kot850609@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/ta-15.jpg">
              <h3>游巧歆</h3>
              <span class="titles">網頁助理</span>
              <span class="educational">亞東技術學院醫務管理系</span>
              <span class="email"><a href="mailto:hi4u2hss13@gmail.com" class="email" target="_blank" title="信箱">hi4u2hss13@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/ta-36.jpg">
              <h3>董奕岑</h3>
              <span class="titles">網頁助理</span>
              <span class="educational">亞東技術學院資訊管理系</span>
              <span class="email"><a href="mailto:z22019494z@gamil.com" class="email" target="_blank" title="信箱">z22019494z@gamil.com</a></span>
            </li>
          </ul>
        </section>
        <!-- ***************************************************** 分隔* **************************************************** -->
        <?php } else if ( $semester === "1062") { ?>
        <section class="team-teacher">
          <h2><span>計畫教師群</span></h2>
          <ul>
            <li>
              <img src="images/team/teacher-01.jpg">
              <h3>林智莉</h3>
              <span class="titles">亞東技術學院通識教育中心副教授</span>
              <span class="educational">政治大學中國文學系博士</span>
              <span class="email"><a href="mailto:fb055@mail.oit.edu.tw" class="email" target="_blank" title="信箱">fb055@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/teacher-02.jpg">
              <h3>吳憶蘭</h3>
              <span class="titles">亞東技術學院通識教育中心副教授</span>
              <span class="educational">文化大學中國文學系博士</span>
              <span class="email"><a href="mailto:fb026@mail.oit.edu.tw" class="email" target="_blank" title="信箱">fb026@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/teacher-03.jpg">
              <h3>傅孝維</h3>
              <span class="titles">亞東技術學院通識教育中心副教授</span>
              <span class="educational">University of Hull心理諮商學系博士</span>
              <span class="email"><a href="mailto:fb045@mail.oit.edu.tw" class="email" target="_blank" title="信箱">fb045@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/teacher-04.jpg">
              <h3>黃啟峰</h3>
              <span class="titles">亞東技術學院通識教育中心助理教授</span>
              <span class="educational">中央大學中國文學系博士</span>
              <span class="email"><a href="mailto:chifeng@mail.oit.edu.tw" class="email" target="_blank" title="信箱">chifeng@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/teacher-06.jpg">
              <h3>謝君讚</h3>
              <span class="titles">亞東技術學院通識教育中心助理教授</span>
              <span class="educational">中央大學中國文學系博士</span>
              <span class="email"><a href="mailto:puppet7038@yahoo.com.tw" class="email" target="_blank" title="信箱">puppet7038@yahoo.com.tw</a></span>
            </li>
            <li>
              <img src="images/team/teacher-09.jpg">
              <h3>楊雅筑</h3>
              <span class="titles">亞東技術學院通識教育中心助理教授</span>
              <span class="educational">國立臺灣師範大學國文所博士班</span>
              <span class="email"><a href="mailto:may20001025@gmail.com" class="email" target="_blank" title="信箱">may20001025@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/teacher-11.jpg">
              <h3>徐其寧</h3>
              <span class="titles">亞東技術學院通識教育中心助理教授</span>
              <span class="educational">清華大學中國文學系博士班</span>
              <span class="email"><a href="mailto:hsu.chining@gmail.com" class="email" target="_blank" title="信箱">hsu.chining@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/teacher-08.jpg">
              <h3>張又勻</h3>
              <span class="titles">亞東技術學院通識教育中心講師</span>
              <span class="educational">美國德州大學教育研究所碩士班</span>
              <span class="email"><a href="mailto:Pb108@mail.oit.edu.tw" class="email" target="_blank" title="信箱">Pb108@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/teacher-05.jpg">
              <h3>李慧琪</h3>
              <span class="titles">亞東技術學院通識教育中心助理教授</span>
              <span class="educational">中央大學中國文學系博士</span>
              <span class="email"><a href="mailto:midoly1127@gmail.com" class="email" target="_blank" title="信箱">midoly1127@gmail.com</a></span>
            </li>
          </ul>
        </section>
        <section class="team-ta">
          <h2><span>教學/網頁助理群</span></h2>
          <ul>
            <li>
              <img src="images/team/ta-07.jpg">
              <h3>陳姿穎</h3>
              <span class="titles">行政助理</span>
              <span class="educational">亞東技術學院通識教育中心中心助理</span>
              <span class="email"><a href="mailto:ot092@mail.oit.edu.tw" class="email" target="_blank" title="信箱">ot092@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/ta-37.jpg">
              <h3>林亞萱</h3>
              <span class="titles">教學助理</span>
              <span class="educational">亞東技術學院材料與纖維系</span>
              <span class="email"><a href="mailto:ab851105@gmail.com" class="email" target="_blank" title="信箱">ab851105@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/ta-38.jpg">
              <h3>吳岱樺</h3>
              <span class="titles">教學助理</span>
              <span class="educational">亞東技術學院醫務管理系</span>
              <span class="email"><a href="mailto:j6294cj86@gmail.com" class="email" target="_blank" title="信箱">j6294cj86@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/ta-39-1.jpg">
              <h3>劉威婷</h3>
              <span class="titles">教學助理</span>
              <span class="educational">亞東技術學院醫務管理系</span>
              <span class="email"><a href="mailto:105110127@mail.oit.edu.tw" class="email" target="_blank" title="信箱">105110127@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/ta-40-1.jpg">
              <h3>彭彥菱</h3>
              <span class="titles">教學助理</span>
              <span class="educational">亞東技術學院材料與纖維系</span>
              <span class="email"><a href="mailto:hsiftae00173@gmail.com" class="email" target="_blank" title="信箱">hsiftae00173@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/ta-41-1.jpg">
              <h3>黃孟澤</h3>
              <span class="titles">教學助理</span>
              <span class="educational">亞東技術學院通訊工程系</span>
              <span class="email"><a href="mailto:max88591@gmail.com" class="email" target="_blank" title="信箱">max88591@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/ta-42-1.jpg">
              <h3>姚妤臻</h3>
              <span class="titles">教學助理</span>
              <span class="educational">亞東技術學院工商業設計系</span>
              <span class="email"><a href="mailto:kimmy0430@kimo.com" class="email" target="_blank" title="信箱">kimmy0430@kimo.com</a></span>
            </li>
            <li>
              <img src="images/team/ta-43.jpg">
              <h3>吳佩璇</h3>
              <span class="titles">網頁助理</span>
              <span class="educational">亞東技術學院醫務管理系</span>
              <span class="email"><a href="mailto:battywu@gmail.com" class="email" target="_blank" title="信箱">battywu@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/ta-44.jpg">
              <h3>游巧歆</h3>
              <span class="titles">網頁助理</span>
              <span class="educational">亞東技術學院醫務管理系</span>
              <span class="email"><a href="mailto:hi4u2hss13@gmail.com" class="email" target="_blank" title="信箱">hi4u2hss13@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/ta-36.jpg">
              <h3>董奕岑</h3>
              <span class="titles">網頁助理</span>
              <span class="educational">亞東技術學院資訊管理系</span>
              <span class="email"><a href="mailto:z22019494z@gamil.com" class="email" target="_blank" title="信箱">z22019494z@gamil.com</a></span>
            </li>
          </ul>
        </section>
        <!-- ***************************************************** 分隔* **************************************************** -->
        <?php } else if ( $semester === "1071") { ?>
        <section class="team-teacher">
          <h2><span>計畫教師群</span></h2>
          <ul>
            <li>
              <img src="images/team/teacher-01.jpg">
              <h3>林智莉</h3>
              <span class="titles">亞東技術學院通識教育中心副教授</span>
              <span class="educational">政治大學中國文學系博士</span>
              <span class="email"><a href="mailto:fb055@mail.oit.edu.tw" class="email" target="_blank" title="信箱">fb055@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/teacher-02.jpg">
              <h3>吳憶蘭</h3>
              <span class="titles">亞東技術學院通識教育中心副教授</span>
              <span class="educational">文化大學中國文學系博士</span>
              <span class="email"><a href="mailto:fb026@mail.oit.edu.tw" class="email" target="_blank" title="信箱">fb026@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/teacher-03.jpg">
              <h3>傅孝維</h3>
              <span class="titles">亞東技術學院通識教育中心副教授</span>
              <span class="educational">University of Hull心理諮商學系博士</span>
              <span class="email"><a href="mailto:fb045@mail.oit.edu.tw" class="email" target="_blank" title="信箱">fb045@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/teacher-04.jpg">
              <h3>黃啟峰</h3>
              <span class="titles">亞東技術學院通識教育中心助理教授</span>
              <span class="educational">中央大學中國文學系博士</span>
              <span class="email"><a href="mailto:chifeng@mail.oit.edu.tw" class="email" target="_blank" title="信箱">chifeng@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/teacher-06.jpg">
              <h3>謝君讚</h3>
              <span class="titles">亞東技術學院通識教育中心助理教授</span>
              <span class="educational">中央大學中國文學系博士</span>
              <span class="email"><a href="mailto:puppet7038@yahoo.com.tw" class="email" target="_blank" title="信箱">puppet7038@yahoo.com.tw</a></span>
            </li>
            <li>
              <img src="images/team/teacher-09.jpg">
              <h3>楊雅筑</h3>
              <span class="titles">亞東技術學院通識教育中心助理教授</span>
              <span class="educational">國立臺灣師範大學國文所博士班</span>
              <span class="email"><a href="mailto:may20001025@gmail.com" class="email" target="_blank" title="信箱">may20001025@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/teacher-11.jpg">
              <h3>徐其寧</h3>
              <span class="titles">亞東技術學院通識教育中心助理教授</span>
              <span class="educational">清華大學中國文學系博士班</span>
              <span class="email"><a href="mailto:hsu.chining@gmail.com" class="email" target="_blank" title="信箱">hsu.chining@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/teacher-08.jpg">
              <h3>張又勻</h3>
              <span class="titles">亞東技術學院通識教育中心講師</span>
              <span class="educational">美國德州大學教育研究所碩士班</span>
              <span class="email"><a href="mailto:Pb108@mail.oit.edu.tw" class="email" target="_blank" title="信箱">Pb108@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/teacher-05.jpg">
              <h3>李慧琪</h3>
              <span class="titles">亞東技術學院通識教育中心助理教授</span>
              <span class="educational">中央大學中國文學系博士</span>
              <span class="email"><a href="mailto:midoly1127@gmail.com" class="email" target="_blank" title="信箱">midoly1127@gmail.com</a></span>
            </li>
          </ul>
        </section>
        <section class="team-ta">
          <h2><span>教學/網頁助理群</span></h2>
          <ul>
          <li>
              <img src="images/team/ta-07.jpg">
              <h3>陳姿穎</h3>
              <span class="titles">行政助理</span>
              <span class="educational">亞東技術學院通識教育中心中心助理</span>
              <span class="email"><a href="mailto:ot092@mail.oit.edu.tw" class="email" target="_blank" title="信箱">ot092@mail.oit.edu.tw</a></span>
            </li>
            <li>
              <img src="images/team/ta-43.jpg">
              <h3>吳佩璇</h3>
              <span class="titles">教學助理</span>
              <span class="educational">亞東技術學院醫務管理系</span>
              <span class="email"><a href="mailto:battywu@gmail.com" class="email" target="_blank" title="信箱">battywu@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/ta-44.jpg">
              <h3>游巧歆</h3>
              <span class="titles">教學助理</span>
              <span class="educational">亞東技術學院醫務管理系</span>
              <span class="email"><a href="mailto:hi4u2hss13@gmail.com" class="email" target="_blank" title="信箱">hi4u2hss13@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/ta-49.jpg" style="width:190px;height:190px">
              <h3>詹朝翔</h3>
              <span class="titles">教學助理</span>
              <span class="educational">亞東技術學院材料纖維系</span>
              <span class="email"><a href="mailto:jerry6041333@gmail.com" class="email" target="_blank" title="信箱">jerry6041333@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/ta-48.jpg">
              <h3>洪鈞頎</h3>
              <span class="titles">教學助理</span>
              <span class="educational">亞東技術學院工商業設計系</span>
              <span class="email"><a href="mailto:hung104107241@gmail.com" class="email" target="_blank" title="信箱">hung104107241@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/ta-47.jpg"  style="width:190px;height:190px">
              <h3>謝旻潔</h3>
              <span class="titles">教學助理</span>
              <span class="educational">亞東技術學院工商業設計系</span>
              <span class="email"><a href="mailto:Kissie3@gmail.com" class="email" target="_blank" title="信箱">Kissie3@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/ta-46.jpg"  style="width:190px;height:190px">
              <h3>吳佳樺</h3>
              <span class="titles">教學助理</span>
              <span class="educational">亞東技術學院資訊管理系</span>
              <span class="email"><a href="mailto:jk60927@gmail.com" class="email" target="_blank" title="信箱">jk60927@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/ta-45.jpg" style="width:190px;height:190px">
              <h3>楊傑閎</h3>
              <span class="titles">網頁助理</span>
              <span class="educational">亞東技術學院資訊管理系</span>
              <span class="email"><a href="mailto:qaawssedd456@gmail.com" class="email" target="_blank" title="信箱">qaawssedd456@gmail.com</a></span>
            </li>
            <li>
              <img src="images/team/ta-50.jpg" style="width:190px;height:190px">
              <h3>陳信宏</h3>
              <span class="titles">網頁助理</span>
              <span class="educational">亞東技術學院材纖系</span>
              <span class="email"><a href="mailto:106101132@mail.oit.edu.tw" class="email" target="_blank" title="信箱">qaawssedd456@gmail.com</a></span>
            </li>
          </ul>
        </section>
        <?php } ?>
      </div>
    </div>
  </article>
  <footer id="footer">
    <div class="subfooter">
      <div class="container">
        <div class="row">
          <div class="col-xxs-12 col-md-7">
            <div class="footer-nav">
              <ul>
                <li><a href="index.php">首頁</a></li>
                <li><a href="news.php">最新消息</a></li>
                <li><a href="intro.php">課程計畫</a></li>
                <li><a href="forums.php">討論區</a></li>
                <li><a href="works.php">優秀作品</a></li>
                <li><a href="picture.php">影像紀錄</a></li>
                <li><a href="team.php">核心團隊</a></li>
              </ul>
            </div>
          </div>
          <div class="col-xxs-12 col-md-5">
            <div class="school">
              <p>亞東技術學院 通識教育中心 OIT Center for General Education</p>
            </div>
            <div class="plan">
              <p>文韻亞東 ─ 中文閱讀書寫課程革新計畫</p>
            </div>
            <div class="hss">
              <p>本課程由教育部資訊及科技教育司支持</p>
            </div>
          </div>
        </div>
      </div> <!-- container -->
    </div> <!-- subfooter -->
    <div class="copyright">
      <div class="container">
        <div class="text">
          <small>
            <address class="author">
              Copyright 2015-2016.
            </address>
            <a href="http://gecw.oit.edu.tw/bin/home.php" title="亞東技術學院通識教育中心" target="_blank">亞東技術學院通識教育中心</a> All
            Rights Reserved.
          </small>
        </div>
        <div class="total">
          <?php require_once "./statistics.php"; ?>
        </div>
      </div>
    </div>
  </footer>
  <div id="outdated"></div> <!-- outdatedbrowser 檢查瀏覽器 -->
  <script src="js/style.js"></script>
  <script type="text/javascript">
    document.getElementById("team-select").addEventListener("change", function () {
      document.location.href = "team.php?semester=" + this.value;
    });
  </script>
  <script src="outdatedbrowser/dom-outdatedbrowser.js"></script> <!-- outdatedbrowser 檢查瀏覽器 -->
</body>

</html>