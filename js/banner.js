/*!
  * Banner
  * Jimmy 懶惰87寫法
  * JavaScript
  * 2015
*/

var carousel = document.getElementById("carousel-content");
var carousel_img = carousel.getElementsByTagName("li");
var carousel_icon = document.getElementById("carousel-indicators").getElementsByTagName("li");

var carousel_now = 0;

function CarouselChangeLeft() {
  switch (carousel_now) {
    case 0:
      CarouselCleanLiClass();
      carousel_img[0].className = "left";
      carousel_img[1].className = "active";
      carousel_img[2].className = "right";

      carousel_now = 1;
      carousel_time = 10;

      CarouselCleanIcon();
      carousel_icon[1].className = "active";
      break;
    case 1:
      CarouselCleanLiClass();
      carousel_img[1].className = "left";
      carousel_img[2].className = "active";
      carousel_img[3].className = "right";

      carousel_now = 2;
      carousel_time = 10;

      CarouselCleanIcon();
      carousel_icon[2].className = "active";
      break;
    case 2:
      CarouselCleanLiClass();
      carousel_img[2].className = "left";
      carousel_img[3].className = "active";
      carousel_img[4].className = "right";

      carousel_now = 3;
      carousel_time = 10;

      CarouselCleanIcon();
      carousel_icon[3].className = "active";
      break;
    case 3:
      CarouselCleanLiClass();
      carousel_img[3].className = "left";
      carousel_img[4].className = "active";

      carousel_now = 4;
      carousel_time = 10;

      CarouselCleanIcon();
      carousel_icon[4].className = "active";
      break;
    default:
  }
}
function CarouselChangeRight() {
  switch (carousel_now) {
    case 1:
      CarouselCleanLiClass();
      carousel_img[0].className = "active";
      carousel_img[1].className = "right";

      carousel_now = 0;
      carousel_time = 10;

      CarouselCleanIcon();
      carousel_icon[0].className = "active";
      break;
    case 2:
      CarouselCleanLiClass();
      carousel_img[0].className = "left";
      carousel_img[1].className = "active";
      carousel_img[2].className = "right";

      carousel_now = 1;
      carousel_time = 10;

      CarouselCleanIcon();
      carousel_icon[1].className = "active";
      break;
    case 3:      
      CarouselCleanLiClass();
      carousel_img[1].className = "left";
      carousel_img[2].className = "active";
      carousel_img[3].className = "right";

      carousel_now = 2;
      carousel_time = 10;

      CarouselCleanIcon();
      carousel_icon[2].className = "active";
      break;
    case 4:
      CarouselCleanLiClass();
      carousel_img[2].className = "left";
      carousel_img[3].className = "active";
      carousel_img[4].className = "right";

      carousel_now = 3;
      carousel_time = 10;

      CarouselCleanIcon();
      carousel_icon[3].className = "active";
      break;
    default:
  }
}
function CarouselChangeClick(id) {  
  switch (id) {
  	case "0":
  	  CarouselCleanLiClass();
      carousel_img[0].className = "active";
      carousel_img[1].className = "right";

      carousel_now = 0;
      carousel_time = 10;

      CarouselCleanIcon();
      carousel_icon[0].className = "active";
      break;
    case "1":      
      CarouselCleanLiClass();
      carousel_img[0].className = "left";
      carousel_img[1].className = "active";
      carousel_img[2].className = "right";

      carousel_now = 1;
      carousel_time = 10;

      CarouselCleanIcon();
      carousel_icon[1].className = "active";
      break;
    case "2":
      CarouselCleanLiClass();
      carousel_img[1].className = "left";
      carousel_img[2].className = "active";
      carousel_img[3].className = "right";

      carousel_now = 2;
      carousel_time = 10;

      CarouselCleanIcon();
      carousel_icon[2].className = "active";
      break;
    case "3":      
      CarouselCleanLiClass();
      carousel_img[2].className = "left";
      carousel_img[3].className = "active";
      carousel_img[4].className = "right";

      carousel_now = 3;
      carousel_time = 10;

      CarouselCleanIcon();
      carousel_icon[3].className = "active";
      break;
    case "4":
      CarouselCleanLiClass();
      carousel_img[3].className = "left";
      carousel_img[4].className = "active";

      carousel_now = 4;
      carousel_time = 10;

      CarouselCleanIcon();
      carousel_icon[4].className = "active";
      break;      
    default:
  }	
}


function CarouselCleanLiClass() {
  for ( i=0 ; i <= carousel_img.length - 1 ; i++ ) {
    carousel_img[i].className = "";
  }
}
function CarouselCleanIcon() {
  for ( i=0 ; i <= carousel_icon.length - 1 ; i++ ) {
    carousel_icon[i].className = "";
  }
}

//設定倒數秒數
var carousel_time = 10;
function showTime() {
  if ( carousel_time > 0 ) {
    carousel_time = carousel_time - 1;
  }
  if( carousel_time == 0 ) {
    CarouselChangeLeft();
  }    
  //每秒執行一次,showTime()
  setTimeout("showTime()",1000);
}
showTime();

var carousel_right_btn = document.getElementById("carousel-right-btn");
carousel_right_btn.addEventListener("click", function(){CarouselChangeLeft();}, false);

var carousel_left_btn = document.getElementById("carousel-left-btn");
carousel_left_btn.addEventListener("click", function(){CarouselChangeRight();}, false);


for ( i=0 ; i <= carousel_icon.length - 1 ; i++ ) {
  carousel_icon[i].addEventListener("click", function(){CarouselIconChange(this);}, false);
}
function CarouselIconChange(e) {
  var carousel_icon_id = e.dataset.slideto;
  CarouselChangeClick(carousel_icon_id);
}
