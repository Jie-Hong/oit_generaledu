/*!
 ************************************************************
 * 回應再回應
 ************************************************************
 * JavaScript
 * 2015
 */

var topic_re_li = document.getElementById("comments-list").getElementsByClassName("comment-reply-btn");
for (i = 0; i <= topic_re_li.length - 1; i++) {
  topic_re_li[i].addEventListener("click", function () {
    TopicRe(this);
  }, false);
}

function TopicRe(e) {
  //如果一直修改不成功 清除電腦所有瀏覽紀錄
  var oitclass = '<option value="">------請選擇班級------</option><option value="1071-ha-a">1071 醫管1A</option><option value="1071-ha-b">1071 醫管1B</option><option value="1071-ce-b">1071 通訊1B</option><option value="1071-mi-a">1071 資管1A</option><option value="1071-mi-b">1071 資管1B</option><option value="1071-mt-a">1071 材纖1A</option><option value="1071-mt-b">1071 材纖1B</option><option value="1071-md-a">1071 行銷1A</option><option value="1071-md-b">1071 行銷1B</option>';
  var topic_re_li_id = e.parentElement.parentElement.dataset.reid;
  e.parentElement.getElementsByClassName("topic-comments-formbox")[0].innerHTML = '<form method="post" action=""><div class="topic-respond-info"><span><select class="topic-dropdown" name="class-select" required>' + oitclass + '</select></span><span><input type="text" name="topic-re-studentid" class="topic-re-input" placeholder="學號" maxlength="9" required/></span><span><input type="text" name="topic-re-name" class="topic-re-input" placeholder="姓名" required/></span></div><textarea class="topic-re-text" name="text" rows="4" placeholder="請輸入回應文字，按Enter或Shift+Enter，可增加高度。" required></textarea><input class="topic-re-btn" type="submit" value="回應"><input type="text" hidden name="comment_id"  value="' + topic_re_li_id + '"></form>';

  //套用 autosize 函式 高度
  autosize(e.parentElement.getElementsByClassName("topic-comments-formbox")[0].querySelector('textarea'));
}