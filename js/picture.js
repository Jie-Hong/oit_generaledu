/*!
 ************************************************************
 * Ta share lightbox
 *
 ************************************************************
 * JavaScript
 * 2015
 */
var p_content = document.getElementById('images-content');
var p_img = p_content.getElementsByTagName('img');
var p_lightbox = document.getElementById("p-lightbox");
var p_now = 0;

for ( i=0 ; i <= p_img.length - 1 ; i++ ) {
  p_img[i].addEventListener("click", function(){ImgLightbox(this);}, false);
}

function ImgLightbox(e) {
  p_lightbox.getElementsByTagName("img")[0].src = e.src;
  p_lightbox.classList.toggle('open-p-lightobx');
  p_now = 1;
}

p_lightbox.addEventListener("click", function(){ImgLightboxClose();}, false);
function ImgLightboxClose() {
  if ( p_now == 1 ) {
  	p_lightbox.classList.toggle('open-p-lightobx');
  	p_now = 0;
  }
}