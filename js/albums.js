/*!
  * albums Lightbox
  * JavaScript
  * 2015
*/
var albums = document.getElementById("albums");
var albums_photo_img = albums.getElementsByTagName("img");

var albums_lightbox = document.getElementById("albums-lightbox");
var albums_now = 0;

for ( i=0 ; i <= albums_photo_img.length - 1 ; i++ ) {
  albums_photo_img[i].addEventListener("click", function(){AlbumsLightbox(this);}, false);
}
function AlbumsLightbox(e) {
  albums_lightbox.getElementsByTagName("img")[0].src = e.src;
  albums_lightbox.classList.toggle('open-albums-lightobx');
  albums_now = 1;
}

albums_lightbox.addEventListener("click", function(){AlbumsLightboxClose();}, false);
function AlbumsLightboxClose() {
  if ( albums_now == 1 ) {
  	albums_lightbox.classList.toggle('open-albums-lightobx');
  	albums_now = 0;
  }
}