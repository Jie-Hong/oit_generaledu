/*!
 ************************************************************
 * Ta share lightbox
 *
 ************************************************************
 * JavaScript
 * 2015
 */
var memory_id = location.hash;
var memory_content = document.getElementById('memory-content');
var memory_box = memory_content.getElementsByClassName("box");
var memory_lightbox = document.getElementById('memory-lightbox');
var memory_now = 0;
var memory_close = document.getElementById("memory-close");

for (i = 0; i <= memory_box.length - 1; i++) {
  memory_box[i].addEventListener("click", function () {
    ImgLightbox(this.dataset.id);
  }, false);
}

function ImgLightbox(e) {

  switch (e) {
    case "2016-7":
      var stateObj = {
          'id': "2016-7"
        },
        url = './memory.php#2016-7',
        mytitle = "2016-7";
      //
      memory_lightbox.getElementsByTagName("img")[0].src = "./images/memory/email-2016-7.png";
      memory_lightbox.getElementsByTagName("img")[1].src = "./images/memory/print-2016-7-1.jpg";
      memory_lightbox.getElementsByTagName("img")[2].src = "./images/memory/print-2016-7-2.jpg";
      memory_lightbox.getElementsByTagName("a")[0].href = "./images/memory/email-2016-7.png";
      memory_lightbox.getElementsByTagName("a")[1].href = "./images/memory/print-2016-7-1.jpg";
      memory_lightbox.getElementsByTagName("a")[2].href = "./images/memory/print-2016-7-2.jpg";
      memory_lightbox.getElementsByTagName("h2")[0].innerHTML = "創刊號 2016-7";
      break;
    case "2016-12":
      var stateObj = {
          'id': "2016-12"
        },
        url = './memory.php#2016-12',
        mytitle = "2016-12";
      //
      memory_lightbox.getElementsByTagName("img")[0].src = "./images/memory/email-2016-11.png";
      memory_lightbox.getElementsByTagName("img")[1].src = "./images/memory/print-2016-11-1.jpg";
      memory_lightbox.getElementsByTagName("img")[2].src = "./images/memory/print-2016-11-2.jpg";
      memory_lightbox.getElementsByTagName("a")[0].href = "./images/memory/email-2016-11.png";
      memory_lightbox.getElementsByTagName("a")[1].href = "./images/memory/print-2016-11-1.jpg";
      memory_lightbox.getElementsByTagName("a")[2].href = "./images/memory/print-2016-11-2.jpg";
      memory_lightbox.getElementsByTagName("h2")[0].innerHTML = "第二期 2016-12";
      break;
    case "2017-6":
      var stateObj = {
          'id': "2017-6"
        },
        url = './memory.php#2017-6',
        mytitle = "2017-6";
      //
      memory_lightbox.getElementsByTagName("img")[0].src = "./images/memory/email-2017-6.jpg";
      memory_lightbox.getElementsByTagName("img")[1].src = "./images/memory/print-2017-6-1.jpg";
      memory_lightbox.getElementsByTagName("img")[2].src = "./images/memory/print-2017-6-2.jpg";
      memory_lightbox.getElementsByTagName("a")[0].href = "./images/memory/email-2017-6.jpg";
      memory_lightbox.getElementsByTagName("a")[1].href = "./images/memory/print-2017-6-1.jpg";
      memory_lightbox.getElementsByTagName("a")[2].href = "./images/memory/print-2017-6-2.jpg";
      memory_lightbox.getElementsByTagName("h2")[0].innerHTML = "第三期 2017-6";
      break;

    case "2017-12":
      var stateObj = {
          'id': "2017-12"
        },
        url = './memory.php#2017-12',
        mytitle = "2017-12";
      //
      memory_lightbox.getElementsByTagName("img")[0].src = "./images/memory/email-2017-12.jpg";
      memory_lightbox.getElementsByTagName("img")[1].src = "./images/memory/print-2017-12-1.jpg";
      memory_lightbox.getElementsByTagName("img")[2].src = "./images/memory/print-2017-12-2.jpg";
      memory_lightbox.getElementsByTagName("a")[0].href = "./images/memory/email-2017-12.jpg";
      memory_lightbox.getElementsByTagName("a")[1].href = "./images/memory/print-2017-12-1.jpg";
      memory_lightbox.getElementsByTagName("a")[2].href = "./images/memory/print-2017-12-2.jpg";
      memory_lightbox.getElementsByTagName("h2")[0].innerHTML = "第四期 2017-12";
      break;
    case "2018-9":
      var stateObj = {
          'id': "2018-9"
        },
        url = './memory.php#2018-9',
        mytitle = "2018-9";
      //
      memory_lightbox.getElementsByTagName("img")[0].src = "./images/memory/email-2018-9.jpg";
      memory_lightbox.getElementsByTagName("img")[1].src = "./images/memory/print-2018-9-1.jpg";
      memory_lightbox.getElementsByTagName("img")[2].src = "./images/memory/print-2018-9-2.jpg";
      memory_lightbox.getElementsByTagName("a")[0].href = "./images/memory/email-2018-9.jpg";
      memory_lightbox.getElementsByTagName("a")[1].href = "./images/memory/print-2018-9-1.jpg";
      memory_lightbox.getElementsByTagName("a")[2].href = "./images/memory/print-2018-9-2.jpg";
      memory_lightbox.getElementsByTagName("h2")[0].innerHTML = "第五期 2018-9";
      break;

  }
  window.history.pushState(stateObj, mytitle, url);

  document.getElementsByTagName('body')[0].classList.toggle('body-hidden');
  memory_lightbox.classList.toggle('open');
  memory_now = 1;
}

memory_close.addEventListener("click", function () {
  ImgLightboxClose();
}, false);

function ImgLightboxClose() {
  if (memory_now == 1) {
    var stateObj = {
        'id': "memory"
      },
      url = './memory.php',
      mytitle = "memory";
    window.history.pushState(stateObj, mytitle, url);
    document.getElementsByTagName('body')[0].classList.toggle('body-hidden');
    memory_lightbox.classList.toggle('open');
    memory_now = 0;
  }
}

if (memory_id === "#2016-7") {
  ImgLightbox("2016-7");
} else if (memory_id === "#2016-12") {
  ImgLightbox("2016-12");
} else if (memory_id === "#2017-6") {
  ImgLightbox("2017-6");
} else if (memory_id === "#2017-12") {
  ImgLightbox("2017-12");
} else if (memory_id === "#2018-9") {
  ImgLightbox("2018-9");
}
//
window.addEventListener('popstate', function () {
  changeURL();
}, false);

function changeURL() {
  /*if ( history.state.id === "memory" ) {*/
  ImgLightboxClose();
  /*}*/
}