/*!
 ************************************************************
 * Ta share lightbox
 *
 ************************************************************
 * JavaScript
 * 2015
 */

var works_share_content = document.getElementById('works-content');
var works_share_img = works_share_content.getElementsByTagName('img');
var works_lightbox = document.getElementById("works-lightbox");
var works_now = 0;

for ( i=0 ; i <= works_share_img.length - 1 ; i++ ) {
  works_share_img[i].addEventListener("click", function(){ImgLightbox(this);}, false);
}

function ImgLightbox(e) {
  works_lightbox.getElementsByTagName("img")[0].src = e.dataset.img;
  works_lightbox.classList.toggle('open-ta-lightobx');
  works_now = 1;
}

works_lightbox.addEventListener("click", function(){ImgLightboxClose();}, false);
function ImgLightboxClose() {
  if ( works_now == 1 ) {
  	works_lightbox.classList.toggle('open-ta-lightobx');
  	works_now = 0;
  }
}