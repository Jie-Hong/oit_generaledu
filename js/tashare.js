/*!
 ************************************************************
 * Ta share lightbox
 *
 ************************************************************
 * JavaScript
 * 2015
 */
var ta_share_content = document.getElementById('ta-share-content');
var ta_share_img = ta_share_content.getElementsByTagName('img');
var ta_lightbox = document.getElementById("ta-lightbox");
var ta_now = 0;

for ( i=0 ; i <= ta_share_img.length - 1 ; i++ ) {
  ta_share_img[i].addEventListener("click", function(){ImgLightbox(this);}, false);
}

function ImgLightbox(e) {
  ta_lightbox.getElementsByTagName("img")[0].src = e.dataset.img;
  ta_lightbox.classList.toggle('open-ta-lightobx');
  ta_now = 1;
}

ta_lightbox.addEventListener("click", function(){ImgLightboxClose();}, false);
function ImgLightboxClose() {
  if ( ta_now == 1 ) {
  	ta_lightbox.classList.toggle('open-ta-lightobx');
  	ta_now = 0;
  }
}