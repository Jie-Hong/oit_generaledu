/*!
 ************************************************************
 * Menu Aside RWD 搭配CSS
 ************************************************************
 * JavaScript
 * 2015
 */

var register_teacher = document.getElementById("register-choose-teacher");
var register_ta = document.getElementById("register-choose-ta");
var register_student = document.getElementById("register-choose-student");

var register_level = document.getElementById("register__level");


register_teacher.addEventListener("click", function(){RegisterChangeLevel(this);}, false);
register_ta.addEventListener("click", function(){RegisterChangeLevel(this);}, false);
register_student.addEventListener("click", function(){RegisterChangeLevel(this);}, false);

function RegisterChangeLevel(e) {
  register_teacher.classList.remove("register-level-choose");
  register_ta.classList.remove("register-level-choose");
  register_student.classList.remove("register-level-choose");

  e.classList.toggle('register-level-choose'); 

  if ( e.id == "register-choose-teacher" ) {
  	var choose_level = 3;
  } else if ( e.id == "register-choose-ta" ) {
    var choose_level = 4;
  } else if ( e.id == "register-choose-student" ) {
    var choose_level = 5;
  }
  register_level.value = choose_level;
}

