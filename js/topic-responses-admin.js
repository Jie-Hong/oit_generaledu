/*!
 ************************************************************
 * 回應再回應
 ************************************************************
 * JavaScript
 * 2015
 */

var topic_re_li = document.getElementById("comments-list").getElementsByClassName("comment-reply-btn");
for ( i=0 ; i <= topic_re_li.length - 1 ; i++ ) {
  topic_re_li[i].addEventListener("click", function(){TopicRe(this);}, false);
}

function TopicRe(e) {  
  var topic_re_li_id = e.parentElement.parentElement.dataset.reid;
  e.parentElement.getElementsByClassName("topic-comments-formbox")[0].innerHTML = '<form method="post" action=""><textarea class="topic-re-text" name="text" rows="4" placeholder="請輸入回應文字，按Enter或Shift+Enter，可增加高度。" required></textarea><input class="topic-re-btn" type="submit" value="回應"><input type="text" hidden name="comment_id"  value="'+topic_re_li_id+'"></form>';

  //套用 autosize 函式 高度
  autosize(e.parentElement.getElementsByClassName("topic-comments-formbox")[0].querySelector('textarea'));
}
