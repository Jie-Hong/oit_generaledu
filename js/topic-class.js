/*!
 ************************************************************
 * 班級篩選
 ************************************************************
 * JavaScript
 * 2015
 */

var comments_filter = document.getElementById("comments-filter");
var comments_select = document.getElementById("comments-filter").getElementsByClassName("comments-select");

for ( i=0 ; i <= comments_select.length - 1 ; i++ ) {
  comments_select[i].addEventListener("click", function(){CommentClass(this);}, false);
}

function CommentClass(e) {
  var comments_select_classname = e.dataset.class;

  switch (comments_select_classname) {
    //全部
    case "all":
      CommentClassReset();
      break;

    //醫管
    case "ha-a":
    case "ha-b":

    //通訊
    case "ce-a":
    case "ce-b":

    //資管
    case "mi-a":
    case "mi-b":

    //材纖
    case "mt-a":
    case "mt-b":

    //行銷
    case "md-a":
    case "md-b":
      CommentClassReset();
      CommentClassChoose(comments_select_classname);
      break;

    default:
      break;
  }

  //清除所有 active
  for ( i=0 ; i <= comments_select.length - 1 ; i++ ) {
    comments_select[i].classList.remove('active');
  }

  //為目前所點的添加active
  e.classList.toggle('active');
}


var comments_list = document.getElementById("comments-list").getElementsByTagName('li');
function CommentClassChoose(e) {
  for ( i=0 ; i <= comments_list.length - 1 ; i++ ) {
  	var comments_list_classname = comments_list[i].dataset.reclass;

    //2次回復
    var comments_list_2 = comments_list[i].parentElement.parentElement.className;

    //如果一直修改不成功 清除電腦所有瀏覽紀錄
  	if ( "1041-" + e == comments_list_classname || "1042-" + e == comments_list_classname || "1051-" + e == comments_list_classname || "1052-" + e == comments_list_classname || "1061-" + e == comments_list_classname || "1062-" + e == comments_list_classname || comments_list_2 == "topic-comments-2" ) {

  	} else {
  	  comments_list[i].style.display = "none";
  	}
  }
}

function CommentClassReset() {
  for ( i=0 ; i <= comments_list.length - 1 ; i++ ) {
  	comments_list[i].style.display = "";
  }
}
