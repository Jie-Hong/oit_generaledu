<?php
  require_once "./php/functions.php";

  $semester = $_GET['semester'];

  if (isset($semester)) {
    if ($semester === "1041" || $semester === "1042" || $semester === "1051" || $semester === "1052" || $semester === "1061") {
    } else {
      header('Location: 404.php');
    }
  } else {
    header('Location: picture.php?semester=1061');
  }
?>
<!DOCTYPE html>
<html lang="zh-Hant-TW">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, , maximum-scale=1">
  <title><?php echo $semester ?> 影像紀錄 - 文韻亞東</title>
  <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css"> <!-- Font-Awesome -->
  <link rel="stylesheet" href="outdatedbrowser/outdatedbrowser.css"> <!-- outdatedbrowser 檢查瀏覽器 -->
  <script src="outdatedbrowser/outdatedbrowser.js"></script> <!-- outdatedbrowser 檢查瀏覽器 -->
  <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<header id="header">
  <section class="header-site-channel">
    <div class="container">
      <div class="header-channel-content">
        <div class="header-logo">
          <a href="index.php"><img src="images/logo.png" /></a>
          <span>本課程由教育部資訊及科技教育司支持</span>
          <div class="header-video">
            <a href="./news-detail.php?id=24">
              <!-- <img src="./video/1042.jpg"> -->
              <!-- <i class="fa fa-play-circle-o"></i> -->
            </a>
          </div>
        </div>
        <?php
        if($loggedin)
        {
        echo
          "<div class=\"header-login\">
             <ul>
               <li><a>您好，$user</a></li>
               <li><a href=\"user-edito.php\">會員編輯</a></li>
               <li><a href=\"admin/index.php\">後台管理</a></li>
               <li><a href=\"logout.php\">登出</a></li>
             </ul>
           </div>";
        }
        else
        {
          echo
            "<div class=\"header-login\">
               <ul>
                 <li><a href=\"login.php\">登入</a></li>
                 <li><a href=\"register.php\">註冊</a></li>
               </ul>
             </div>";
        }
        ?>
      </div>
    </div>
  </section>
  <nav class="navbar" role="navigation">
    <div class="container">
      <div class="navbar-collapse">
        <ul class="nav navbar-nav">
          <li><a href="news.php">最新消息</a></li>
          <li><a href="intro.php">課程計畫</a></li>
          <li><a href="forums.php">討論區</a></li>
          <li><a href="vote.php">投票活動</a></li>
          <li><a href="memory.php">亞東印記</a></li>
          <li><a href="works.php">優秀作品</a></li>
          <li class="active"><a href="picture.php">影像紀錄</a></li>
          <li><a href="activity.php">活動集錦</a></li>
          <li><a href="videosharing.php">影片分享</a></li>
          <li><a href="feedback.php">TA回饋分享</a></li>
          <li><a href="team.php">核心團隊</a></li>
          <li><a href="links.php">相關資源</a></li>
        </ul>
      </div>
    </div>
  </nav>
</header>
<section class="images-title">
  <div class="container">
    <h1><span>Images Record</span>影像紀錄</h1>
  </div>
</section>
<section class="images-main">
  <div class="container">
    <div class="breadcrumb">
      <ul>
        <li><a href="index.php">首頁</a></li>
        <li><a href="picture.php">影像紀錄</a></li>
        <li><a href="picture.php?semester=<?php echo $semester ?>">第<?php echo $semester ?>學期</a></li>
      </ul>
    </div>

    <div class="team-select">
      <select id="team-select" class="team-dropdown">
        <option value="1061" <?php if ($semester==="1061"){ echo "selected"; }?>>1061 學期</option>
        <option value="1052" <?php if ($semester==="1052"){ echo "selected"; }?>>1052 學期</option>
        <option value="1051" <?php if ($semester==="1051"){ echo "selected"; }?>>1051 學期</option>
        <option value="1042" <?php if ($semester==="1042"){ echo "selected"; }?>>1042 學期</option>
        <option value="1041" <?php if ($semester==="1041"){ echo "selected"; }?>>1041 學期</option>
      </select>
    </div>

    <div class="images-content" id="images-content">
      <?php if ( $semester === "1042") { ?>
      <h2>104-2 影像紀錄照片</h2>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-08-08/1.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>活動照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-08-08/2.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>活動照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-08-08/3.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>活動照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-08-08/4.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>活動照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-08-08/5.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>活動照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-08-08/6.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>活動照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-08-08/7.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>活動照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-08-08/8.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>活動照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-08-08/9.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>活動照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-08-08/10.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>活動照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-08-08/11.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>活動照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-08-08/12.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>活動照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-08-08/13.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>活動照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-08-08/14.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>活動照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-08-08/15.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>活動照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-08-08/16.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>活動照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-08-08/17.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>活動照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-08-08/18.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>活動照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-08-08/19.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>活動照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-08-08/20.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>活動照片</h3>
        </div>
      </div>

      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-05-08/DSCN5032.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>作文聊天室</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-05-08/DSCN5033.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>作文聊天室</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-05-08/DSCN5034.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>作文聊天室</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-05-08/DSCN5035.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>作文聊天室</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-05-08/DSCN5036.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>作文聊天室</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-05-08/DSCN5037.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>作文聊天室</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-05-08/DSCN5038.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>作文聊天室</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-05-08/DSCN5039.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>作文聊天室</h3>
        </div>
      </div>

      <!-- ***************************************************** 分隔* **************************************************** -->
      <?php } else if ( $semester === "1041") { ?>
      <h2>104-1 影像紀錄照片</h2>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-10-07/01.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>林智莉老師上課照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-10-07/02.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>林智莉老師上課照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-10-07/03.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>林智莉老師上課照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-10-14-1/DSCN0463.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>作文聊天室</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-10-14-1/DSCN0464.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>作文聊天室</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-10-14-1/DSCN0465.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>作文聊天室</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-10-14-1/DSCN0467.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>作文聊天室</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-10-14-1/DSCN0468.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>2014-10-14 作文聊天室</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-10-14-2/DSCN0435.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>吳憶蘭老師上課照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-10-14-2/DSCN0436.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>吳憶蘭老師上課照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-10-14-2/DSCN0437.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>吳憶蘭老師上課照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-10-14-3/DSCN0408.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>李慧琪老師上課照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-10-14-3/DSCN0409.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>李慧琪老師上課照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-10-14-3/DSCN0416.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>李慧琪老師上課照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-10-14-4/DSCN0398.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>黃啟峰老師上課照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-10-14-4/DSCN0402.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>黃啟峰老師上課照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-10-14-4/DSCN0407.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>黃啟峰老師上課照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-10-14-5/DSCN0420.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>蕭涵珍老師上課照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-10-14-5/DSCN0425.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>蕭涵珍老師上課照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-10-14-5/DSCN0433.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>蕭涵珍老師上課照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-10-15-1/DSC06853.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>作文聊天室</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-10-15-1/DSC06854.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>作文聊天室</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-10-15-1/DSC06856.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>作文聊天室</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-10-15-2/DSC06841.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>張又勻老師上課照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-10-15-2/DSC06843.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>張又勻老師上課照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-10-15-2/DSC06852.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>張又勻老師上課照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-10-15-3/DSC06828.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>謝君讚老師上課照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-10-15-3/DSC06830.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>謝君讚老師上課照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-10-15-3/DSC06833.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>謝君讚老師上課照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-11-01-1/18271.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>林智莉課堂照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-11-01-1/18272.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>林智莉課堂照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-11-01-1/284945.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>林智莉課堂照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-11-01-1/284947.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>林智莉課堂照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-11-01-1/284951.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>林智莉課堂照片</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-11-01-2/DSCN4115.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>作文聊天室</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-11-01-2/DSCN4116.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>作文聊天室</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-11-01-2/DSCN4117.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>作文聊天室</h3>
        </div>
      </div>

      <!-- ***************************************************** 分隔* **************************************************** -->

      <?php } else if ( $semester === "1051") { ?>
      <h2>105-1 影像紀錄照片</h2>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-10-12/1.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-管管作家</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-10-12/2.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-管管作家</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-10-12/3.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-管管作家</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-10-12/4.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-管管作家</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-10-12/5.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-管管作家</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-11-30/1.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-廖玉蕙老師</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-11-30/2.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-廖玉蕙老師</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-11-30/3.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-廖玉蕙老師</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2015-11-30/4.JPG" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-廖玉蕙老師</h3>
        </div>
      </div>



      <!-- ***************************************************** 分隔* **************************************************** -->

      <?php } else if ( $semester === "1052") { ?>
      <h2>105-2 影像紀錄照片</h2>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-03-29/1.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-羅智成老師</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-03-29/2.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-羅智成老師</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-03-29/3.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-羅智成老師</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-03-29/4.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-羅智成老師</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-03-29/5.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-羅智成老師</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-03-29/6.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-羅智成老師</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-05-03/1.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-廖鴻基老師</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-05-03/2.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-廖鴻基老師</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-05-03/3.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-廖鴻基老師</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-05-03/4.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-廖鴻基老師</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-05-03/5.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-廖鴻基老師</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-05-24/1.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-鄭美里老師</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-05-24/2.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-鄭美里老師</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-05-24/3.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-鄭美里老師</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2016-05-24/4.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-鄭美里老師</h3>
        </div>
      </div>
      <!-- ***************************************************** 分隔* **************************************************** -->

      <?php } else if ( $semester === "1061") { ?>
      <h2>106-1 影像紀錄照片</h2>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2017-10-17/1.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-宋芳綺老師</h3> 作家面對面-簡媜老師
        </div>期末成果展-藝文走廊
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2017-10-17/2.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-宋芳綺老師</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2017-10-17/3.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-宋芳綺老師</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2017-10-17/4.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-宋芳綺老師</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2017-10-17/5.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-宋芳綺老師</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2017-10-17/6.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-宋芳綺老師</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2017-11-21/1.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-朱嘉雯老師</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2017-11-21/2.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-朱嘉雯老師</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2017-11-21/3.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-朱嘉雯老師</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2017-11-21/4.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-朱嘉雯老師</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2017-11-28/1.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-簡媜老師</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2017-11-28/2.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-簡媜老師</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2017-11-28/3.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-簡媜老師</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2017-11-28/4.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-簡媜老師</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2017-11-28/5.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-簡媜老師</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2017-11-28/6.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>作家面對面-簡媜老師</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2017-Final/1.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>期末成果展-藝文走廊</h3>
        </div>
      </div>
      <div class="photo-item">
        <div class="photo-item-img-box">
          <img src="images/record/2017-Final/2.jpg" />
        </div>
        <div class="photo-item-title">
          <h3>期末成果展-藝文走廊</h3>
        </div>
      </div>
<!--年度資料往上插!-->

      <?php } ?>
    </div>
  </div>
</section>
<footer id="footer">
  <div class="subfooter">
    <div class="container">
      <div class="row">
        <div class="col-xxs-12 col-md-7">
          <div class="footer-nav">
            <ul>
              <li><a href="index.php">首頁</a></li>
              <li><a href="news.php">最新消息</a></li>
              <li><a href="intro.php">課程計畫</a></li>
              <li><a href="forums.php">討論區</a></li>
              <li><a href="works.php">優秀作品</a></li>
              <li><a href="picture.php">影像紀錄</a></li>
              <li><a href="team.php">核心團隊</a></li>
            </ul>
          </div>
        </div>
        <div class="col-xxs-12 col-md-5">
          <div class="school">
            <p>亞東技術學院 通識教育中心 OIT Center for General Education</p>
          </div>
          <div class="plan">
            <p>文韻亞東 ─ 中文閱讀書寫課程革新計畫</p>
          </div>
          <div class="hss">
            <p>本課程由教育部資訊及科技教育司支持</p>
          </div>
        </div>
      </div>
    </div> <!-- container -->
  </div> <!-- subfooter -->
  <div class="copyright">
    <div class="container">
      <div class="text">
        <small>
          <address class="author">
            Copyright  2015-2016.
          </address>
          <a href="http://gecw.oit.edu.tw/bin/home.php" title="亞東技術學院通識教育中心" target="_blank">亞東技術學院通識教育中心</a> All Rights Reserved.
        </small>
      </div>
      <div class="total">
        <?php require_once "./statistics.php"; ?>
      </div>
    </div>
  </div>
</footer>
<div id="p-lightbox" class="p-lightobx">
  <div class="p-lightobx-center">
    <img src="">
  </div>
</div>
<div id="outdated"></div> <!-- outdatedbrowser 檢查瀏覽器 -->
<script src="js/style.js"></script>
<script type="text/javascript"><!-- 學期切換的事件聆聽javascript-->
  document.getElementById("team-select").addEventListener("change", function() {
    document.location.href= "picture.php?semester=" + this.value;
  });
</script>
<script src="outdatedbrowser/dom-outdatedbrowser.js"></script> <!-- outdatedbrowser 檢查瀏覽器 -->
<script src="js/picture.js"></script>
</body>
</html>
